--
-- PostgreSQL database dump
--

-- Dumped from database version 10.7 (Ubuntu 10.7-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.2 (Ubuntu 11.2-1.pgdg18.04+1)

-- Started on 2019-04-14 17:39:57 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 16480)
-- Name: categoriaevento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categoriaevento (
    id bigint NOT NULL,
    descricao character varying(100) NOT NULL
);


ALTER TABLE public.categoriaevento OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16478)
-- Name: categoriaevento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categoriaevento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categoriaevento_id_seq OWNER TO postgres;

--
-- TOC entry 3058 (class 0 OID 0)
-- Dependencies: 196
-- Name: categoriaevento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categoriaevento_id_seq OWNED BY public.categoriaevento.id;


--
-- TOC entry 199 (class 1259 OID 16488)
-- Name: cidade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cidade (
    id bigint NOT NULL,
    nome character varying(100) NOT NULL,
    estado_id bigint NOT NULL
);


ALTER TABLE public.cidade OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16486)
-- Name: cidade_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cidade_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cidade_id_seq OWNER TO postgres;

--
-- TOC entry 3059 (class 0 OID 0)
-- Dependencies: 198
-- Name: cidade_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cidade_id_seq OWNED BY public.cidade.id;


--
-- TOC entry 200 (class 1259 OID 16494)
-- Name: empresa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.empresa (
    cnae character varying(7),
    cnpj character varying(14) NOT NULL,
    nomefantasia character varying(100) NOT NULL,
    razaosocial character varying(100) NOT NULL,
    id bigint NOT NULL,
    endereco_id bigint NOT NULL
);


ALTER TABLE public.empresa OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16499)
-- Name: empresa_meios_contato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.empresa_meios_contato (
    empresa_id bigint NOT NULL,
    meios_contato_id bigint NOT NULL
);


ALTER TABLE public.empresa_meios_contato OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16504)
-- Name: endereco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.endereco (
    id bigint NOT NULL,
    bairro character varying(50) NOT NULL,
    cep character varying(8) NOT NULL,
    complemento character varying(50),
    logradouro character varying(100) NOT NULL,
    numero integer NOT NULL,
    cidade_id bigint NOT NULL,
    endereco_id bigint NOT NULL
);


ALTER TABLE public.endereco OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16502)
-- Name: endereco_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.endereco_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.endereco_id_seq OWNER TO postgres;

--
-- TOC entry 3060 (class 0 OID 0)
-- Dependencies: 202
-- Name: endereco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.endereco_id_seq OWNED BY public.endereco.id;


--
-- TOC entry 205 (class 1259 OID 16512)
-- Name: estado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estado (
    id bigint NOT NULL,
    nome character varying(100) NOT NULL
);


ALTER TABLE public.estado OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16510)
-- Name: estado_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estado_id_seq OWNER TO postgres;

--
-- TOC entry 3061 (class 0 OID 0)
-- Dependencies: 204
-- Name: estado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estado_id_seq OWNED BY public.estado.id;


--
-- TOC entry 207 (class 1259 OID 16520)
-- Name: evento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.evento (
    id bigint NOT NULL,
    datahorainicio timestamp without time zone NOT NULL,
    datahoratermino timestamp without time zone NOT NULL,
    descricao character varying(255) NOT NULL,
    facilitador character varying(100) NOT NULL,
    imagemdivulgacao text,
    maximodeparticipantes integer NOT NULL,
    minimodeparticipantes integer,
    observacoes character varying(255) NOT NULL,
    valor numeric(19,2) NOT NULL,
    categoriaevento_id bigint NOT NULL,
    empresa_id bigint NOT NULL,
    endereco_id bigint NOT NULL,
    tipoevento_id bigint NOT NULL
);


ALTER TABLE public.evento OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16518)
-- Name: evento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evento_id_seq OWNER TO postgres;

--
-- TOC entry 3062 (class 0 OID 0)
-- Dependencies: 206
-- Name: evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.evento_id_seq OWNED BY public.evento.id;


--
-- TOC entry 208 (class 1259 OID 16529)
-- Name: evento_meios_contato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.evento_meios_contato (
    evento_id bigint NOT NULL,
    meios_contato_id bigint NOT NULL
);


ALTER TABLE public.evento_meios_contato OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16534)
-- Name: meiocontato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.meiocontato (
    id bigint NOT NULL,
    contato character varying(255) NOT NULL,
    tipomeiocontato_id bigint NOT NULL
);


ALTER TABLE public.meiocontato OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16532)
-- Name: meiocontato_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.meiocontato_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meiocontato_id_seq OWNER TO postgres;

--
-- TOC entry 3063 (class 0 OID 0)
-- Dependencies: 209
-- Name: meiocontato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.meiocontato_id_seq OWNED BY public.meiocontato.id;


--
-- TOC entry 211 (class 1259 OID 16540)
-- Name: participante; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.participante (
    cpf character varying(11) NOT NULL,
    datanascimento timestamp without time zone NOT NULL,
    nome character varying(100) NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.participante OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16545)
-- Name: participante_meios_contato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.participante_meios_contato (
    participante_id bigint NOT NULL,
    meios_contato_id bigint NOT NULL
);


ALTER TABLE public.participante_meios_contato OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16550)
-- Name: tipoevento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipoevento (
    id bigint NOT NULL,
    descricao character varying(100) NOT NULL
);


ALTER TABLE public.tipoevento OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16548)
-- Name: tipoevento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipoevento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipoevento_id_seq OWNER TO postgres;

--
-- TOC entry 3064 (class 0 OID 0)
-- Dependencies: 213
-- Name: tipoevento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipoevento_id_seq OWNED BY public.tipoevento.id;


--
-- TOC entry 216 (class 1259 OID 16558)
-- Name: tipomeiocontato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipomeiocontato (
    id bigint NOT NULL,
    descricao character varying(50) NOT NULL
);


ALTER TABLE public.tipomeiocontato OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16556)
-- Name: tipomeiocontato_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipomeiocontato_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipomeiocontato_id_seq OWNER TO postgres;

--
-- TOC entry 3065 (class 0 OID 0)
-- Dependencies: 215
-- Name: tipomeiocontato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipomeiocontato_id_seq OWNED BY public.tipomeiocontato.id;


--
-- TOC entry 218 (class 1259 OID 16566)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id bigint NOT NULL,
    nomeusuario character varying(20) NOT NULL,
    senha character varying(128) NOT NULL
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16564)
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_seq OWNER TO postgres;

--
-- TOC entry 3066 (class 0 OID 0)
-- Dependencies: 217
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuario.id;


--
-- TOC entry 2855 (class 2604 OID 16483)
-- Name: categoriaevento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoriaevento ALTER COLUMN id SET DEFAULT nextval('public.categoriaevento_id_seq'::regclass);


--
-- TOC entry 2856 (class 2604 OID 16491)
-- Name: cidade id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cidade ALTER COLUMN id SET DEFAULT nextval('public.cidade_id_seq'::regclass);


--
-- TOC entry 2857 (class 2604 OID 16507)
-- Name: endereco id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.endereco ALTER COLUMN id SET DEFAULT nextval('public.endereco_id_seq'::regclass);


--
-- TOC entry 2858 (class 2604 OID 16515)
-- Name: estado id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado ALTER COLUMN id SET DEFAULT nextval('public.estado_id_seq'::regclass);


--
-- TOC entry 2859 (class 2604 OID 16523)
-- Name: evento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento ALTER COLUMN id SET DEFAULT nextval('public.evento_id_seq'::regclass);


--
-- TOC entry 2860 (class 2604 OID 16537)
-- Name: meiocontato id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meiocontato ALTER COLUMN id SET DEFAULT nextval('public.meiocontato_id_seq'::regclass);


--
-- TOC entry 2861 (class 2604 OID 16553)
-- Name: tipoevento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipoevento ALTER COLUMN id SET DEFAULT nextval('public.tipoevento_id_seq'::regclass);


--
-- TOC entry 2862 (class 2604 OID 16561)
-- Name: tipomeiocontato id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipomeiocontato ALTER COLUMN id SET DEFAULT nextval('public.tipomeiocontato_id_seq'::regclass);


--
-- TOC entry 2863 (class 2604 OID 16569)
-- Name: usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id SET DEFAULT nextval('public.usuario_id_seq'::regclass);


--
-- TOC entry 3031 (class 0 OID 16480)
-- Dependencies: 197
-- Data for Name: categoriaevento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categoriaevento (id, descricao) FROM stdin;
\.


--
-- TOC entry 3033 (class 0 OID 16488)
-- Dependencies: 199
-- Data for Name: cidade; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cidade (id, nome, estado_id) FROM stdin;
\.


--
-- TOC entry 3034 (class 0 OID 16494)
-- Dependencies: 200
-- Data for Name: empresa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.empresa (cnae, cnpj, nomefantasia, razaosocial, id, endereco_id) FROM stdin;
\.


--
-- TOC entry 3035 (class 0 OID 16499)
-- Dependencies: 201
-- Data for Name: empresa_meios_contato; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.empresa_meios_contato (empresa_id, meios_contato_id) FROM stdin;
\.


--
-- TOC entry 3037 (class 0 OID 16504)
-- Dependencies: 203
-- Data for Name: endereco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.endereco (id, bairro, cep, complemento, logradouro, numero, cidade_id, endereco_id) FROM stdin;
\.


--
-- TOC entry 3039 (class 0 OID 16512)
-- Dependencies: 205
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estado (id, nome) FROM stdin;
\.


--
-- TOC entry 3041 (class 0 OID 16520)
-- Dependencies: 207
-- Data for Name: evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.evento (id, datahorainicio, datahoratermino, descricao, facilitador, imagemdivulgacao, maximodeparticipantes, minimodeparticipantes, observacoes, valor, categoriaevento_id, empresa_id, endereco_id, tipoevento_id) FROM stdin;
\.


--
-- TOC entry 3042 (class 0 OID 16529)
-- Dependencies: 208
-- Data for Name: evento_meios_contato; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.evento_meios_contato (evento_id, meios_contato_id) FROM stdin;
\.


--
-- TOC entry 3044 (class 0 OID 16534)
-- Dependencies: 210
-- Data for Name: meiocontato; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.meiocontato (id, contato, tipomeiocontato_id) FROM stdin;
\.


--
-- TOC entry 3045 (class 0 OID 16540)
-- Dependencies: 211
-- Data for Name: participante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.participante (cpf, datanascimento, nome, id) FROM stdin;
\.


--
-- TOC entry 3046 (class 0 OID 16545)
-- Dependencies: 212
-- Data for Name: participante_meios_contato; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.participante_meios_contato (participante_id, meios_contato_id) FROM stdin;
\.


--
-- TOC entry 3048 (class 0 OID 16550)
-- Dependencies: 214
-- Data for Name: tipoevento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipoevento (id, descricao) FROM stdin;
\.


--
-- TOC entry 3050 (class 0 OID 16558)
-- Dependencies: 216
-- Data for Name: tipomeiocontato; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipomeiocontato (id, descricao) FROM stdin;
\.


--
-- TOC entry 3052 (class 0 OID 16566)
-- Dependencies: 218
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id, nomeusuario, senha) FROM stdin;
\.


--
-- TOC entry 3067 (class 0 OID 0)
-- Dependencies: 196
-- Name: categoriaevento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categoriaevento_id_seq', 1, false);


--
-- TOC entry 3068 (class 0 OID 0)
-- Dependencies: 198
-- Name: cidade_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cidade_id_seq', 1, false);


--
-- TOC entry 3069 (class 0 OID 0)
-- Dependencies: 202
-- Name: endereco_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.endereco_id_seq', 1, false);


--
-- TOC entry 3070 (class 0 OID 0)
-- Dependencies: 204
-- Name: estado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estado_id_seq', 1, false);


--
-- TOC entry 3071 (class 0 OID 0)
-- Dependencies: 206
-- Name: evento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.evento_id_seq', 1, false);


--
-- TOC entry 3072 (class 0 OID 0)
-- Dependencies: 209
-- Name: meiocontato_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.meiocontato_id_seq', 1, false);


--
-- TOC entry 3073 (class 0 OID 0)
-- Dependencies: 213
-- Name: tipoevento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipoevento_id_seq', 1, false);


--
-- TOC entry 3074 (class 0 OID 0)
-- Dependencies: 215
-- Name: tipomeiocontato_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipomeiocontato_id_seq', 1, false);


--
-- TOC entry 3075 (class 0 OID 0)
-- Dependencies: 217
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_seq', 1, false);


--
-- TOC entry 2865 (class 2606 OID 16485)
-- Name: categoriaevento categoriaevento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoriaevento
    ADD CONSTRAINT categoriaevento_pkey PRIMARY KEY (id);


--
-- TOC entry 2867 (class 2606 OID 16493)
-- Name: cidade cidade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cidade
    ADD CONSTRAINT cidade_pkey PRIMARY KEY (id);


--
-- TOC entry 2869 (class 2606 OID 16498)
-- Name: empresa empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_pkey PRIMARY KEY (id);


--
-- TOC entry 2873 (class 2606 OID 16509)
-- Name: endereco endereco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.endereco
    ADD CONSTRAINT endereco_pkey PRIMARY KEY (id);


--
-- TOC entry 2875 (class 2606 OID 16517)
-- Name: estado estado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id);


--
-- TOC entry 2877 (class 2606 OID 16528)
-- Name: evento evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento
    ADD CONSTRAINT evento_pkey PRIMARY KEY (id);


--
-- TOC entry 2881 (class 2606 OID 16539)
-- Name: meiocontato meiocontato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meiocontato
    ADD CONSTRAINT meiocontato_pkey PRIMARY KEY (id);


--
-- TOC entry 2883 (class 2606 OID 16544)
-- Name: participante participante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participante
    ADD CONSTRAINT participante_pkey PRIMARY KEY (id);


--
-- TOC entry 2887 (class 2606 OID 16555)
-- Name: tipoevento tipoevento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipoevento
    ADD CONSTRAINT tipoevento_pkey PRIMARY KEY (id);


--
-- TOC entry 2889 (class 2606 OID 16563)
-- Name: tipomeiocontato tipomeiocontato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipomeiocontato
    ADD CONSTRAINT tipomeiocontato_pkey PRIMARY KEY (id);


--
-- TOC entry 2879 (class 2606 OID 16575)
-- Name: evento_meios_contato uk_4bsfjqrb727l17cuqdqegs83n; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento_meios_contato
    ADD CONSTRAINT uk_4bsfjqrb727l17cuqdqegs83n UNIQUE (meios_contato_id);


--
-- TOC entry 2885 (class 2606 OID 16577)
-- Name: participante_meios_contato uk_ifo37b8ahofkmdy1gfni5xlgs; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participante_meios_contato
    ADD CONSTRAINT uk_ifo37b8ahofkmdy1gfni5xlgs UNIQUE (meios_contato_id);


--
-- TOC entry 2871 (class 2606 OID 16573)
-- Name: empresa_meios_contato uk_sgktg28tuxmggl20gloj2wwkx; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa_meios_contato
    ADD CONSTRAINT uk_sgktg28tuxmggl20gloj2wwkx UNIQUE (meios_contato_id);


--
-- TOC entry 2891 (class 2606 OID 16571)
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2898 (class 2606 OID 16608)
-- Name: endereco fk10v8nejbei3jb07gl024pfx96; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.endereco
    ADD CONSTRAINT fk10v8nejbei3jb07gl024pfx96 FOREIGN KEY (endereco_id) REFERENCES public.participante(id);


--
-- TOC entry 2893 (class 2606 OID 16583)
-- Name: empresa fk1bsdq6lhlktaf86nwgbftqrag; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT fk1bsdq6lhlktaf86nwgbftqrag FOREIGN KEY (endereco_id) REFERENCES public.endereco(id);


--
-- TOC entry 2894 (class 2606 OID 16588)
-- Name: empresa fk1qxb7ae8vdagy0mb5p5pjifed; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT fk1qxb7ae8vdagy0mb5p5pjifed FOREIGN KEY (id) REFERENCES public.usuario(id);


--
-- TOC entry 2905 (class 2606 OID 16643)
-- Name: meiocontato fk5vkdr60tx83okowhh42lax5gg; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meiocontato
    ADD CONSTRAINT fk5vkdr60tx83okowhh42lax5gg FOREIGN KEY (tipomeiocontato_id) REFERENCES public.tipomeiocontato(id);


--
-- TOC entry 2897 (class 2606 OID 16603)
-- Name: endereco fk8b1kcb3wucapb8dejshyn5fsx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.endereco
    ADD CONSTRAINT fk8b1kcb3wucapb8dejshyn5fsx FOREIGN KEY (cidade_id) REFERENCES public.cidade(id);


--
-- TOC entry 2906 (class 2606 OID 16648)
-- Name: participante fkb06umka1q78t2smjwj53i5i8d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participante
    ADD CONSTRAINT fkb06umka1q78t2smjwj53i5i8d FOREIGN KEY (id) REFERENCES public.usuario(id);


--
-- TOC entry 2896 (class 2606 OID 16598)
-- Name: empresa_meios_contato fkgfo7692t3t7h3pw05tacbvn8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa_meios_contato
    ADD CONSTRAINT fkgfo7692t3t7h3pw05tacbvn8 FOREIGN KEY (empresa_id) REFERENCES public.empresa(id);


--
-- TOC entry 2899 (class 2606 OID 16613)
-- Name: evento fkgnq7godxae8tw13blsq77o74u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento
    ADD CONSTRAINT fkgnq7godxae8tw13blsq77o74u FOREIGN KEY (categoriaevento_id) REFERENCES public.categoriaevento(id);


--
-- TOC entry 2903 (class 2606 OID 16633)
-- Name: evento_meios_contato fkjxpv6s791n1lgb4m7aoonq7lt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento_meios_contato
    ADD CONSTRAINT fkjxpv6s791n1lgb4m7aoonq7lt FOREIGN KEY (meios_contato_id) REFERENCES public.meiocontato(id);


--
-- TOC entry 2908 (class 2606 OID 16658)
-- Name: participante_meios_contato fkk5wi3oaecmtumj5kyeeu9n7k5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participante_meios_contato
    ADD CONSTRAINT fkk5wi3oaecmtumj5kyeeu9n7k5 FOREIGN KEY (participante_id) REFERENCES public.participante(id);


--
-- TOC entry 2892 (class 2606 OID 16578)
-- Name: cidade fkkworrwk40xj58kevvh3evi500; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cidade
    ADD CONSTRAINT fkkworrwk40xj58kevvh3evi500 FOREIGN KEY (estado_id) REFERENCES public.estado(id);


--
-- TOC entry 2907 (class 2606 OID 16653)
-- Name: participante_meios_contato fkq8q9qw4ml6u61qeyofhi1wf5m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participante_meios_contato
    ADD CONSTRAINT fkq8q9qw4ml6u61qeyofhi1wf5m FOREIGN KEY (meios_contato_id) REFERENCES public.meiocontato(id);


--
-- TOC entry 2901 (class 2606 OID 16623)
-- Name: evento fkqd0alhw4xw95c617h4kgil7qs; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento
    ADD CONSTRAINT fkqd0alhw4xw95c617h4kgil7qs FOREIGN KEY (endereco_id) REFERENCES public.endereco(id);


--
-- TOC entry 2900 (class 2606 OID 16618)
-- Name: evento fkqk3rs917r9nn0g5iargrbects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento
    ADD CONSTRAINT fkqk3rs917r9nn0g5iargrbects FOREIGN KEY (empresa_id) REFERENCES public.empresa(id);


--
-- TOC entry 2902 (class 2606 OID 16628)
-- Name: evento fkr5txxi0im1pui5fcidvomc88e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento
    ADD CONSTRAINT fkr5txxi0im1pui5fcidvomc88e FOREIGN KEY (tipoevento_id) REFERENCES public.tipoevento(id);


--
-- TOC entry 2904 (class 2606 OID 16638)
-- Name: evento_meios_contato fksblq48qd50cxt04bqno0b0lpu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento_meios_contato
    ADD CONSTRAINT fksblq48qd50cxt04bqno0b0lpu FOREIGN KEY (evento_id) REFERENCES public.evento(id);


--
-- TOC entry 2895 (class 2606 OID 16593)
-- Name: empresa_meios_contato fksfo5av6ynd7o6ljqqf1rt78ow; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa_meios_contato
    ADD CONSTRAINT fksfo5av6ynd7o6ljqqf1rt78ow FOREIGN KEY (meios_contato_id) REFERENCES public.meiocontato(id);


-- Completed on 2019-04-14 17:39:57 -03

--
-- PostgreSQL database dump complete
--

