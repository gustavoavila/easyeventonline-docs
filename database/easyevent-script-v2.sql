--
-- PostgreSQL database dump
--

-- Dumped from database version 10.7 (Ubuntu 10.7-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.2 (Ubuntu 11.2-1.pgdg18.04+1)

-- Started on 2019-04-19 20:32:56 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 17373)
-- Name: categoriaevento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categoriaevento (
    id bigint NOT NULL,
    descricao character varying(100) NOT NULL
);


ALTER TABLE public.categoriaevento OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 17371)
-- Name: categoriaevento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categoriaevento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categoriaevento_id_seq OWNER TO postgres;

--
-- TOC entry 3032 (class 0 OID 0)
-- Dependencies: 196
-- Name: categoriaevento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categoriaevento_id_seq OWNED BY public.categoriaevento.id;


--
-- TOC entry 199 (class 1259 OID 17381)
-- Name: cidade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cidade (
    id bigint NOT NULL,
    nome character varying(100) NOT NULL,
    estado_id bigint NOT NULL,
    codigo_ibge integer NOT NULL
);


ALTER TABLE public.cidade OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 17379)
-- Name: cidade_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cidade_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cidade_id_seq OWNER TO postgres;

--
-- TOC entry 3033 (class 0 OID 0)
-- Dependencies: 198
-- Name: cidade_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cidade_id_seq OWNED BY public.cidade.id;


--
-- TOC entry 200 (class 1259 OID 17387)
-- Name: empresa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.empresa (
    cnae character varying(7),
    cnpj character varying(14) NOT NULL,
    nomefantasia character varying(100) NOT NULL,
    razaosocial character varying(100) NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.empresa OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 17394)
-- Name: endereco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.endereco (
    id bigint NOT NULL,
    bairro character varying(50) NOT NULL,
    cep character varying(8) NOT NULL,
    complemento character varying(50),
    logradouro character varying(100) NOT NULL,
    numero integer NOT NULL,
    cidade_id bigint NOT NULL
);


ALTER TABLE public.endereco OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 17392)
-- Name: endereco_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.endereco_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.endereco_id_seq OWNER TO postgres;

--
-- TOC entry 3034 (class 0 OID 0)
-- Dependencies: 201
-- Name: endereco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.endereco_id_seq OWNED BY public.endereco.id;


--
-- TOC entry 204 (class 1259 OID 17402)
-- Name: estado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estado (
    id bigint NOT NULL,
    nome character varying(100) NOT NULL,
    sigla character varying(2) NOT NULL
);


ALTER TABLE public.estado OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 17400)
-- Name: estado_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estado_id_seq OWNER TO postgres;

--
-- TOC entry 3035 (class 0 OID 0)
-- Dependencies: 203
-- Name: estado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estado_id_seq OWNED BY public.estado.id;


--
-- TOC entry 206 (class 1259 OID 17410)
-- Name: evento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.evento (
    id bigint NOT NULL,
    datahorainicio timestamp without time zone NOT NULL,
    datahoratermino timestamp without time zone NOT NULL,
    descricao character varying(255) NOT NULL,
    facilitador character varying(100) NOT NULL,
    imagemdivulgacao text,
    maximodeparticipantes integer NOT NULL,
    minimodeparticipantes integer,
    observacoes character varying(255) NOT NULL,
    valor numeric(19,2) NOT NULL,
    categoriaevento_id bigint NOT NULL,
    empresa_id bigint NOT NULL,
    endereco_id bigint NOT NULL,
    tipoevento_id bigint NOT NULL
);


ALTER TABLE public.evento OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 17408)
-- Name: evento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evento_id_seq OWNER TO postgres;

--
-- TOC entry 3036 (class 0 OID 0)
-- Dependencies: 205
-- Name: evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.evento_id_seq OWNED BY public.evento.id;


--
-- TOC entry 208 (class 1259 OID 17421)
-- Name: meiocontato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.meiocontato (
    id bigint NOT NULL,
    contato character varying(255) NOT NULL,
    evento_id bigint,
    tipomeiocontato_id bigint NOT NULL,
    usuario_id bigint
);


ALTER TABLE public.meiocontato OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 17419)
-- Name: meiocontato_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.meiocontato_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meiocontato_id_seq OWNER TO postgres;

--
-- TOC entry 3037 (class 0 OID 0)
-- Dependencies: 207
-- Name: meiocontato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.meiocontato_id_seq OWNED BY public.meiocontato.id;


--
-- TOC entry 209 (class 1259 OID 17427)
-- Name: participante; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.participante (
    cpf character varying(11) NOT NULL,
    datanascimento timestamp without time zone NOT NULL,
    nome character varying(100) NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.participante OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 17434)
-- Name: tipoevento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipoevento (
    id bigint NOT NULL,
    descricao character varying(100) NOT NULL
);


ALTER TABLE public.tipoevento OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 17432)
-- Name: tipoevento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipoevento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipoevento_id_seq OWNER TO postgres;

--
-- TOC entry 3038 (class 0 OID 0)
-- Dependencies: 210
-- Name: tipoevento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipoevento_id_seq OWNED BY public.tipoevento.id;


--
-- TOC entry 213 (class 1259 OID 17442)
-- Name: tipomeiocontato; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipomeiocontato (
    id bigint NOT NULL,
    descricao character varying(50) NOT NULL
);


ALTER TABLE public.tipomeiocontato OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 17440)
-- Name: tipomeiocontato_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipomeiocontato_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipomeiocontato_id_seq OWNER TO postgres;

--
-- TOC entry 3039 (class 0 OID 0)
-- Dependencies: 212
-- Name: tipomeiocontato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipomeiocontato_id_seq OWNED BY public.tipomeiocontato.id;


--
-- TOC entry 215 (class 1259 OID 17450)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id bigint NOT NULL,
    nomeusuario character varying(20) NOT NULL,
    senha character varying(128) NOT NULL,
    endereco_id bigint NOT NULL
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 17448)
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_seq OWNER TO postgres;

--
-- TOC entry 3040 (class 0 OID 0)
-- Dependencies: 214
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuario.id;


--
-- TOC entry 2843 (class 2604 OID 17376)
-- Name: categoriaevento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoriaevento ALTER COLUMN id SET DEFAULT nextval('public.categoriaevento_id_seq'::regclass);


--
-- TOC entry 2844 (class 2604 OID 17384)
-- Name: cidade id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cidade ALTER COLUMN id SET DEFAULT nextval('public.cidade_id_seq'::regclass);


--
-- TOC entry 2845 (class 2604 OID 17397)
-- Name: endereco id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.endereco ALTER COLUMN id SET DEFAULT nextval('public.endereco_id_seq'::regclass);


--
-- TOC entry 2846 (class 2604 OID 17405)
-- Name: estado id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado ALTER COLUMN id SET DEFAULT nextval('public.estado_id_seq'::regclass);


--
-- TOC entry 2847 (class 2604 OID 17413)
-- Name: evento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento ALTER COLUMN id SET DEFAULT nextval('public.evento_id_seq'::regclass);


--
-- TOC entry 2848 (class 2604 OID 17424)
-- Name: meiocontato id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meiocontato ALTER COLUMN id SET DEFAULT nextval('public.meiocontato_id_seq'::regclass);


--
-- TOC entry 2849 (class 2604 OID 17437)
-- Name: tipoevento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipoevento ALTER COLUMN id SET DEFAULT nextval('public.tipoevento_id_seq'::regclass);


--
-- TOC entry 2850 (class 2604 OID 17445)
-- Name: tipomeiocontato id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipomeiocontato ALTER COLUMN id SET DEFAULT nextval('public.tipomeiocontato_id_seq'::regclass);


--
-- TOC entry 2851 (class 2604 OID 17453)
-- Name: usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id SET DEFAULT nextval('public.usuario_id_seq'::regclass);


--
-- TOC entry 3008 (class 0 OID 17373)
-- Dependencies: 197
-- Data for Name: categoriaevento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categoriaevento (id, descricao) FROM stdin;
\.


--
-- TOC entry 3010 (class 0 OID 17381)
-- Dependencies: 199
-- Data for Name: cidade; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cidade (id, nome, estado_id, codigo_ibge) FROM stdin;
3	Brasiléia	1	120010
5	Capixaba	1	120017
7	Epitaciolândia	1	120025
8	Feijó	1	120030
10	Mâncio Lima	1	120033
12	Marechal Thaumaturgo	1	120035
14	Porto Acre	1	120080
15	Porto Walter	1	120039
17	Rodrigues Alves	1	120042
19	Sena Madureira	1	120050
21	Tarauacá	1	120060
22	Xapuri	1	120070
24	Anadia	2	270020
26	Atalaia	2	270040
28	Barra De São Miguel	2	270060
29	Batalha	2	270070
31	Belo Monte	2	270090
33	Branquinha	2	270110
34	Cacimbinhas	2	270120
36	Campestre	2	270135
38	Campo Grande	2	270150
40	Capela	2	270170
41	Carneiros	2	270180
43	Coité Do Nóia	2	270200
45	Coqueiro Seco	2	270220
47	Craíbas	2	270235
49	Dois Riachos	2	270250
50	Estrela De Alagoas	2	270255
52	Feliz Deserto	2	270270
54	Girau Do Ponciano	2	270290
55	Ibateguara	2	270300
57	Igreja Nova	2	270320
59	Jacaré Dos Homens	2	270340
61	Japaratinga	2	270360
63	Jequiá Da Praia	2	270375
64	Joaquim Gomes	2	270380
66	Junqueiro	2	270400
68	Limoeiro De Anadia	2	270420
70	Major Isidoro	2	270440
71	Mar Vermelho	2	270490
73	Maravilha	2	270460
75	Maribondo	2	270480
77	Matriz De Camaragibe	2	270510
78	Messias	2	270520
80	Monteirópolis	2	270540
82	Novo Lino	2	270560
84	Olho D`Água Do Casado	2	270580
85	Olho D`Água Grande	2	270590
87	Ouro Branco	2	270610
89	Palmeira Dos Índios	2	270630
91	Pariconha	2	270642
92	Paripueira	2	270644
94	Paulo Jacinto	2	270660
96	Piaçabuçu	2	270680
97	Pilar	2	270690
99	Piranhas	2	270710
101	Porto Calvo	2	270730
103	Porto Real Do Colégio	2	270750
104	Quebrangulo	2	270760
106	Roteiro	2	270780
108	Santana Do Ipanema	2	270800
109	Santana Do Mundaú	2	270810
111	São José Da Laje	2	270830
113	São Luís Do Quitunde	2	270850
114	São Miguel Dos Campos	2	270860
116	São Sebastião	2	270880
117	Satuba	2	270890
119	Tanque D`Arca	2	270900
121	Teotônio Vilela	2	270915
122	Traipu	2	270920
124	Viçosa	2	270940
126	Amaturá	3	130006
2713	Princesa Isabel	15	251230
129	Apuí	3	130014
131	Autazes	3	130030
132	Barcelos	3	130040
134	Benjamin Constant	3	130060
136	Boa Vista Do Ramos	3	130068
138	Borba	3	130080
140	Canutama	3	130090
141	Carauari	3	130100
143	Careiro Da Várzea	3	130115
145	Codajás	3	130130
147	Envira	3	130150
149	Guajará	3	130165
150	Humaitá	3	130170
152	Iranduba	3	130185
154	Itamarati	3	130195
156	Japurá	3	130210
158	Jutaí	3	130230
159	Lábrea	3	130240
161	Manaquiri	3	130255
163	Manicoré	3	130270
165	Maués	3	130290
166	Nhamundá	3	130300
168	Novo Airão	3	130320
170	Parintins	3	130340
171	Pauini	3	130350
173	Rio Preto Da Eva	3	130356
175	Santo Antônio Do Içá	3	130370
177	São Paulo De Olivença	3	130390
178	São Sebastião Do Uatumã	3	130395
179	Silves	3	130400
181	Tapauá	3	130410
183	Tonantins	3	130423
185	Urucará	3	130430
187	Amapá	4	160010
188	Calçoene	4	160020
190	Ferreira Gomes	4	160023
192	Laranjal Do Jari	4	160027
194	Mazagão	4	160040
196	Pedra Branca Do Amaparí	4	160015
198	Pracuúba	4	160055
199	Santana	4	160060
201	Tartarugalzinho	4	160070
203	Abaíra	5	290010
204	Abaré	5	290020
206	Adustina	5	290035
208	Aiquara	5	290060
210	Alcobaça	5	290080
211	Almadina	5	290090
213	Amélia Rodrigues	5	290110
215	Anagé	5	290120
217	Andorinha	5	290135
218	Angical	5	290140
220	Antas	5	290160
222	Antônio Gonçalves	5	290180
224	Apuarema	5	290195
225	Araças	5	290205
227	Araci	5	290210
229	Arataca	5	290225
231	Aurelino Leal	5	290240
233	Baixa Grande	5	290260
234	Banzaê	5	290265
236	Barra Da Estiva	5	290280
238	Barra Do Mendes	5	290300
239	Barra Do Rocha	5	290310
241	Barro Alto	5	290323
243	Barro Preto	5	290330
245	Belo Campo	5	290350
246	Biritinga	5	290360
248	Boa Vista Do Tupim	5	290380
250	Bom Jesus Da Serra	5	290395
252	Bonito	5	290405
253	Boquira	5	290410
255	Brejões	5	290430
2716	Quixabá	15	251260
258	Brumado	5	290460
260	Buritirama	5	290475
262	Cabaceiras Do Paraguaçu	5	290485
264	Caculé	5	290500
265	Caém	5	290510
267	Caetité	5	290520
269	Cairu	5	290540
271	Camacan	5	290560
272	Camaçari	5	290570
275	Campo Formoso	5	290600
276	Canápolis	5	290610
278	Canavieiras	5	290630
279	Candeal	5	290640
281	Candiba	5	290660
283	Cansanção	5	290680
285	Capela Do Alto Alegre	5	290685
287	Caraíbas	5	290689
288	Caravelas	5	290690
290	Carinhanha	5	290710
292	Castro Alves	5	290730
293	Catolândia	5	290740
295	Caturama	5	290755
297	Chorrochó	5	290770
299	Cipó	5	290790
300	Coaraci	5	290800
302	Conceição Da Feira	5	290820
304	Conceição Do Coité	5	290840
306	Conde	5	290860
307	Condeúba	5	290870
309	Coração De Maria	5	290890
310	Cordeiros	5	290900
312	Coronel João Sá	5	290920
314	Cotegipe	5	290940
316	Crisópolis	5	290960
317	Cristópolis	5	290970
319	Curaçá	5	290990
321	Dias D`Ávila	5	291005
322	Dom Basílio	5	291010
324	Elísio Medrado	5	291030
326	Entre Rios	5	291050
328	Esplanada	5	291060
329	Euclides Da Cunha	5	291070
331	Fátima	5	291075
333	Feira De Santana	5	291080
334	Filadélfia	5	291085
336	Floresta Azul	5	291100
338	Gandu	5	291120
339	Gavião	5	291125
341	Glória	5	291140
343	Governador Mangabeira	5	291160
344	Guajeru	5	291165
346	Guaratinga	5	291180
348	Iaçu	5	291190
350	Ibicaraí	5	291210
352	Ibicuí	5	291230
353	Ibipeba	5	291240
355	Ibiquera	5	291260
357	Ibirapuã	5	291280
359	Ibitiara	5	291300
360	Ibititá	5	291310
362	Ichu	5	291330
364	Igrapiúna	5	291345
366	Ilhéus	5	291360
368	Ipecaetá	5	291380
369	Ipiaú	5	291390
371	Ipupiara	5	291410
373	Iramaia	5	291430
375	Irará	5	291450
376	Irecê	5	291460
378	Itaberaba	5	291470
380	Itacaré	5	291490
382	Itagi	5	291510
384	Itagimirim	5	291530
387	Itajuípe	5	291550
389	Itamari	5	291570
391	Itanagra	5	291590
392	Itanhém	5	291600
394	Itapé	5	291620
396	Itapetinga	5	291640
398	Itapitanga	5	291660
400	Itarantim	5	291680
401	Itatim	5	291685
403	Itiúba	5	291700
405	Ituaçu	5	291720
407	Iuiú	5	291733
409	Jacaraci	5	291740
410	Jacobina	5	291750
412	Jaguarari	5	291770
414	Jandaíra	5	291790
416	Jeremoabo	5	291810
417	Jiquiriçá	5	291820
420	Juazeiro	5	291840
421	Jucuruçu	5	291845
423	Jussari	5	291855
424	Jussiape	5	291860
426	Lagoa Real	5	291875
428	Lajedão	5	291890
430	Lajedo Do Tabocal	5	291905
431	Lamarão	5	291910
433	Lauro De Freitas	5	291920
435	Licínio De Almeida	5	291940
437	Luís Eduardo Magalhães	5	291955
438	Macajuba	5	291960
440	Macaúbas	5	291980
442	Madre De Deus	5	291992
444	Maiquinique	5	292000
446	Malhada	5	292020
448	Manoel Vitorino	5	292040
449	Mansidão	5	292045
451	Maragogipe	5	292060
453	Marcionílio Souza	5	292080
455	Mata De São João	5	292100
456	Matina	5	292105
458	Miguel Calmon	5	292120
460	Mirangaba	5	292140
461	Mirante	5	292145
463	Morpará	5	292160
465	Mortugaba	5	292180
467	Mucuri	5	292200
469	Mundo Novo	5	292210
470	Muniz Ferreira	5	292220
472	Muritiba	5	292230
473	Mutuípe	5	292240
475	Nilo Peçanha	5	292260
477	Nova Canaã	5	292270
479	Nova Ibiá	5	292275
481	Nova Redenção	5	292285
482	Nova Soure	5	292290
484	Novo Horizonte	5	292303
486	Olindina	5	292310
488	Ouriçangas	5	292330
489	Ourolândia	5	292335
491	Palmeiras	5	292350
492	Paramirim	5	292360
494	Paripiranga	5	292380
496	Paulo Afonso	5	292400
498	Pedrão	5	292410
500	Piatã	5	292430
501	Pilão Arcado	5	292440
503	Pindobaçu	5	292460
505	Piraí Do Norte	5	292467
506	Piripá	5	292470
508	Planaltino	5	292490
510	Poções	5	292510
512	Ponto Novo	5	292525
514	Potiraguá	5	292540
515	Prado	5	292550
518	Presidente Tancredo Neves	5	292575
519	Queimadas	5	292580
521	Quixabeira	5	292593
523	Remanso	5	292600
525	Riachão Das Neves	5	292620
527	Riacho De Santana	5	292640
528	Ribeira Do Amparo	5	292650
530	Ribeirão Do Largo	5	292665
531	Rio De Contas	5	292670
533	Rio Do Pires	5	292690
535	Rodelas	5	292710
536	Ruy Barbosa	5	292720
538	Salvador	5	292740
540	Santa Brígida	5	292760
542	Santa Cruz Da Vitória	5	292780
543	Santa Inês	5	292790
545	Santa Maria Da Vitória	5	292810
547	Santa Teresinha	5	292850
548	Santaluz	5	292800
550	Santanópolis	5	292830
552	Santo Antônio De Jesus	5	292870
554	São Desidério	5	292890
556	São Felipe	5	292910
557	São Félix	5	292900
559	São Francisco Do Conde	5	292920
561	São Gonçalo Dos Campos	5	292930
563	São José Do Jacuípe	5	292937
564	São Miguel Das Matas	5	292940
566	Sapeaçu	5	292960
567	Sátiro Dias	5	292970
569	Saúde	5	292980
571	Sebastião Laranjeiras	5	293000
573	Sento Sé	5	293020
574	Serra Do Ramalho	5	293015
576	Serra Preta	5	293040
577	Serrinha	5	293050
579	Simões Filho	5	293070
581	Sítio Do Quinto	5	293076
583	Souto Soares	5	293080
585	Tanhaçu	5	293100
586	Tanque Novo	5	293105
588	Taperoá	5	293120
590	Teixeira De Freitas	5	293135
592	Teofilândia	5	293150
593	Teolândia	5	293160
595	Tremedal	5	293180
597	Uauá	5	293200
598	Ubaíra	5	293210
600	Ubatã	5	293230
602	Umburanas	5	293245
604	Urandi	5	293260
606	Utinga	5	293280
608	Valente	5	293300
609	Várzea Da Roça	5	293305
611	Várzea Nova	5	293315
613	Vera Cruz	5	293320
615	Vitória Da Conquista	5	293330
616	Wagner	5	293340
619	Xique Xique	5	293360
620	Abaiara	6	230010
621	Acarape	6	230015
623	Acopiara	6	230030
625	Alcântaras	6	230050
627	Alto Santo	6	230070
629	Antonina Do Norte	6	230080
630	Apuiarés	6	230090
632	Aracati	6	230110
634	Ararendá	6	230125
636	Aratuba	6	230140
637	Arneiroz	6	230150
639	Aurora	6	230170
641	Banabuiú	6	230185
2718	Riachão	15	251274
643	Barreira	6	230195
645	Barroquinha	6	230205
647	Beberibe	6	230220
649	Boa Viagem	6	230240
650	Brejo Santo	6	230250
652	Campos Sales	6	230270
654	Capistrano	6	230290
656	Cariré	6	230310
658	Cariús	6	230330
659	Carnaubal	6	230340
661	Catarina	6	230360
663	Caucaia	6	230370
665	Chaval	6	230390
666	Choró	6	230393
668	Coreaú	6	230400
670	Crato	6	230420
672	Cruz	6	230425
674	Ererê	6	230427
675	Eusébio	6	230428
677	Forquilha	6	230435
679	Fortim	6	230445
680	Frecheirinha	6	230450
682	Graça	6	230465
684	Granjeiro	6	230480
686	Guaiúba	6	230495
687	Guaraciaba Do Norte	6	230500
689	Hidrolândia	6	230520
691	Ibaretama	6	230526
693	Ibicuitinga	6	230533
694	Icapuí	6	230535
696	Iguatu	6	230550
698	Ipaporanga	6	230565
700	Ipu	6	230580
701	Ipueiras	6	230590
703	Irauçuba	6	230610
705	Itaitinga	6	230625
707	Itapipoca	6	230640
708	Itapiúna	6	230650
710	Itatira	6	230660
712	Jaguaribara	6	230680
714	Jaguaruana	6	230700
715	Jardim	6	230710
718	Juazeiro Do Norte	6	230730
719	Jucás	6	230740
721	Limoeiro Do Norte	6	230760
722	Madalena	6	230763
724	Maranguape	6	230770
726	Martinópole	6	230790
728	Mauriti	6	230810
729	Meruoca	6	230820
731	Milhã	6	230835
733	Missão Velha	6	230840
735	Monsenhor Tabosa	6	230860
736	Morada Nova	6	230870
738	Morrinhos	6	230890
740	Mulungu	6	230910
742	Nova Russas	6	230930
744	Ocara	6	230945
745	Orós	6	230950
747	Pacatuba	6	230970
749	Pacujá	6	230990
750	Palhano	6	231000
752	Paracuru	6	231020
754	Parambu	6	231030
756	Pedra Branca	6	231050
758	Pentecoste	6	231070
760	Pindoretama	6	231085
762	Pires Ferreira	6	231095
763	Poranga	6	231100
765	Potengi	6	231120
767	Quiterianópolis	6	231126
769	Quixelô	6	231135
771	Quixeré	6	231150
772	Redenção	6	231160
2721	Riacho De Santo Antônio	15	251278
776	Salitre	6	231195
778	Santana Do Acaraú	6	231200
780	São Benedito	6	231230
781	São Gonçalo Do Amarante	6	231240
783	São Luís Do Curu	6	231260
784	Senador Pompeu	6	231270
786	Sobral	6	231290
788	Tabuleiro Do Norte	6	231310
790	Tarrafas	6	231325
791	Tauá	6	231330
793	Tianguá	6	231340
795	Tururu	6	231355
797	Umari	6	231370
798	Umirim	6	231375
800	Uruoca	6	231390
802	Várzea Alegre	6	231400
804	Brasília	7	530010
806	Água Doce Do Norte	8	320016
807	Águia Branca	8	320013
809	Alfredo Chaves	8	320030
811	Anchieta	8	320040
812	Apiacá	8	320050
815	Baixo Guandu	8	320080
816	Barra De São Francisco	8	320090
818	Bom Jesus Do Norte	8	320110
819	Brejetuba	8	320115
821	Cariacica	8	320130
823	Colatina	8	320150
825	Conceição Do Castelo	8	320170
826	Divino De São Lourenço	8	320180
828	Dores Do Rio Preto	8	320200
829	Ecoporanga	8	320210
832	Guaçuí	8	320230
833	Guarapari	8	320240
835	Ibiraçu	8	320250
836	Ibitirama	8	320255
838	Irupi	8	320265
840	Itapemirim	8	320280
842	Iúna	8	320300
843	Jaguaré	8	320305
845	João Neiva	8	320313
847	Linhares	8	320320
849	Marataízes	8	320332
850	Marechal Floriano	8	320334
852	Mimoso Do Sul	8	320340
854	Mucurici	8	320360
856	Muqui	8	320380
857	Nova Venécia	8	320390
859	Pedro Canário	8	320405
861	Piúma	8	320420
862	Ponto Belo	8	320425
864	Rio Bananal	8	320435
866	Santa Leopoldina	8	320450
868	Santa Teresa	8	320460
869	São Domingos Do Norte	8	320465
871	São José Do Calçado	8	320480
872	São Mateus	8	320490
874	Serra	8	320500
875	Sooretama	8	320501
877	Venda Nova Do Imigrante	8	320506
879	Vila Pavão	8	320515
881	Vila Velha	8	320520
882	Vitória	8	320530
884	Abadiânia	9	520010
886	Adelândia	9	520015
888	Água Limpa	9	520020
889	Águas Lindas De Goiás	9	520025
891	Aloândia	9	520050
893	Alto Paraíso De Goiás	9	520060
894	Alvorada Do Norte	9	520080
896	Americano Do Brasil	9	520085
898	Anápolis	9	520110
2724	Salgadinho	15	251300
902	Aparecida Do Rio Doce	9	520145
903	Aporé	9	520150
905	Aragarças	9	520170
907	Araguapaz	9	520215
909	Aruanã	9	520250
910	Aurilândia	9	520260
912	Baliza	9	520310
914	Bela Vista De Goiás	9	520330
916	Bom Jesus De Goiás	9	520350
917	Bonfinópolis	9	520355
919	Brazabrantes	9	520360
921	Buriti Alegre	9	520390
923	Buritinópolis	9	520396
924	Cabeceiras	9	520400
926	Cachoeira De Goiás	9	520420
928	Caçu	9	520430
929	Caiapônia	9	520440
931	Caldazinha	9	520455
933	Campinaçu	9	520465
934	Campinorte	9	520470
936	Campo Limpo De Goiás	9	520485
938	Campos Verdes	9	520495
940	Castelândia	9	520505
941	Catalão	9	520510
943	Cavalcante	9	520530
944	Ceres	9	520540
946	Chapadão Do Céu	9	520547
948	Cocalzinho De Goiás	9	520551
950	Córrego Do Ouro	9	520570
951	Corumbá De Goiás	9	520580
953	Cristalina	9	520620
955	Crixás	9	520640
957	Cumari	9	520660
958	Damianópolis	9	520670
960	Davinópolis	9	520690
962	Divinópolis De Goiás	9	520830
964	Edealina	9	520735
965	Edéia	9	520740
967	Faina	9	520753
969	Firminópolis	9	520780
971	Formosa	9	520800
972	Formoso	9	520810
974	Goianápolis	9	520840
975	Goiandira	9	520850
977	Goiânia	9	520870
979	Goiás	9	520890
981	Gouvelândia	9	520915
982	Guapó	9	520920
984	Guarani De Goiás	9	520940
986	Heitoraí	9	520960
988	Hidrolina	9	520980
989	Iaciara	9	520990
991	Indiara	9	520995
993	Ipameri	9	521010
995	Iporá	9	521020
996	Israelândia	9	521030
998	Itaguari	9	521056
1000	Itajá	9	521080
1002	Itapirapuã	9	521100
1003	Itapuranga	9	521120
1005	Itauçu	9	521140
1007	Ivolândia	9	521160
1009	Jaraguá	9	521180
1011	Jaupaci	9	521200
1012	Jesúpolis	9	521205
1014	Jussara	9	521220
1016	Leopoldo De Bulhões	9	521230
1018	Mairipotaba	9	521260
1020	Mara Rosa	9	521280
1021	Marzagão	9	521290
1023	Maurilândia	9	521300
1025	Minaçu	9	521308
1027	Moiporá	9	521340
2863	Ipojuca	16	260720
1029	Montes Claros De Goiás	9	521370
1031	Montividiu Do Norte	9	521377
1033	Morro Agudo De Goiás	9	521385
1034	Mossâmedes	9	521390
1036	Mundo Novo	9	521405
1038	Nazário	9	521440
1039	Nerópolis	9	521450
1041	Nova América	9	521470
1043	Nova Crixás	9	521483
1045	Nova Iguaçu De Goiás	9	521487
1046	Nova Roma	9	521490
1048	Novo Brasil	9	521520
1050	Novo Planalto	9	521525
1052	Ouro Verde De Goiás	9	521540
1054	Padre Bernardo	9	521560
1056	Palmeiras De Goiás	9	521570
1057	Palmelo	9	521580
1059	Panamá	9	521600
1061	Paraúna	9	521640
1062	Perolândia	9	521645
1064	Pilar De Goiás	9	521690
1066	Piranhas	9	521720
1068	Pires Do Rio	9	521740
1069	Planaltina	9	521760
1071	Porangatu	9	521800
1073	Portelândia	9	521810
1074	Posse	9	521830
1076	Quirinópolis	9	521850
1078	Rianápolis	9	521870
1080	Rio Verde	9	521880
1081	Rubiataba	9	521890
1083	Santa Bárbara De Goiás	9	521910
1085	Santa Fé De Goiás	9	521925
1087	Santa Isabel	9	521935
1088	Santa Rita Do Araguaia	9	521940
1090	Santa Rosa De Goiás	9	521950
1092	Santa Terezinha De Goiás	9	521970
1093	Santo Antônio Da Barra	9	521971
1095	Santo Antônio Do Descoberto	9	521975
1096	São Domingos	9	521980
1098	São João D`Aliança	9	522000
1100	São Luís De Montes Belos	9	522010
1101	São Luíz Do Norte	9	522015
1103	São Miguel Do Passa Quatro	9	522026
1104	São Patrício	9	522028
1106	Senador Canedo	9	522045
1108	Silvânia	9	522060
1109	Simolândia	9	522068
1111	Taquaral De Goiás	9	522100
1113	Terezópolis De Goiás	9	522119
1115	Trindade	9	522140
1116	Trombas	9	522145
1118	Turvelândia	9	522155
1120	Uruaçu	9	522160
1122	Urutaí	9	522180
1124	Varjão	9	522190
1125	Vianópolis	9	522200
1127	Vila Boa	9	522220
1128	Vila Propício	9	522230
1130	Afonso Cunha	10	210010
1132	Alcântara	10	210020
1133	Aldeias Altas	10	210030
1135	Alto Alegre Do Maranhão	10	210043
1137	Alto Parnaíba	10	210050
1138	Amapá Do Maranhão	10	210055
1140	Anajatuba	10	210070
1141	Anapurus	10	210080
1143	Araguanã	10	210087
1145	Arame	10	210095
1147	Axixá	10	210110
1148	Bacabal	10	210120
1152	Balsas	10	210140
1154	Barra Do Corda	10	210160
1156	Bela Vista Do Maranhão	10	210177
1157	Belágua	10	210173
1159	Bequimão	10	210190
1161	Boa Vista Do Gurupi	10	210197
1162	Bom Jardim	10	210200
1164	Bom Lugar	10	210207
1166	Brejo De Areia	10	210215
1167	Buriti	10	210220
1169	Buriticupu	10	210232
1171	Cachoeira Grande	10	210237
1173	Cajari	10	210250
1175	Cândido Mendes	10	210260
1176	Cantanhede	10	210270
1178	Carolina	10	210280
1179	Carutapera	10	210290
1181	Cedral	10	210310
1183	Centro Do Guilherme	10	210315
1185	Chapadinha	10	210320
1186	Cidelândia	10	210325
1188	Coelho Neto	10	210340
1190	Conceição Do Lago Açu	10	210355
1191	Coroatá	10	210360
1193	Davinópolis	10	210375
1195	Duque Bacelar	10	210390
1197	Estreito	10	210405
1199	Fernando Falcão	10	210408
1200	Formosa Da Serra Negra	10	210409
1202	Fortuna	10	210420
1203	Godofredo Viana	10	210430
1205	Governador Archer	10	210450
1207	Governador Eugênio Barros	10	210460
1209	Governador Newton Bello	10	210465
1210	Governador Nunes Freire	10	210467
1212	Grajaú	10	210480
1213	Guimarães	10	210490
1215	Icatu	10	210510
1217	Igarapé Grande	10	210520
1218	Imperatriz	10	210530
1220	Itapecuru Mirim	10	210540
1222	Jatobá	10	210545
1224	João Lisboa	10	210550
1225	Joselândia	10	210560
1227	Lago Da Pedra	10	210570
1229	Lago Dos Rodrigues	10	210594
1230	Lago Verde	10	210590
1232	Lagoa Grande Do Maranhão	10	210596
1234	Lima Campos	10	210600
1235	Loreto	10	210610
1237	Magalhães De Almeida	10	210630
1239	Marajá Do Sena	10	210635
1241	Mata Roma	10	210640
1242	Matinha	10	210650
1244	Matões Do Norte	10	210663
1246	Mirador	10	210670
1247	Miranda Do Norte	10	210675
1249	Monção	10	210690
1251	Morros	10	210710
1252	Nina Rodrigues	10	210720
1254	Nova Iorque	10	210730
1256	Olho D`Água Das Cunhãs	10	210740
1258	Paço Do Lumiar	10	210750
1259	Palmeirândia	10	210760
1261	Parnarama	10	210780
1262	Passagem Franca	10	210790
1264	Paulino Neves	10	210805
1266	Pedreiras	10	210820
1268	Penalva	10	210830
1269	Peri Mirim	10	210840
1271	Pindaré Mirim	10	210850
1273	Pio Xii	10	210870
1276	Porto Franco	10	210900
1277	Porto Rico Do Maranhão	10	210905
1279	Presidente Juscelino	10	210920
1281	Presidente Sarney	10	210927
1282	Presidente Vargas	10	210930
1284	Raposa	10	210945
1286	Ribamar Fiquene	10	210955
1287	Rosário	10	210960
1290	Santa Helena	10	210980
1291	Santa Inês	10	210990
1293	Santa Luzia Do Paruá	10	211003
1295	Santa Rita	10	211020
1296	Santana Do Maranhão	10	211023
1298	Santo Antônio Dos Lopes	10	211030
1300	São Bento	10	211050
1301	São Bernardo	10	211060
1303	São Domingos Do Maranhão	10	211070
1304	São Félix De Balsas	10	211080
1306	São Francisco Do Maranhão	10	211090
1308	São João Do Carú	10	211102
1310	São João Do Soter	10	211107
1311	São João Dos Patos	10	211110
1313	São José Dos Basílios	10	211125
1314	São Luís	10	211130
1316	São Mateus Do Maranhão	10	211150
1317	São Pedro Da Água Branca	10	211153
1319	São Raimundo Das Mangabeiras	10	211160
1321	São Roberto	10	211167
1322	São Vicente Ferrer	10	211170
1324	Senador Alexandre Costa	10	211174
1326	Serrano Do Maranhão	10	211178
1327	Sítio Novo	10	211180
1329	Sucupira Do Riachão	10	211195
1331	Timbiras	10	211210
1332	Timon	10	211220
1334	Tufilândia	10	211227
1336	Turiaçu	10	211240
1337	Turilândia	10	211245
1339	Urbano Santos	10	211260
1341	Viana	10	211280
1343	Vitória Do Mearim	10	211290
1344	Vitorino Freire	10	211300
1346	Abadia Dos Dourados	11	310010
1348	Abre Campo	11	310030
1350	Açucena	11	310050
1351	Água Boa	11	310060
1353	Aguanil	11	310080
1355	Águas Vermelhas	11	310100
1357	Aiuruoca	11	310120
1358	Alagoa	11	310130
1360	Além Paraíba	11	310150
1362	Alfredo Vasconcelos	11	310163
1364	Alpercata	11	310180
1366	Alterosa	11	310200
1367	Alto Caparaó	11	310205
1369	Alto Rio Doce	11	310210
1371	Alvinópolis	11	310230
1373	Amparo Do Serra	11	310250
1374	Andradas	11	310260
1376	Angelândia	11	310285
1378	Antônio Dias	11	310300
1380	Araçaí	11	310320
1381	Aracitaba	11	310330
1383	Araguari	11	310350
1384	Arantina	11	310360
1386	Araporã	11	310375
1388	Araújos	11	310390
1390	Arceburgo	11	310410
1392	Areado	11	310430
1393	Argirita	11	310440
1395	Arinos	11	310450
2726	Santa Cecília	15	251315
1398	Augusto De Lima	11	310480
1399	Baependi	11	310490
1401	Bambuí	11	310510
1403	Bandeira Do Sul	11	310530
1405	Barão De Monte Alto	11	310550
1406	Barbacena	11	310560
1408	Barroso	11	310590
1410	Belmiro Braga	11	310610
1412	Belo Oriente	11	310630
1413	Belo Vale	11	310640
1415	Berizal	11	310665
1417	Betim	11	310670
1419	Bicas	11	310690
1420	Biquinhas	11	310700
1422	Bocaina De Minas	11	310720
1424	Bom Despacho	11	310740
1426	Bom Jesus Da Penha	11	310760
1427	Bom Jesus Do Amparo	11	310770
1429	Bom Repouso	11	310790
1431	Bonfim	11	310810
1433	Bonito De Minas	11	310825
1434	Borda Da Mata	11	310830
1436	Botumirim	11	310850
1438	Brasilândia De Minas	11	310855
1440	Brasópolis	11	310890
1441	Braúnas	11	310880
1443	Bueno Brandão	11	310910
1445	Bugre	11	310925
1446	Buritis	11	310930
1448	Cabeceira Grande	11	310945
1450	Cachoeira Da Prata	11	310960
1452	Cachoeira De Pajeú	11	310270
1454	Caetanópolis	11	310990
1455	Caeté	11	311000
1457	Cajuri	11	311020
1459	Camacho	11	311040
1460	Camanducaia	11	311050
1462	Cambuquira	11	311070
1464	Campanha	11	311090
1466	Campina Verde	11	311110
1468	Campo Belo	11	311120
1470	Campo Florido	11	311140
1471	Campos Altos	11	311150
1473	Cana Verde	11	311190
1475	CanÁpolis	11	311180
1476	Candeias	11	311200
1478	Caparaó	11	311210
1480	Capelinha	11	311230
1482	Capim Branco	11	311250
1484	Capitão Andrade	11	311265
1486	Capitólio	11	311280
1487	Caputira	11	311290
1489	Caranaíba	11	311310
1491	Carangola	11	311330
1493	Carbonita	11	311350
1494	Careaçu	11	311360
1496	Carmésia	11	311380
1498	Carmo Da Mata	11	311400
1500	Carmo Do Cajuru	11	311420
1501	Carmo Do Paranaíba	11	311430
1503	Carmópolis De Minas	11	311450
1504	Carneirinho	11	311455
1506	Carvalhópolis	11	311470
1508	Casa Grande	11	311490
1510	CÁssia	11	311510
1511	Cataguases	11	311530
1514	Catuji	11	311545
1515	Catuti	11	311547
1517	Cedro Do Abaeté	11	311560
1518	Central De Minas	11	311570
1520	ChÁcara	11	311590
1522	Chapada Do Norte	11	311610
2865	Itacuruba	16	260740
1525	Cipotânea	11	311630
1527	Claro Dos Poções	11	311650
1529	Coimbra	11	311670
1531	Comendador Gomes	11	311690
1533	Conceição Da Aparecida	11	311710
1535	Conceição Das Alagoas	11	311730
1536	Conceição Das Pedras	11	311720
1538	Conceição Do Mato Dentro	11	311750
1539	Conceição Do ParÁ	11	311760
1541	Conceição Dos Ouros	11	311780
1543	Confins	11	311787
1544	Congonhal	11	311790
1546	Congonhas Do Norte	11	311810
1548	Conselheiro Lafaiete	11	311830
1550	Consolação	11	311850
1551	Contagem	11	311860
1553	Coração De Jesus	11	311880
1555	Cordislândia	11	311900
1556	Corinto	11	311910
1558	Coromandel	11	311930
1560	Coronel Murta	11	311950
1562	Coronel Xavier Chaves	11	311970
1563	Córrego Danta	11	311980
1565	Córrego Fundo	11	311995
1567	Couto De Magalhães De Minas	11	312010
1568	Crisólita	11	312015
1570	CristÁlia	11	312030
1572	Cristina	11	312050
1574	Cruzeiro Da Fortaleza	11	312070
1575	Cruzília	11	312080
1577	Curral De Dentro	11	312087
1579	Datas	11	312100
1581	Delfinópolis	11	312120
1582	Delta	11	312125
1584	Desterro De Entre Rios	11	312140
1586	Diamantina	11	312160
1588	Dionísio	11	312180
1589	Divinésia	11	312190
1591	Divino Das Laranjeiras	11	312210
1593	Divinópolis	11	312230
1594	Divisa Alegre	11	312235
1596	Divisópolis	11	312245
1597	Dom Bosco	11	312247
1599	Dom Joaquim	11	312260
1601	Dom Viçoso	11	312280
1603	Dores De Campos	11	312300
1604	Dores De Guanhães	11	312310
1606	Dores Do Turvo	11	312330
1607	Doresópolis	11	312340
1609	Durandé	11	312352
1611	Engenheiro Caldas	11	312370
1613	Entre Folhas	11	312385
1614	Entre Rios De Minas	11	312390
1616	Esmeraldas	11	312410
1618	Espinosa	11	312430
1620	Estiva	11	312450
1621	Estrela Dalva	11	312460
1623	Estrela Do Sul	11	312480
1625	Ewbank Da Câmara	11	312500
1626	Extrema	11	312510
1628	Faria Lemos	11	312530
1630	Felisburgo	11	312560
1631	Felixlândia	11	312570
1633	Ferros	11	312590
1635	Florestal	11	312600
1637	Formoso	11	312620
1638	Fortaleza De Minas	11	312630
1640	Francisco Badaró	11	312650
1642	Francisco SÁ	11	312670
1644	Frei Gaspar	11	312680
1645	Frei Inocêncio	11	312690
2867	Itambé	16	260765
1649	Fruta De Leite	11	312707
1650	Frutal	11	312710
1652	Galiléia	11	312730
1654	Glaucilândia	11	312735
1655	Goiabeira	11	312737
1657	Gonçalves	11	312740
1659	Gouveia	11	312760
1661	Grão Mogol	11	312780
1662	Grupiara	11	312790
1664	Guapé	11	312810
1666	Guaraciama	11	312825
1668	Guarani	11	312840
1669	GuararÁ	11	312850
1671	Guaxupé	11	312870
1673	Guimarânia	11	312890
1675	Gurinhatã	11	312910
1677	Iapu	11	312930
1678	Ibertioga	11	312940
1680	Ibiaí	11	312960
1682	Ibiraci	11	312970
1684	Ibitiúra De Minas	11	312990
1686	Icaraí De Minas	11	313005
1687	Igarapé	11	313010
1689	Iguatama	11	313030
1691	Ilicínea	11	313050
1693	Inconfidentes	11	313060
1694	Indaiabira	11	313065
1696	Ingaí	11	313080
1698	Inhaúma	11	313100
1700	Ipaba	11	313115
1701	Ipanema	11	313120
1703	Ipiaçu	11	313140
1705	Iraí De Minas	11	313160
1707	Itabirinha	11	313180
1708	Itabirito	11	313190
1710	Itacarambi	11	313210
1712	Itaipé	11	313230
1714	Itamarandiba	11	313250
1716	Itambacuri	11	313270
1718	Itamogi	11	313290
1719	Itamonte	11	313300
1721	Itanhomi	11	313320
1722	Itaobim	11	313330
1724	Itapecerica	11	313350
1726	Itatiaiuçu	11	313370
1728	Itaúna	11	313380
1729	Itaverava	11	313390
1731	Itueta	11	313410
1733	Itumirim	11	313430
1735	Itutinga	11	313450
1737	Jacinto	11	313470
1738	Jacuí	11	313480
1740	Jaguaraçu	11	313500
1742	Jampruca	11	313507
1743	Janaúba	11	313510
1745	Japaraíba	11	313530
1747	Jeceaba	11	313540
1749	Jequeri	11	313550
1750	Jequitaí	11	313560
1753	Jesuânia	11	313590
1754	Joaíma	11	313600
1756	João Monlevade	11	313620
1758	Joaquim Felício	11	313640
1759	Jordânia	11	313650
1761	José Raydan	11	313655
1763	Juatuba	11	313665
1764	Juiz De Fora	11	313670
1766	Juruaia	11	313690
1768	Ladainha	11	313700
1770	Lagoa Da Prata	11	313720
1771	Lagoa Dos Patos	11	313730
1773	Lagoa Formosa	11	313750
1775	Lagoa Santa	11	313760
1776	Lajinha	11	313770
2870	Itaquitinga	16	260780
1779	Laranjal	11	313800
1781	Lavras	11	313820
1783	Leme Do Prado	11	313835
1785	Liberdade	11	313850
1786	Lima Duarte	11	313860
1788	Lontra	11	313865
1790	Luislândia	11	313868
1792	Luz	11	313880
1793	Machacalis	11	313890
1795	Madre De Deus De Minas	11	313910
1797	Mamonas	11	313925
1799	Manhuaçu	11	313940
1800	Manhumirim	11	313950
1802	Mar De Espanha	11	313980
1804	Maria Da Fé	11	313990
1806	Marilac	11	314010
1808	MaripÁ De Minas	11	314020
1809	Marliéria	11	314030
1811	Martinho Campos	11	314050
1813	Mata Verde	11	314055
1815	Mateus Leme	11	314070
1816	Mathias Lobato	11	317150
1818	Matias Cardoso	11	314085
1820	Mato Verde	11	314100
1821	Matozinhos	11	314110
1823	Medeiros	11	314130
1825	Mendes Pimentel	11	314150
1827	Mesquita	11	314170
1828	Minas Novas	11	314180
1830	Mirabela	11	314200
1832	Miraí	11	314220
1834	Moeda	11	314230
1836	Monjolos	11	314250
1838	Montalvânia	11	314270
1839	Monte Alegre De Minas	11	314280
1841	Monte Belo	11	314300
1843	Monte Formoso	11	314315
1845	Monte Sião	11	314340
1846	Montes Claros	11	314330
1848	Morada Nova De Minas	11	314350
1850	Morro Do Pilar	11	314370
1851	Munhoz	11	314380
1853	Mutum	11	314400
1855	Nacip Raydan	11	314420
1856	Nanuque	11	314430
1858	Natalândia	11	314437
1860	Nazareno	11	314450
1862	Ninheira	11	314465
1864	Nova Era	11	314470
1865	Nova Lima	11	314480
1867	Nova Ponte	11	314500
1869	Nova Resende	11	314510
1871	Nova União	11	313660
1872	Novo Cruzeiro	11	314530
1874	Novorizonte	11	314537
1876	Olhos D`Água	11	314545
1878	Oliveira	11	314560
1879	Oliveira Fortes	11	314570
1881	Oratórios	11	314585
1883	Ouro Branco	11	314590
1884	Ouro Fino	11	314600
1886	Ouro Verde De Minas	11	314620
1888	Padre Paraíso	11	314630
1890	Paineiras	11	314640
1891	Pains	11	314650
1893	Palma	11	314670
1895	Papagaios	11	314690
1897	Paracatu	11	314700
1898	Paraguaçu	11	314720
1900	Paraopeba	11	314740
1902	Passa Tempo	11	314770
1904	Passabém	11	314750
1905	Passos	11	314790
2728	Santa Helena	15	251330
1908	Patrocínio	11	314810
1910	Paula Cândido	11	314830
1912	Pavão	11	314850
1913	Peçanha	11	314860
1915	Pedra Bonita	11	314875
1917	Pedra Do IndaiÁ	11	314890
1919	Pedralva	11	314910
1920	Pedras De Maria Da Cruz	11	314915
1922	Pedro Leopoldo	11	314930
1924	Pequeri	11	314950
1925	Pequi	11	314960
1927	Perdizes	11	314980
1929	Periquito	11	314995
1931	Piau	11	315010
1933	Piedade De Ponte Nova	11	315020
1934	Piedade Do Rio Grande	11	315030
1936	Pimenta	11	315050
1938	Pintópolis	11	315057
1939	Piracema	11	315060
1941	Piranga	11	315080
1943	Piranguinho	11	315100
1944	Pirapetinga	11	315110
1946	Piraúba	11	315130
1948	Piumhi	11	315150
1950	Poço Fundo	11	315170
1952	Pocrane	11	315190
1953	Pompéu	11	315200
1955	Ponto Chique	11	315213
1957	Porteirinha	11	315220
1958	Porto Firme	11	315230
1960	Pouso Alegre	11	315250
1962	Prados	11	315270
1964	PratÁpolis	11	315290
1966	Presidente Bernardes	11	315310
1968	Presidente Kubitschek	11	315330
1969	Presidente OlegÁrio	11	315340
1971	Quartel Geral	11	315370
1972	Queluzito	11	315380
1974	Raul Soares	11	315400
1976	Reduto	11	315415
1978	Resplendor	11	315430
1979	Ressaquinha	11	315440
1981	Riacho Dos Machados	11	315450
1983	Ribeirão Vermelho	11	315470
1985	Rio Casca	11	315490
1986	Rio Do Prado	11	315510
1988	Rio Espera	11	315520
1990	Rio Novo	11	315540
1992	Rio Pardo De Minas	11	315560
1993	Rio Piracicaba	11	315570
1995	Rio Preto	11	315590
1997	RitÁpolis	11	315610
1999	Rodeiro	11	315630
2000	Romaria	11	315640
2002	Rubelita	11	315650
2004	SabarÁ	11	315670
2006	Sacramento	11	315690
2007	Salinas	11	315700
2009	Santa BÁrbara	11	315720
2011	Santa BÁrbara Do Monte Verde	11	315727
2012	Santa BÁrbara Do Tugúrio	11	315730
2014	Santa Cruz De Salinas	11	315737
2016	Santa Efigênia De Minas	11	315750
2017	Santa Fé De Minas	11	315760
2019	Santa Juliana	11	315770
2020	Santa Luzia	11	315780
2022	Santa Maria De Itabira	11	315800
2024	Santa Maria Do Suaçuí	11	315820
2026	Santa Rita De Ibitipoca	11	315940
2027	Santa Rita De Jacutinga	11	315930
2029	Santa Rita Do Itueto	11	315950
2033	Santana Da Vargem	11	315830
2035	Santana De Pirapama	11	315850
2036	Santana Do Deserto	11	315860
2038	Santana Do Jacaré	11	315880
2040	Santana Do Paraíso	11	315895
2041	Santana Do Riacho	11	315900
2043	Santo Antônio Do Amparo	11	315990
2045	Santo Antônio Do Grama	11	316010
2046	Santo Antônio Do Itambé	11	316020
2048	Santo Antônio Do Monte	11	316040
2050	Santo Antônio Do Rio Abaixo	11	316050
2051	Santo Hipólito	11	316060
2053	São Bento Abade	11	316080
2055	São Domingos Das Dores	11	316095
2056	São Domingos Do Prata	11	316100
2058	São Francisco	11	316110
2060	São Francisco De Sales	11	316130
2062	São Geraldo	11	316150
2063	São Geraldo Da Piedade	11	316160
2065	São Gonçalo Do Abaeté	11	316170
2066	São Gonçalo Do ParÁ	11	316180
2068	São Gonçalo Do Rio Preto	11	312550
2070	São Gotardo	11	316210
2071	São João Batista Do Glória	11	316220
2073	São João Da Mata	11	316230
2075	São João Das Missões	11	316245
2076	São João Del Rei	11	316250
2078	São João Do Manteninha	11	316257
2080	São João Do Pacuí	11	316265
2081	São João Do Paraíso	11	316270
2083	São João Nepomuceno	11	316290
2085	São José Da Barra	11	316294
2086	São José Da Lapa	11	316295
2088	São José Da Varginha	11	316310
2090	São José Do Divino	11	316330
2091	São José Do Goiabal	11	316340
2093	São José Do Mantimento	11	316360
2095	São Miguel Do Anta	11	316380
2096	São Pedro Da União	11	316390
2098	São Pedro Dos Ferros	11	316400
2099	São Romão	11	316420
2101	São Sebastião Da Bela Vista	11	316440
2103	São Sebastião Do Anta	11	316447
2104	São Sebastião Do Maranhão	11	316450
2106	São Sebastião Do Paraíso	11	316470
2108	São Sebastião Do Rio Verde	11	316490
2109	São Thomé Das Letras	11	316520
2110	São Tiago	11	316500
2112	São Vicente De Minas	11	316530
2114	SardoÁ	11	316550
2115	Sarzedo	11	316553
2117	Senador Amaral	11	316557
2119	Senador Firmino	11	316570
2121	Senador Modestino Gonçalves	11	316590
2122	Senhora De Oliveira	11	316600
2124	Senhora Dos Remédios	11	316620
2126	Seritinga	11	316640
2127	Serra Azul De Minas	11	316650
2129	Serra Do Salitre	11	316680
2131	Serrania	11	316690
2133	Serranos	11	316700
2134	Serro	11	316710
2136	Setubinha	11	316555
2137	Silveirânia	11	316730
2139	Simão Pereira	11	316750
2141	SobrÁlia	11	316770
2143	Tabuleiro	11	316790
2144	Taiobeiras	11	316800
2731	Santa Rita	15	251370
2147	Tapiraí	11	316820
2149	Tarumirim	11	316840
2150	Teixeiras	11	316850
2152	Timóteo	11	316870
2154	Tiros	11	316890
2155	Tocantins	11	316900
2157	Toledo	11	316910
2159	Três Corações	11	316930
2161	Três Pontas	11	316940
2162	Tumiritinga	11	316950
2164	Turmalina	11	316970
2166	UbÁ	11	316990
2167	Ubaí	11	317000
2169	Uberaba	11	317010
2171	Umburatiba	11	317030
2173	União De Minas	11	317043
2174	Uruana De Minas	11	317047
2176	Urucuia	11	317052
2178	Vargem Bonita	11	317060
2180	Varginha	11	317070
2181	Varjão De Minas	11	317075
2183	Varzelândia	11	317090
2184	Vazante	11	317100
2186	Veredinha	11	317107
2188	Vermelho Novo	11	317115
2190	Viçosa	11	317130
2191	Vieiras	11	317140
2193	Virgínia	11	317170
2195	Virgolândia	11	317190
2197	Volta Grande	11	317210
2198	Wenceslau Braz	11	317220
2200	Alcinópolis	12	500025
2202	Anastácio	12	500070
2203	Anaurilândia	12	500080
2205	Antônio João	12	500090
2207	Aquidauana	12	500110
2209	Bandeirantes	12	500150
2210	Bataguassu	12	500190
2212	Bela Vista	12	500210
2214	Bonito	12	500220
2216	Caarapó	12	500240
2217	Camapuã	12	500260
2219	Caracol	12	500280
2221	Chapadão Do Sul	12	500295
2222	Corguinho	12	500310
2224	Corumbá	12	500320
2226	Coxim	12	500330
2227	Deodápolis	12	500345
2229	Douradina	12	500350
2231	Eldorado	12	500375
2233	Figueirão	12	500390
2234	Glória De Dourados	12	500400
2236	Iguatemi	12	500430
2237	Inocência	12	500440
2239	Itaquiraí	12	500460
2241	Japorã	12	500480
2243	Jardim	12	500500
2245	Juti	12	500515
2246	Ladário	12	500520
2248	Maracaju	12	500540
2250	Mundo Novo	12	500568
2252	Nioaque	12	500580
2254	Nova Andradina	12	500620
2255	Novo Horizonte Do Sul	12	500625
2257	Paranhos	12	500635
2259	Ponta Porã	12	500660
2261	Ribas Do Rio Pardo	12	500710
2262	Rio Brilhante	12	500720
2264	Rio Verde De Mato Grosso	12	500740
2266	Santa Rita Do Pardo	12	500755
2268	Selvíria	12	500780
2269	Sete Quedas	12	500770
2271	Sonora	12	500793
2733	Santana De Mangueira	15	251350
2274	Terenos	12	500800
2276	Vicentina	12	500840
2278	Água Boa	13	510020
2280	Alto Araguaia	13	510030
2281	Alto Boa Vista	13	510035
2283	Alto Paraguai	13	510050
2285	Apiacás	13	510080
2286	Araguaiana	13	510100
2288	Araputanga	13	510125
2290	Aripuanã	13	510140
2292	Barra Do Bugres	13	510170
2294	Bom Jesus Do Araguaia	13	510185
2295	Brasnorte	13	510190
2297	Campinápolis	13	510260
2299	Campo Verde	13	510267
2300	Campos De Júlio	13	510268
2302	Canarana	13	510270
2304	Castanheira	13	510285
2306	Cláudia	13	510305
2307	Cocalinho	13	510310
2309	Colniza	13	510325
2310	Comodoro	13	510330
2313	Cotriguaçu	13	510337
2314	Cuiabá	13	510340
2316	Denise	13	510345
2318	Dom Aquino	13	510360
2320	Figueirópolis D`Oeste	13	510380
2321	Gaúcha Do Norte	13	510385
2323	Glória D`Oeste	13	510395
2325	Guiratinga	13	510420
2327	Ipiranga Do Norte	13	510452
2328	Itanhangá	13	510454
2330	Itiquira	13	510460
2332	Jangada	13	510490
2334	Juara	13	510510
2335	Juína	13	510515
2337	Juscimeira	13	510520
2339	Lucas Do Rio Verde	13	510525
2341	Marcelândia	13	510558
2342	Matupá	13	510560
2344	Nobres	13	510590
2346	Nossa Senhora Do Livramento	13	510610
2348	Nova Brasilândia	13	510620
2349	Nova Canaã Do Norte	13	510621
2351	Nova Lacerda	13	510618
2353	Nova Maringá	13	510890
2355	Nova Mutum	13	510622
2356	Nova Nazaré	13	510617
2358	Nova Santa Helena	13	510619
2360	Nova Xavantina	13	510625
2362	Novo Mundo	13	510626
2363	Novo Santo Antônio	13	510631
2365	Paranaíta	13	510629
2367	Pedra Preta	13	510637
2369	Planalto Da Serra	13	510645
2370	Poconé	13	510650
2372	Ponte Branca	13	510670
2374	Porto Alegre Do Norte	13	510677
2376	Porto Esperidião	13	510682
2377	Porto Estrela	13	510685
2379	Primavera Do Leste	13	510704
2381	Reserva Do Cabaçal	13	510715
2383	Ribeirãozinho	13	510719
2384	Rio Branco	13	510720
2386	Rondonópolis	13	510760
2388	Salto Do Céu	13	510775
2389	Santa Carmem	13	510724
2391	Santa Rita Do Trivelato	13	510776
2393	Santo Afonso	13	510726
2395	Santo Antônio Do Leverger	13	510780
2736	São Bentinho	15	251392
2399	São José Do Xingu	13	510735
2401	São Pedro Da Cipa	13	510740
2402	Sapezal	13	510787
2403	Serra Nova Dourada	13	510788
2405	Sorriso	13	510792
2407	Tangará Da Serra	13	510795
2409	Terra Nova Do Norte	13	510805
2410	Tesouro	13	510810
2412	União Do Sul	13	510830
2414	Várzea Grande	13	510840
2415	Vera	13	510850
2417	Vila Rica	13	510860
2419	Abel Figueiredo	14	150013
2421	Afuá	14	150030
2422	Água Azul Do Norte	14	150034
2424	Almeirim	14	150050
2426	Anajás	14	150070
2428	Anapu	14	150085
2430	Aurora Do Pará	14	150095
2431	Aveiro	14	150100
2433	Baião	14	150120
2435	Barcarena	14	150130
2436	Belém	14	150140
2438	Benevides	14	150150
2440	Bonito	14	150160
2442	Brasil Novo	14	150172
2444	Breu Branco	14	150178
2445	Breves	14	150180
2447	Cachoeira Do Arari	14	150200
2449	Cametá	14	150210
2450	Canaã Dos Carajás	14	150215
2452	Capitão Poço	14	150230
2454	Chaves	14	150250
2456	Conceição Do Araguaia	14	150270
2457	Concórdia Do Pará	14	150275
2459	Curionópolis	14	150277
2461	Curuá	14	150285
2463	Dom Eliseu	14	150293
2465	Faro	14	150300
2466	Floresta Do Araguaia	14	150304
2468	Goianésia Do Pará	14	150309
2469	Gurupá	14	150310
2471	Igarapé Miri	14	150330
2473	Ipixuna Do Pará	14	150345
2475	Itaituba	14	150360
2476	Itupiranga	14	150370
2478	Jacundá	14	150380
2480	Limoeiro Do Ajuru	14	150400
2482	Magalhães Barata	14	150410
2484	Maracanã	14	150430
2485	Marapanim	14	150440
2487	Medicilândia	14	150445
2489	Mocajuba	14	150460
2491	Monte Alegre	14	150480
2492	Muaná	14	150490
2494	Nova Ipixuna	14	150497
2496	Novo Progresso	14	150503
2498	Óbidos	14	150510
2499	Oeiras Do Pará	14	150520
2501	Ourém	14	150540
2503	Pacajá	14	150548
2504	Palestina Do Pará	14	150549
2506	Parauapebas	14	150553
2508	Peixe Boi	14	150560
2509	Piçarra	14	150563
2511	Ponta De Pedras	14	150570
2513	Porto De Moz	14	150590
2515	Primavera	14	150610
2517	Redenção	14	150613
2518	Rio Maria	14	150616
2520	Rurópolis	14	150619
2522	Salvaterra	14	150630
2872	Jaqueira	16	260795
2526	Santa Luzia Do Pará	14	150655
2527	Santa Maria Das Barreiras	14	150658
2529	Santana Do Araguaia	14	150670
2530	Santarém	14	150680
2532	Santo Antônio Do Tauá	14	150700
2534	São Domingos Do Araguaia	14	150715
2536	São Félix Do Xingu	14	150730
2537	São Francisco Do Pará	14	150740
2539	São João Da Ponta	14	150746
2541	São João Do Araguaia	14	150750
2542	São Miguel Do Guamá	14	150760
2544	Sapucaia	14	150775
2545	Senador José Porfírio	14	150780
2547	Tailândia	14	150795
2549	Terra Santa	14	150797
2550	Tomé Açu	14	150800
2552	Trairão	14	150805
2554	Tucuruí	14	150810
2556	Uruará	14	150815
2557	Vigia	14	150820
2559	Vitória Do Xingu	14	150835
2561	Água Branca	15	250010
2563	Alagoa Grande	15	250030
2565	Alagoinha	15	250050
2567	Algodão De Jandaíra	15	250057
2568	Alhandra	15	250060
2570	Aparecida	15	250077
2572	Arara	15	250090
2574	Areia	15	250110
2576	Areial	15	250120
2577	Aroeiras	15	250130
2579	Baía Da Traição	15	250140
2581	Baraúna	15	250153
2583	Barra De Santana	15	250157
2584	Barra De São Miguel	15	250170
2586	Belém	15	250190
2588	Bernardino Batista	15	250205
2589	Boa Ventura	15	250210
2591	Bom Jesus	15	250220
2593	Bonito De Santa Fé	15	250240
2595	Borborema	15	250270
2596	Brejo Do Cruz	15	250280
2598	Caaporã	15	250300
2600	Cabedelo	15	250320
2602	Cacimba De Areia	15	250340
2603	Cacimba De Dentro	15	250350
2605	Caiçara	15	250360
2607	Cajazeirinhas	15	250375
2609	Camalaú	15	250390
2610	Campina Grande	15	250400
2612	Caraúbas	15	250407
2614	Casserengue	15	250415
2616	Catolé Do Rocha	15	250430
2617	Caturité	15	250435
2619	Condado	15	250450
2621	Congo	15	250470
2623	Coxixola	15	250485
2625	Cubati	15	250500
2626	Cuité	15	250510
2628	Cuitegi	15	250520
2630	Curral Velho	15	250530
2631	Damião	15	250535
2633	Diamante	15	250560
2634	Dona Inês	15	250570
2636	Emas	15	250590
2638	Fagundes	15	250610
2640	Gado Bravo	15	250625
2641	Guarabira	15	250630
2643	Gurjão	15	250650
2645	Igaracy	15	250260
2647	Ingá	15	250680
2648	Itabaiana	15	250690
2714	Puxinanã	15	251240
2715	Queimadas	15	251250
2717	Remígio	15	251270
2719	Riachão Do Bacamarte	15	251275
2720	Riachão Do Poço	15	251276
2722	Riacho Dos Cavalos	15	251280
2723	Rio Tinto	15	251290
2725	Salgado De São Félix	15	251310
2727	Santa Cruz	15	251320
2729	Santa Inês	15	251335
2730	Santa Luzia	15	251340
2732	Santa Teresinha	15	251380
2734	Santana Dos Garrotes	15	251360
2735	Santo André	15	251385
2737	São Bento	15	251390
2739	São Domingos Do Cariri	15	251394
2740	São Francisco	15	251398
2742	São João Do Rio Do Peixe	15	250070
2743	São João Do Tigre	15	251410
2745	São José De Caiana	15	251430
2747	São José De Piranhas	15	251450
2748	São José De Princesa	15	251455
2750	São José Do Brejo Do Cruz	15	251465
2751	São José Do Sabugi	15	251470
2753	São José Dos Ramos	15	251445
2754	São Mamede	15	251490
2756	São Sebastião De Lagoa De Roça	15	251510
2758	Sapé	15	251530
2652	Jacaraú	15	250730
2654	João Pessoa	15	250750
2656	Juarez Távora	15	250760
2657	Juazeirinho	15	250770
2659	Juripiranga	15	250790
2660	Juru	15	250800
2662	Lagoa De Dentro	15	250820
2664	Lastro	15	250840
2666	Logradouro	15	250855
2668	Mãe D`Água	15	250870
2669	Malta	15	250880
2671	Manaíra	15	250900
2673	Mari	15	250910
2675	Massaranduba	15	250920
2676	Mataraca	15	250930
2678	Mato Grosso	15	250937
2680	Mogeiro	15	250940
2682	Monte Horebe	15	250960
2683	Monteiro	15	250970
2685	Natuba	15	250990
2687	Nova Floresta	15	251010
2689	Nova Palmeira	15	251030
2691	Olivedos	15	251050
2693	Parari	15	251065
2694	Passagem	15	251070
2696	Paulista	15	251090
2698	Pedra Lavrada	15	251110
2700	Pedro Régis	15	251272
2701	Piancó	15	251130
2703	Pilar	15	251150
2705	Pilõezinhos	15	251170
2707	Pitimbu	15	251190
2708	Pocinhos	15	251200
2710	Poço De José De Moura	15	251207
2712	Prata	15	251220
2759	Seridó	15	251540
2761	Serra Da Raiz	15	251560
2762	Serra Grande	15	251570
2764	Serraria	15	251590
2766	Sobrado	15	251597
2767	Solânea	15	251600
2769	Sossêgo	15	251615
2771	Sumé	15	251630
2773	Taperoá	15	251650
2774	Tavares	15	251660
2777	Triunfo	15	251680
2779	Umbuzeiro	15	251700
2780	Várzea	15	251710
2782	Vista Serrana	15	250550
2784	Abreu E Lima	16	260005
2786	Afrânio	16	260020
2787	Agrestina	16	260030
2789	Águas Belas	16	260050
2791	Aliança	16	260070
2793	Amaraji	16	260090
2794	Angelim	16	260100
2796	Araripina	16	260110
2798	Barra De Guabiraba	16	260130
2800	Belém De Maria	16	260150
2802	Belo Jardim	16	260170
2803	Betânia	16	260180
2805	Bodocó	16	260200
2807	Bom Jardim	16	260220
2808	Bonito	16	260230
2810	Brejinho	16	260250
2812	Buenos Aires	16	260270
2813	Buíque	16	260280
2815	Cabrobó	16	260300
2817	Caetés	16	260320
2818	Calçado	16	260330
2820	Camaragibe	16	260345
2822	Camutanga	16	260360
2823	Canhotinho	16	260370
2825	Carnaíba	16	260390
2827	Carpina	16	260400
2828	Caruaru	16	260410
2830	Catende	16	260420
2832	Chã De Alegria	16	260440
2834	Condado	16	260460
2836	Cortês	16	260480
2837	Cumaru	16	260490
2839	Custódia	16	260510
2841	Escada	16	260520
2843	Feira Nova	16	260540
2845	Ferreiros	16	260550
2846	Flores	16	260560
2848	Frei Miguelinho	16	260580
2850	Garanhuns	16	260600
2852	Goiana	16	260620
2853	Granito	16	260630
2855	Iati	16	260650
2857	Ibirajuba	16	260670
2859	Iguaraci	16	260690
2861	Inajá	16	260700
2862	Ingazeira	16	260710
2864	Ipubi	16	260730
2866	Itaíba	16	260750
2868	Itapetim	16	260770
2869	Itapissuma	16	260775
2871	Jaboatão Dos Guararapes	16	260790
2873	Jataúba	16	260800
2875	João Alfredo	16	260810
2877	Jucati	16	260825
2878	Jupi	16	260830
2880	Lagoa Do Carro	16	260845
2882	Lagoa Do Ouro	16	260860
2884	Lagoa Grande	16	260875
2885	Lajedo	16	260880
2887	Macaparana	16	260900
2889	Manari	16	260915
2891	Mirandiba	16	260930
2893	Moreno	16	260940
2894	Nazaré Da Mata	16	260950
2896	Orobó	16	260970
2898	Ouricuri	16	260990
2899	Palmares	16	261000
2901	Panelas	16	261020
2903	Parnamirim	16	261040
2905	Paudalho	16	261060
5414	Valparaíso	26	355630
2908	Pesqueira	16	261090
2909	Petrolândia	16	261100
2911	Poção	16	261120
2913	Primavera	16	261140
2915	Quixaba	16	261153
2916	Recife	16	261160
2918	Ribeirão	16	261180
2920	Sairé	16	261200
2922	Salgueiro	16	261220
2923	Saloá	16	261230
2925	Santa Cruz	16	261245
2927	Santa Cruz Do Capibaribe	16	261250
2929	Santa Maria Da Boa Vista	16	261260
2930	Santa Maria Do Cambucá	16	261270
2932	São Benedito Do Sul	16	261290
2934	São Caitano	16	261310
2935	São João	16	261320
2937	São José Da Coroa Grande	16	261340
2939	São José Do Egito	16	261360
2941	São Vicente Ferrer	16	261380
2942	Serra Talhada	16	261390
2944	Sertânia	16	261410
2946	Solidão	16	261440
2947	Surubim	16	261450
2949	Tacaimbó	16	261470
2951	Tamandaré	16	261485
2953	Terezinha	16	261510
2954	Terra Nova	16	261520
2956	Toritama	16	261540
2958	Trindade	16	261560
2960	Tupanatinga	16	261580
2961	Tuparetama	16	261590
2963	Verdejante	16	261610
2965	Vertentes	16	261620
2967	Vitória De Santo Antão	16	261640
2968	Xexéu	16	261650
2970	Agricolândia	17	220010
2972	Alagoinha Do Piauí	17	220025
2974	Alto Longá	17	220030
2975	Altos	17	220040
2977	Amarante	17	220050
2979	Anísio De Abreu	17	220070
2981	Aroazes	17	220090
2982	Aroeiras Do Itaim	17	220095
2984	Assunção Do Piauí	17	220105
2985	Avelino Lopes	17	220110
2987	Barra D`Alcântara	17	220117
2989	Barreiras Do Piauí	17	220130
2991	Batalha	17	220150
2993	Belém Do Piauí	17	220157
2994	Beneditinos	17	220160
2996	Betânia Do Piauí	17	220173
2997	Boa Hora	17	220177
2999	Bom Jesus	17	220190
3001	Bonfim Do Piauí	17	220192
3003	Brasileira	17	220196
3004	Brejo Do Piauí	17	220198
3006	Buriti Dos Montes	17	220202
3008	Cajazeiras Do Piauí	17	220207
3009	Cajueiro Da Praia	17	220208
3011	Campinas Do Piauí	17	220210
3012	Campo Alegre Do Fidalgo	17	220211
3014	Campo Largo Do Piauí	17	220217
3015	Campo Maior	17	220220
3018	Capitão De Campos	17	220240
3019	Capitão Gervásio Oliveira	17	220245
3021	Caraúbas Do Piauí	17	220253
3023	Castelo Do Piauí	17	220260
3024	Caxingó	17	220265
3026	Cocal De Telha	17	220271
3028	Coivaras	17	220273
5416	Vargem Grande Do Sul	26	355640
3032	Coronel José Dias	17	220285
3033	Corrente	17	220290
3035	Cristino Castro	17	220310
3037	Currais	17	220323
3039	Curralinhos	17	220325
3040	Demerval Lobão	17	220330
3042	Dom Expedito Lopes	17	220340
3044	Domingos Mourão	17	220342
3046	Eliseu Martins	17	220360
3047	Esperantina	17	220370
3049	Flores Do Piauí	17	220380
3051	Floriano	17	220390
3053	Francisco Ayres	17	220410
3054	Francisco Macedo	17	220415
3056	Fronteiras	17	220430
3058	Gilbués	17	220440
3059	Guadalupe	17	220450
3061	Hugo Napoleão	17	220460
3063	Inhuma	17	220470
3065	Isaías Coelho	17	220490
3067	Itaueira	17	220510
3068	Jacobina Do Piauí	17	220515
3070	Jardim Do Mulato	17	220525
3072	Jerumenha	17	220530
3073	João Costa	17	220535
3075	Joca Marques	17	220545
3077	Juazeiro Do Piauí	17	220551
3079	Jurema	17	220553
3080	Lagoa Alegre	17	220555
3082	Lagoa Do Barro Do Piauí	17	220556
3084	Lagoa Do Sítio	17	220559
3085	Lagoinha Do Piauí	17	220554
3087	Luís Correia	17	220570
3089	Madeiro	17	220585
3091	Marcolândia	17	220595
3092	Marcos Parente	17	220600
3094	Matias Olímpio	17	220610
3096	Miguel Leão	17	220630
3098	Monsenhor Gil	17	220640
3099	Monsenhor Hipólito	17	220650
3101	Morro Cabeça No Tempo	17	220665
3103	Murici Dos Portelas	17	220669
3104	Nazaré Do Piauí	17	220670
3106	Nossa Senhora De Nazaré	17	220675
3108	Nova Santa Rita	17	220795
3109	Novo Oriente Do Piauí	17	220690
3111	Oeiras	17	220700
3113	Padre Marcos	17	220720
3114	Paes Landim	17	220730
3116	Palmeira Do Piauí	17	220740
3117	Palmeirais	17	220750
3119	Parnaguá	17	220760
3121	Passagem Franca Do Piauí	17	220775
3123	Pau D`Arco Do Piauí	17	220779
3125	Pavussu	17	220785
3126	Pedro Ii	17	220790
3128	Picos	17	220800
3130	Pio Ix	17	220820
3132	Piripiri	17	220840
3133	Porto	17	220850
3135	Prata Do Piauí	17	220860
3137	Redenção Do Gurguéia	17	220870
3139	Riacho Frio	17	220885
3140	Ribeira Do Piauí	17	220887
3142	Rio Grande Do Piauí	17	220900
3144	Santa Cruz Dos Milagres	17	220915
3145	Santa Filomena	17	220920
3147	Santa Rosa Do Piauí	17	220937
3149	Santo Antônio De Lisboa	17	220940
3273	Colorado	18	410590
3152	São Braz Do Piauí	17	220955
3154	São Francisco De Assis Do Piauí	17	220965
3156	São Gonçalo Do Gurguéia	17	220975
3158	São João Da Canabrava	17	220985
3159	São João Da Fronteira	17	220987
3161	São João Da Varjota	17	220995
3163	São João Do Piauí	17	221000
3164	São José Do Divino	17	221005
3166	São José Do Piauí	17	221020
3167	São Julião	17	221030
3169	São Luis Do Piauí	17	221037
3171	São Miguel Do Fidalgo	17	221039
3172	São Miguel Do Tapuio	17	221040
3174	São Raimundo Nonato	17	221060
3176	Sebastião Leal	17	221063
3177	Sigefredo Pacheco	17	221065
3179	Simplício Mendes	17	221080
3181	Sussuapara	17	221093
3183	Tanque Do Piauí	17	221097
3184	Teresina	17	221100
3186	Uruçuí	17	221120
3188	Várzea Branca	17	221135
3190	Vera Mendes	17	221150
3191	Vila Nova Do Piauí	17	221160
3193	Abatiá	18	410010
3195	Agudos Do Sul	18	410030
3197	Altamira Do Paraná	18	410045
3198	Alto Paraíso	18	412862
3200	Alto Piquiri	18	410070
3201	Altônia	18	410050
3203	Amaporã	18	410090
3205	Anahy	18	410105
3207	Ângulo	18	410115
3208	Antonina	18	410120
3210	Apucarana	18	410140
3212	Arapoti	18	410160
3214	Araruna	18	410170
3215	Araucária	18	410180
3217	Assaí	18	410190
3219	Astorga	18	410210
3221	Balsa Nova	18	410230
3222	Bandeirantes	18	410240
3224	Barra Do Jacaré	18	410270
3226	Bela Vista Da Caroba	18	410275
3228	Bituruna	18	410290
3229	Boa Esperança	18	410300
3231	Boa Ventura De São Roque	18	410304
3233	Bocaiúva Do Sul	18	410310
3234	Bom Jesus Do Sul	18	410315
3236	Bom Sucesso Do Sul	18	410322
3238	Braganey	18	410335
3239	Brasilândia Do Sul	18	410337
3241	Cafelândia	18	410345
3243	Califórnia	18	410350
3245	Cambé	18	410370
3246	Cambira	18	410380
3248	Campina Do Simão	18	410395
3250	Campo Bonito	18	410405
3252	Campo Largo	18	410420
3253	Campo Magro	18	410425
3255	Cândido De Abreu	18	410440
3257	Cantagalo	18	410445
3258	Capanema	18	410450
3260	Carambeí	18	410465
3262	Cascavel	18	410480
3263	Castro	18	410490
3265	Centenário Do Sul	18	410510
3267	Céu Azul	18	410530
3269	Cianorte	18	410550
3271	Clevelândia	18	410570
3272	Colombo	18	410580
5419	Vera Cruz	26	355660
3276	Contenda	18	410620
3278	Cornélio Procópio	18	410640
3280	Coronel Vivida	18	410650
3281	Corumbataí Do Sul	18	410655
3283	Cruzeiro Do Iguaçu	18	410657
3285	Cruzeiro Do Sul	18	410670
3286	Cruzmaltina	18	410685
3288	Curiúva	18	410700
3290	Diamante Do Norte	18	410710
3292	Dois Vizinhos	18	410720
3293	Douradina	18	410725
3295	Doutor Ulysses	18	412863
3297	Engenheiro Beltrão	18	410750
3298	Entre Rios Do Oeste	18	410753
3300	Espigão Alto Do Iguaçu	18	410754
3302	Faxinal	18	410760
3304	Fênix	18	410770
3305	Fernandes Pinheiro	18	410773
3307	Flor Da Serra Do Sul	18	410785
3309	Floresta	18	410790
3311	Flórida	18	410810
3313	Foz Do Iguaçu	18	410830
3314	Foz Do Jordão	18	410845
3316	Francisco Beltrão	18	410840
3318	Godoy Moreira	18	410855
3319	Goioerê	18	410860
3321	Grandes Rios	18	410870
3323	Guairaçá	18	410890
3325	Guapirama	18	410900
3326	Guaporema	18	410910
3328	Guaraniaçu	18	410930
3330	Guaraqueçaba	18	410950
3332	Honório Serpa	18	410965
3334	Ibema	18	410975
3335	Ibiporã	18	410980
3337	Iguaraçu	18	411000
3339	Imbaú	18	411007
3341	Inácio Martins	18	411020
3343	Indianópolis	18	411040
3344	Ipiranga	18	411050
3346	Iracema Do Oeste	18	411065
3348	Iretama	18	411080
3350	Itaipulândia	18	411095
3352	Itambé	18	411110
3354	Itaperuçu	18	411125
3355	Itaúna Do Sul	18	411130
3357	Ivaiporã	18	411150
3359	Ivatuba	18	411160
3360	Jaboti	18	411170
3362	Jaguapitã	18	411190
3364	Jandaia Do Sul	18	411210
3366	Japira	18	411230
3368	Jardim Alegre	18	411250
3370	Jataizinho	18	411270
3371	Jesuítas	18	411275
3373	Jundiaí Do Sul	18	411290
3375	Jussara	18	411300
3376	Kaloré	18	411310
3378	Laranjal	18	411325
3380	Leópolis	18	411340
3382	Lindoeste	18	411345
3383	Loanda	18	411350
3385	Londrina	18	411370
3387	Lunardelli	18	411375
3389	Mallet	18	411390
3390	Mamborê	18	411400
3392	Mandaguari	18	411420
3394	Manfrinópolis	18	411435
3396	Manoel Ribas	18	411450
3398	Maria Helena	18	411470
3399	Marialva	18	411480
3401	Marilena	18	411500
5556	Sucupira	27	172085
3404	Mariópolis	18	411530
3406	Marmeleiro	18	411540
3408	Marumbi	18	411550
3409	Matelândia	18	411560
3411	Mato Rico	18	411573
3413	Medianeira	18	411580
3415	Mirador	18	411590
3416	Miraselva	18	411600
3418	Moreira Sales	18	411610
3420	Munhoz De Melo	18	411630
3422	Nova Aliança Do Ivaí	18	411650
3424	Nova Aurora	18	411670
3425	Nova Cantu	18	411680
3427	Nova Esperança Do Sudoeste	18	411695
3429	Nova Laranjeiras	18	411705
3431	Nova Olímpia	18	411720
3432	Nova Prata Do Iguaçu	18	411725
3434	Nova Santa Rosa	18	411722
3435	Nova Tebas	18	411727
3437	Ortigueira	18	411730
3439	Ouro Verde Do Oeste	18	411745
3441	Palmas	18	411760
3442	Palmeira	18	411770
3444	Palotina	18	411790
3446	Paranacity	18	411810
3448	Paranapoema	18	411830
3449	Paranavaí	18	411840
3451	Pato Branco	18	411850
3453	Paulo Frontin	18	411870
3455	Perobal	18	411885
3456	Pérola	18	411890
3458	Piên	18	411910
3460	Pinhal De São Bento	18	411925
3462	Pinhão	18	411930
3463	Piraí Do Sul	18	411940
3465	Pitanga	18	411960
3467	Planaltina Do Paraná	18	411970
3469	Ponta Grossa	18	411990
3470	Pontal Do Paraná	18	411995
3472	Porto Amazonas	18	412010
3474	Porto Rico	18	412020
3476	Prado Ferreira	18	412033
3477	Pranchita	18	412035
3479	Primeiro De Maio	18	412050
3481	Quarto Centenário	18	412065
3483	Quatro Barras	18	412080
3485	Quedas Do Iguaçu	18	412090
3486	Querência Do Norte	18	412100
3488	Quitandinha	18	412120
3490	Rancho Alegre	18	412130
3492	Realeza	18	412140
3493	Rebouças	18	412150
3495	Reserva	18	412170
3497	Ribeirão Claro	18	412180
3499	Rio Azul	18	412200
3500	Rio Bom	18	412210
3502	Rio Branco Do Ivaí	18	412217
3503	Rio Branco Do Sul	18	412220
3505	Rolândia	18	412240
3507	Rondon	18	412260
3509	Sabáudia	18	412270
3510	Salgado Filho	18	412280
3512	Salto Do Lontra	18	412300
3514	Santa Cecília Do Pavão	18	412320
3516	Santa Fé	18	412340
3517	Santa Helena	18	412350
3519	Santa Isabel Do Ivaí	18	412370
3521	Santa Lúcia	18	412382
3522	Santa Maria Do Oeste	18	412385
3524	Santa Mônica	18	412395
3526	Santa Terezinha De Itaipu	18	412405
3529	Santo Antônio Do Caiuá	18	412420
3530	Santo Antônio Do Paraíso	18	412430
3532	Santo Inácio	18	412450
3534	São Jerônimo Da Serra	18	412470
3535	São João	18	412480
3537	São João Do Ivaí	18	412500
3538	São João Do Triunfo	18	412510
3540	São Jorge Do Ivaí	18	412530
3542	São José Da Boa Vista	18	412540
3543	São José Das Palmeiras	18	412545
3545	São Manoel Do Paraná	18	412555
3546	São Mateus Do Sul	18	412560
3548	São Pedro Do Iguaçu	18	412575
3550	São Pedro Do Paraná	18	412590
3552	São Tomé	18	412610
3553	Sapopema	18	412620
3555	Saudade Do Iguaçu	18	412627
3556	Sengés	18	412630
3558	Sertaneja	18	412640
3560	Siqueira Campos	18	412660
3561	Sulina	18	412665
3563	Tamboara	18	412670
3565	Tapira	18	412690
3567	Telêmaco Borba	18	412710
3568	Terra Boa	18	412720
3570	Terra Roxa	18	412740
3572	Tijucas Do Sul	18	412760
3574	Tomazina	18	412780
3576	Tunas Do Paraná	18	412788
3577	Tuneiras Do Oeste	18	412790
3579	Turvo	18	412796
3581	Umuarama	18	412810
3583	Uniflor	18	412830
3584	Uraí	18	412840
3586	Vera Cruz Do Oeste	18	412855
3588	Virmond	18	412865
3589	Vitorino	18	412870
3591	Xambrê	18	412880
3593	Aperibé	19	330015
3594	Araruama	19	330020
3596	Armação Dos Búzios	19	330023
3598	Barra Do Piraí	19	330030
3600	Belford Roxo	19	330045
3601	Bom Jardim	19	330050
3603	Cabo Frio	19	330070
3605	Cambuci	19	330090
3606	Campos Dos Goytacazes	19	330100
3608	Carapebus	19	330093
3610	Carmo	19	330120
3611	Casimiro De Abreu	19	330130
3613	Conceição De Macabu	19	330140
3615	Duas Barras	19	330160
3617	Engenheiro Paulo De Frontin	19	330180
3618	Guapimirim	19	330185
3620	Itaboraí	19	330190
3621	Itaguaí	19	330200
3623	Itaocara	19	330210
3625	Itatiaia	19	330225
3627	Laje Do Muriaé	19	330230
3628	Macaé	19	330240
3630	Magé	19	330250
3632	Maricá	19	330270
3634	Mesquita	19	330285
3636	Miracema	19	330300
3637	Natividade	19	330310
3639	Niterói	19	330330
3641	Nova Iguaçu	19	330350
3642	Paracambi	19	330360
3644	Parati	19	330380
3646	Petrópolis	19	330390
3648	Piraí	19	330400
3649	Porciúncula	19	330410
3653	Quissamã	19	330415
3655	Rio Bonito	19	330430
3657	Rio Das Flores	19	330450
3659	Rio De Janeiro	19	330455
3660	Santa Maria Madalena	19	330460
3662	São Fidélis	19	330480
3664	São Gonçalo	19	330490
3665	São João Da Barra	19	330500
3667	São José De Ubá	19	330513
3668	São José Do Vale Do Rio Preto	19	330515
3670	São Sebastião Do Alto	19	330530
3671	Sapucaia	19	330540
3673	Seropédica	19	330555
3675	Sumidouro	19	330570
3676	Tanguá	19	330575
3678	Trajano De Moraes	19	330590
3680	Valença	19	330610
3681	Varre Sai	19	330615
3684	Acari	20	240010
3685	Açu	20	240020
3687	Água Nova	20	240040
3688	Alexandria	20	240050
3690	Alto Do Rodrigues	20	240070
3692	Antônio Martins	20	240090
3694	Areia Branca	20	240110
3696	Augusto Severo	20	240130
3698	Baraúna	20	240145
3699	Barcelona	20	240150
3701	Bodó	20	240165
3703	Brejinho	20	240180
3705	Caiçara Do Rio Do Vento	20	240190
3706	Caicó	20	240200
3708	Canguaretama	20	240220
3710	Carnaúba Dos Dantas	20	240240
3712	Ceará Mirim	20	240260
3713	Cerro Corá	20	240270
3715	Coronel João Pessoa	20	240290
3717	Currais Novos	20	240310
3719	Encanto	20	240330
3720	Equador	20	240340
3722	Extremoz	20	240360
3724	Fernando Pedroza	20	240375
3726	Francisco Dantas	20	240390
3728	Galinhos	20	240410
3729	Goianinha	20	240420
3731	Grossos	20	240440
3732	Guamaré	20	240450
3734	Ipanguaçu	20	240470
3736	Itajá	20	240485
3738	Jaçanã	20	240500
3739	Jandaíra	20	240510
3741	Januário Cicco	20	240530
3743	Jardim De Angicos	20	240550
3745	Jardim Do Seridó	20	240570
3747	João Dias	20	240590
3748	José Da Penha	20	240600
3750	Jundiá	20	240615
3752	Lagoa De Pedras	20	240630
3753	Lagoa De Velhos	20	240640
3755	Lagoa Salgada	20	240660
3757	Lajes Pintadas	20	240680
3759	Luís Gomes	20	240700
3761	Macau	20	240720
3762	Major Sales	20	240725
3764	Martins	20	240740
3766	Messias Targino	20	240760
3768	Monte Alegre	20	240780
3770	Mossoró	20	240800
3771	Natal	20	240810
3773	Nova Cruz	20	240830
3775	Ouro Branco	20	240850
3776	Paraná	20	240860
3779	Parelhas	20	240890
3781	Passa E Fica	20	240910
3783	Patu	20	240930
3785	Pedra Grande	20	240950
3786	Pedra Preta	20	240960
3788	Pedro Velho	20	240980
3789	Pendências	20	240990
3792	Portalegre	20	241020
3793	Porto Do Mangue	20	241025
3795	Pureza	20	241040
3796	Rafael Fernandes	20	241050
3798	Riacho Da Cruz	20	241070
3800	Riachuelo	20	241090
3801	Rio Do Fogo	20	240895
3803	Ruy Barbosa	20	241110
3805	Santa Maria	20	240933
3807	Santana Do Seridó	20	241142
3809	São Bento Do Norte	20	241160
3810	São Bento Do Trairí	20	241170
3812	São Francisco Do Oeste	20	241190
3814	São João Do Sabugi	20	241210
3815	São José De Mipibu	20	241220
3817	São José Do Seridó	20	241240
3819	São Miguel Do Gostoso	20	241255
3821	São Pedro	20	241270
3822	São Rafael	20	241280
3824	São Vicente	20	241300
3826	Senador Georgino Avelino	20	241320
3827	Serra De São Bento	20	241330
3829	Serra Negra Do Norte	20	241340
3831	Serrinha Dos Pintos	20	241355
3833	Sítio Novo	20	241370
3834	Taboleiro Grande	20	241380
3836	Tangará	20	241400
3838	Tenente Laurentino Cruz	20	241415
3840	Tibau Do Sul	20	241420
3842	Touros	20	241440
3843	Triunfo Potiguar	20	241445
3845	Upanema	20	241460
3847	Venha Ver	20	241475
3848	Vera Cruz	20	241480
3850	Vila Flor	20	241500
3852	Alto Alegre Dos Parecis	21	110037
3854	Alvorada D`Oeste	21	110034
3855	Ariquemes	21	110002
3857	Cabixi	21	110003
3859	Cacoal	21	110004
3861	Candeias Do Jamari	21	110080
3862	Castanheiras	21	110090
3864	Chupinguaia	21	110092
3866	Corumbiara	21	110007
3867	Costa Marques	21	110008
3869	Espigão D`Oeste	21	110009
3871	Guajará Mirim	21	110010
3872	Itapuã Do Oeste	21	110110
3874	Ji Paraná	21	110012
3876	Ministro Andreazza	21	110120
3878	Monte Negro	21	110140
3879	Nova Brasilândia D`Oeste	21	110014
3881	Nova União	21	110143
3883	Ouro Preto Do Oeste	21	110015
3884	Parecis	21	110145
3886	Pimenteiras Do Oeste	21	110146
3888	Presidente Médici	21	110025
3890	Rio Crespo	21	110026
3891	Rolim De Moura	21	110028
3893	São Felipe D`Oeste	21	110148
3895	São Miguel Do Guaporé	21	110032
3896	Seringueiras	21	110150
3898	Theobroma	21	110160
3899	Urupá	21	110170
5422	Vista Alegre Do Alto	26	355690
3902	Vilhena	21	110030
3903	Alto Alegre	22	140005
3905	Boa Vista	22	140010
3907	Cantá	22	140017
3909	Caroebe	22	140023
3911	Mucajaí	22	140030
3912	Normandia	22	140040
3914	Rorainópolis	22	140047
3916	São Luiz	22	140060
3917	Uiramutã	22	140070
3919	Água Santa	23	430005
3921	Ajuricaba	23	430020
3923	Alegrete	23	430040
3925	Almirante Tamandaré Do Sul	23	430047
3927	Alto Alegre	23	430055
3929	Alvorada	23	430060
3930	Amaral Ferrador	23	430063
3932	André Da Rocha	23	430066
3934	Antônio Prado	23	430080
3936	Araricá	23	430087
3937	Aratiba	23	430090
3939	Arroio Do Padre	23	430107
3941	Arroio Do Tigre	23	430120
3942	Arroio Dos Ratos	23	430110
3944	Arvorezinha	23	430140
3946	Áurea	23	430155
3947	Bagé	23	430160
3949	Barão	23	430165
3951	Barão Do Triunfo	23	430175
3953	Barra Do Quaraí	23	430187
3954	Barra Do Ribeiro	23	430190
3956	Barra Funda	23	430195
3957	Barracão	23	430180
3959	Benjamin Constant Do Sul	23	430205
3961	Boa Vista Das Missões	23	430215
3963	Boa Vista Do Cadeado	23	430222
3964	Boa Vista Do Incra	23	430223
3966	Bom Jesus	23	430230
3968	Bom Progresso	23	430237
3969	Bom Retiro Do Sul	23	430240
3971	Bossoroca	23	430250
3973	Braga	23	430260
3974	Brochier	23	430265
3976	Caçapava Do Sul	23	430280
3978	Cachoeira Do Sul	23	430300
3980	Cacique Doble	23	430320
3981	Caibaté	23	430330
3983	Camaquã	23	430350
3985	Cambará Do Sul	23	430360
3987	Campina Das Missões	23	430370
3989	Campo Bom	23	430390
3990	Campo Novo	23	430400
3992	Candelária	23	430420
3994	Candiota	23	430435
3995	Canela	23	430440
3997	Canoas	23	430460
3999	Capão Bonito Do Sul	23	430462
4001	Capão Do Cipó	23	430465
4002	Capão Do Leão	23	430466
4004	Capitão	23	430469
4006	Caraá	23	430471
4007	Carazinho	23	430470
4009	Carlos Gomes	23	430485
4011	Caseiros	23	430495
4012	Catuípe	23	430500
4014	Centenário	23	430511
4016	Cerro Branco	23	430513
4018	Cerro Grande Do Sul	23	430517
4020	Chapada	23	430530
4021	Charqueadas	23	430535
4023	Chiapetta	23	430540
4025	Chuvisca	23	430544
4028	Colinas	23	430558
4030	Condor	23	430570
4032	Coqueiro Baixo	23	430583
4034	Coronel Barros	23	430587
4036	Coronel Pilar	23	430593
4037	Cotiporã	23	430595
4039	Crissiumal	23	430600
4041	Cristal Do Sul	23	430607
4043	Cruzaltense	23	430613
4045	David Canabarro	23	430630
4046	Derrubadas	23	430632
4048	Dilermando De Aguiar	23	430637
4050	Dois Irmãos Das Missões	23	430642
4051	Dois Lajeados	23	430645
4053	Dom Pedrito	23	430660
4055	Dona Francisca	23	430670
4056	Doutor Maurício Cardoso	23	430673
4058	Eldorado Do Sul	23	430676
4059	Encantado	23	430680
4061	Engenho Velho	23	430692
4063	Entre Ijuís	23	430693
4065	Erechim	23	430700
4066	Ernestina	23	430705
4068	Erval Seco	23	430730
4070	Esperança Do Sul	23	430745
4072	Estação	23	430755
4074	Esteio	23	430770
4075	Estrela	23	430780
4077	Eugênio De Castro	23	430783
4079	Farroupilha	23	430790
4080	Faxinal Do Soturno	23	430800
4082	Fazenda Vilanova	23	430807
4084	Flores Da Cunha	23	430820
4086	Fontoura Xavier	23	430830
4087	Formigueiro	23	430840
4089	Fortaleza Dos Valos	23	430845
4091	Garibaldi	23	430860
4092	Garruchos	23	430865
4094	General Câmara	23	430880
4096	Getúlio Vargas	23	430890
4098	Glorinha	23	430905
4099	Gramado	23	430910
4101	Gramado Xavier	23	430915
4103	Guabiju	23	430925
4105	Guaporé	23	430940
4107	Harmonia	23	430955
4108	Herval	23	430710
4110	Horizontina	23	430960
4111	Hulha Negra	23	430965
4113	Ibarama	23	430975
4115	Ibiraiaras	23	430990
4117	Ibirubá	23	431000
4119	Ijuí	23	431020
4120	Ilópolis	23	431030
4122	Imigrante	23	431036
4124	Inhacorá	23	431041
4125	Ipê	23	431043
4127	Iraí	23	431050
4129	Itacurubi	23	431055
4131	Itaqui	23	431060
4132	Itati	23	431065
4134	Ivorá	23	431075
4136	Jaboticaba	23	431085
4138	Jacutinga	23	431090
4139	Jaguarão	23	431100
4141	Jaquirana	23	431112
4143	Jóia	23	431115
4145	Lagoa Bonita Do Sul	23	431123
4147	Lagoa Vermelha	23	431130
4148	Lagoão	23	431125
4150	Lajeado Do Bugre	23	431142
4152	Liberato Salzano	23	431160
4153	Lindolfo Collor	23	431162
5424	Votorantim	26	355700
4156	Machadinho	23	431170
4158	Manoel Viana	23	431175
4160	Maratá	23	431179
4161	Marau	23	431180
4163	Mariana Pimentel	23	431198
4165	Marques De Souza	23	431205
4166	Mata	23	431210
4168	Mato Leitão	23	431215
4170	Maximiliano De Almeida	23	431220
4171	Minas Do Leão	23	431225
4173	Montauri	23	431235
4175	Monte Belo Do Sul	23	431238
4176	Montenegro	23	431240
4178	Morrinhos Do Sul	23	431244
4180	Morro Reuter	23	431247
4182	Muçum	23	431260
4184	Muliterno	23	431262
4185	Não Me Toque	23	431265
4187	Nonoai	23	431270
4189	Nova Araçá	23	431280
4190	Nova Bassano	23	431290
4192	Nova Bréscia	23	431300
4194	Nova Esperança Do Sul	23	431303
4195	Nova Hartz	23	431306
4197	Nova Palma	23	431310
4199	Nova Prata	23	431330
4201	Nova Roma Do Sul	23	431335
4203	Novo Barreiro	23	431349
4204	Novo Cabrais	23	431339
4206	Novo Machado	23	431342
4208	Novo Xingu	23	431346
4209	Osório	23	431350
4211	Palmares Do Sul	23	431365
4213	Palmitinho	23	431380
4214	Panambi	23	431390
4216	Paraí	23	431400
4218	Pareci Novo	23	431403
4219	Parobé	23	431405
4222	Passo Fundo	23	431410
4223	Paulo Bento	23	431413
4225	Pedras Altas	23	431417
4227	Pejuçara	23	431430
4228	Pelotas	23	431440
4230	Pinhal	23	431445
4232	Pinhal Grande	23	431447
4233	Pinheirinho Do Vale	23	431449
4235	Pirapó	23	431455
4237	Planalto	23	431470
4239	Pontão	23	431477
4240	Ponte Preta	23	431478
4242	Porto Alegre	23	431490
4244	Porto Mauá	23	431505
4246	Porto Xavier	23	431510
4247	Pouso Novo	23	431513
4249	Progresso	23	431515
4251	Putinga	23	431520
4252	Quaraí	23	431530
4254	Quevedos	23	431532
4256	Redentora	23	431540
4257	Relvado	23	431545
4259	Rio Dos Índios	23	431555
4261	Rio Pardo	23	431570
4263	Roca Sales	23	431580
4264	Rodeio Bonito	23	431590
4266	Rolante	23	431600
4268	Rondinha	23	431620
4270	Rosário Do Sul	23	431640
4271	Sagrada Família	23	431642
4273	Salto Do Jacuí	23	431645
4275	Salvador Do Sul	23	431650
4276	Sananduva	23	431660
4278	Santa Cecília Do Sul	23	431673
4282	Santa Maria	23	431690
4284	Santa Rosa	23	431720
4285	Santa Tereza	23	431725
4287	Santana Da Boa Vista	23	431700
4289	Santiago	23	431740
4290	Santo Ângelo	23	431750
4292	Santo Antônio Das Missões	23	431770
4294	Santo Antônio Do Planalto	23	431775
4296	Santo Cristo	23	431790
4297	Santo Expedito Do Sul	23	431795
4299	São Domingos Do Sul	23	431805
4301	São Francisco De Paula	23	431820
4302	São Gabriel	23	431830
4304	São João Da Urtiga	23	431842
4306	São Jorge	23	431844
4307	São José Das Missões	23	431845
4309	São José Do Hortêncio	23	431848
4311	São José Do Norte	23	431850
4312	São José Do Ouro	23	431860
4314	São José Dos Ausentes	23	431862
4315	São Leopoldo	23	431870
4317	São Luiz Gonzaga	23	431890
4318	São Marcos	23	431900
4320	São Martinho Da Serra	23	431912
4322	São Nicolau	23	431920
4324	São Pedro Da Serra	23	431935
4325	São Pedro Das Missões	23	431936
4327	São Pedro Do Sul	23	431940
4328	São Sebastião Do Caí	23	431950
4330	São Valentim	23	431970
4332	São Valério Do Sul	23	431973
4333	São Vendelino	23	431975
4335	Sapiranga	23	431990
4337	Sarandi	23	432010
4338	Seberi	23	432020
4340	Segredo	23	432026
4342	Senador Salgado Filho	23	432032
4344	Serafina Corrêa	23	432040
4345	Sério	23	432045
4347	Sertão Santana	23	432055
4349	Severiano De Almeida	23	432060
4351	Sinimbu	23	432067
4352	Sobradinho	23	432070
4354	Tabaí	23	432085
4356	Tapera	23	432100
4357	Tapes	23	432110
4359	Taquari	23	432130
4361	Tavares	23	432135
4363	Terra De Areia	23	432143
4364	Teutônia	23	432145
4366	Tiradentes Do Sul	23	432147
4368	Torres	23	432150
4370	Travesseiro	23	432162
4372	Três Cachoeiras	23	432166
4373	Três Coroas	23	432170
4375	Três Forquilhas	23	432183
4377	Três Passos	23	432190
4378	Trindade Do Sul	23	432195
4380	Tucunduva	23	432210
4382	Tupanci Do Sul	23	432218
4384	Tupandi	23	432225
4385	Tuparendi	23	432230
4387	Ubiretama	23	432234
4389	Unistalda	23	432237
4391	Vacaria	23	432250
4392	Vale Do Sol	23	432253
4394	Vale Verde	23	432252
4396	Venâncio Aires	23	432260
4398	Veranópolis	23	432280
4400	Viadutos	23	432290
4401	Viamão	23	432300
4404	Vila Flores	23	432330
4406	Vila Maria	23	432340
4408	Vista Alegre	23	432350
4409	Vista Alegre Do Prata	23	432360
4411	Vitória Das Missões	23	432375
4412	Westfália	23	432377
4414	Abdon Batista	24	420005
4416	Agrolândia	24	420020
4418	Água Doce	24	420040
4420	Águas Frias	24	420055
4421	Águas Mornas	24	420060
4423	Alto Bela Vista	24	420075
4425	Angelina	24	420090
4427	Anitápolis	24	420110
4428	Antônio Carlos	24	420120
4430	Arabutã	24	420127
4432	Araranguá	24	420140
4433	Armazém	24	420150
4435	Arvoredo	24	420165
4437	Atalanta	24	420180
4439	Balneário Arroio Do Silva	24	420195
4441	Balneário Camboriú	24	420200
4442	Balneário Gaivota	24	420207
4444	Bandeirante	24	420208
4445	Barra Bonita	24	420209
4447	Bela Vista Do Toldo	24	420213
4449	Benedito Novo	24	420220
4451	Blumenau	24	420240
4453	Bom Jardim Da Serra	24	420250
4454	Bom Jesus	24	420253
4456	Bom Retiro	24	420260
4457	Bombinhas	24	420245
4459	Braço Do Norte	24	420280
4461	Brunópolis	24	420287
4463	Caçador	24	420300
4464	Caibi	24	420310
4466	Camboriú	24	420320
4468	Campo Belo Do Sul	24	420340
4470	Campos Novos	24	420360
4471	Canelinha	24	420370
4473	Capão Alto	24	420325
4475	Capivari De Baixo	24	420395
4477	Caxambu Do Sul	24	420410
4479	Cerro Negro	24	420417
4481	Chapecó	24	420420
4482	Cocal Do Sul	24	420425
4484	Cordilheira Alta	24	420435
4486	Coronel Martins	24	420445
4488	Corupá	24	420450
4489	Criciúma	24	420460
4491	Cunhataí	24	420475
4493	Descanso	24	420490
4495	Dona Emma	24	420510
4496	Doutor Pedrinho	24	420515
4498	Ermo	24	420519
4499	Erval Velho	24	420520
4501	Flor Do Sertão	24	420535
4503	Formosa Do Sul	24	420543
4505	Fraiburgo	24	420550
4506	Frei Rogério	24	420555
4508	Garopaba	24	420570
4510	Gaspar	24	420590
4512	Grão Pará	24	420610
4513	Gravatal	24	420620
4515	Guaraciaba	24	420640
4517	Guarujá Do Sul	24	420660
4519	Herval D`Oeste	24	420670
4520	Ibiam	24	420675
4522	Ibirama	24	420690
4524	Ilhota	24	420710
4526	Imbituba	24	420730
4527	Imbuia	24	420740
4529	Iomerê	24	420757
5426	Zacarias	26	355715
4532	Ipuaçu	24	420768
4533	Ipumirim	24	420770
4535	Irani	24	420780
4537	Irineópolis	24	420790
4538	Itá	24	420800
4540	Itajaí	24	420820
4542	Itapiranga	24	420840
4544	Ituporanga	24	420850
4545	Jaborá	24	420860
4547	Jaguaruna	24	420880
4549	Jardinópolis	24	420895
4551	Joinville	24	420910
4552	José Boiteux	24	420915
4554	Lacerdópolis	24	420920
4556	Laguna	24	420940
4558	Laurentino	24	420950
4559	Lauro Muller	24	420960
4561	Leoberto Leal	24	420980
4563	Lontras	24	420990
4564	Luiz Alves	24	421000
4566	Macieira	24	421005
4568	Major Gercino	24	421020
4570	Maracajá	24	421040
4571	Maravilha	24	421050
4573	Massaranduba	24	421060
4575	Meleiro	24	421080
4577	Modelo	24	421090
4579	Monte Carlo	24	421105
4581	Morro Da Fumaça	24	421120
4582	Morro Grande	24	421125
4584	Nova Erechim	24	421140
4586	Nova Trento	24	421150
4587	Nova Veneza	24	421160
4589	Orleans	24	421170
4591	Ouro	24	421180
4592	Ouro Verde	24	421185
4594	Painel	24	421189
4596	Palma Sola	24	421200
4598	Palmitos	24	421210
4599	Papanduva	24	421220
4601	Passo De Torres	24	421225
4603	Paulo Lopes	24	421230
4605	Penha	24	421250
4606	Peritiba	24	421260
4608	Pinhalzinho	24	421290
4610	Piratuba	24	421310
4612	Pomerode	24	421320
4613	Ponte Alta	24	421330
4615	Ponte Serrada	24	421340
4617	Porto União	24	421360
4619	Praia Grande	24	421380
4620	Presidente Castello Branco	24	421390
4622	Presidente Nereu	24	421410
4624	Quilombo	24	421420
4626	Rio Das Antas	24	421440
4627	Rio Do Campo	24	421450
4629	Rio Do Sul	24	421480
4631	Rio Fortuna	24	421490
4632	Rio Negrinho	24	421500
4634	Riqueza	24	421507
4636	Romelândia	24	421520
4637	Salete	24	421530
4639	Salto Veloso	24	421540
4641	Santa Cecília	24	421550
4643	Santa Rosa De Lima	24	421560
4645	Santa Terezinha	24	421567
4646	Santa Terezinha Do Progresso	24	421568
4648	Santo Amaro Da Imperatriz	24	421570
4650	São Bernardino	24	421575
4651	São Bonifácio	24	421590
4653	São Cristovão Do Sul	24	421605
4655	São Francisco Do Sul	24	421620
4658	São João Do Oeste	24	421625
4660	São Joaquim	24	421650
4661	São José	24	421660
4663	São José Do Cerrito	24	421680
4665	São Ludgero	24	421700
4667	São Miguel Da Boa Vista	24	421715
4668	São Miguel Do Oeste	24	421720
4670	Saudades	24	421730
4671	Schroeder	24	421740
4673	Serra Alta	24	421755
4675	Sombrio	24	421770
4677	Taió	24	421780
4678	Tangará	24	421790
4680	Tijucas	24	421800
4682	Timbó	24	421820
4684	Três Barras	24	421830
4685	Treviso	24	421835
4687	Treze Tílias	24	421850
4689	Tubarão	24	421870
4690	Tunápolis	24	421875
4692	União Do Oeste	24	421885
4694	Urupema	24	421895
4696	Vargeão	24	421910
4697	Vargem	24	421915
4699	Vidal Ramos	24	421920
4701	Vitor Meireles	24	421935
4703	Xanxerê	24	421950
4705	Xaxim	24	421970
4706	Zortéa	24	421985
4708	Aquidabã	25	280020
4710	Arauá	25	280040
4712	Barra Dos Coqueiros	25	280060
4713	Boquim	25	280067
4715	Campo Do Brito	25	280100
4717	Canindé De São Francisco	25	280120
4718	Capela	25	280130
4720	Carmópolis	25	280150
4722	Cristinápolis	25	280170
4724	Divina Pastora	25	280200
4726	Feira Nova	25	280220
4727	Frei Paulo	25	280230
4729	General Maynard	25	280250
4731	Ilha Das Flores	25	280270
4733	Itabaiana	25	280290
4734	Itabaianinha	25	280300
4736	Itaporanga D`Ajuda	25	280320
4738	Japoatã	25	280340
4740	Laranjeiras	25	280360
4741	Macambira	25	280370
4743	Malhador	25	280390
4745	Moita Bonita	25	280410
4747	Muribeca	25	280430
4748	Neópolis	25	280440
4750	Nossa Senhora Da Glória	25	280450
4752	Nossa Senhora De Lourdes	25	280470
4753	Nossa Senhora Do Socorro	25	280480
4755	Pedra Mole	25	280500
4757	Pinhão	25	280520
4758	Pirambu	25	280530
4760	Poço Verde	25	280550
4762	Propriá	25	280570
4764	Riachuelo	25	280590
4765	Ribeirópolis	25	280600
4767	Salgado	25	280620
4769	Santa Rosa De Lima	25	280650
4770	Santana Do São Francisco	25	280640
4772	São Cristóvão	25	280670
4774	São Francisco	25	280690
4775	São Miguel Do Aleixo	25	280700
4777	Siriri	25	280720
4779	Tobias Barreto	25	280740
4781	Umbaúba	25	280760
4784	Aguaí	26	350030
4786	Águas De Lindóia	26	350050
4788	Águas De São Pedro	26	350060
4789	Agudos	26	350070
4791	Alfredo Marcondes	26	350080
4793	Altinópolis	26	350100
4794	Alto Alegre	26	350110
4797	Álvares Machado	26	350130
4798	Álvaro De Carvalho	26	350140
4800	Americana	26	350160
4802	Américo De Campos	26	350180
4803	Amparo	26	350190
4805	Andradina	26	350210
4807	Anhembi	26	350230
4808	Anhumas	26	350240
4810	Aparecida D`Oeste	26	350260
4812	Araçariguama	26	350275
4814	Araçoiaba Da Serra	26	350290
4816	Arandu	26	350310
4817	Arapeí	26	350315
4819	Araras	26	350330
4821	Arealva	26	350340
4823	Areiópolis	26	350360
4825	Artur Nogueira	26	350380
4826	Arujá	26	350390
4828	Assis	26	350400
4830	Auriflama	26	350420
4832	Avanhandava	26	350440
4833	Avaré	26	350450
4835	Balbinos	26	350470
4837	Bananal	26	350490
4839	Barbosa	26	350510
4840	Bariri	26	350520
4842	Barra Do Chapéu	26	350535
4844	Barretos	26	350550
4845	Barrinha	26	350560
4847	Bastos	26	350580
4849	Bauru	26	350600
4851	Bento De Abreu	26	350620
4853	Bertioga	26	350635
4854	Bilac	26	350640
4856	Biritiba Mirim	26	350660
4858	Bocaina	26	350680
4859	Bofete	26	350690
4861	Bom Jesus Dos Perdões	26	350710
4863	Borá	26	350720
4864	Boracéia	26	350730
4866	Borebi	26	350745
4868	Bragança Paulista	26	350760
4870	Brejo Alegre	26	350775
4871	Brodowski	26	350780
4873	Buri	26	350800
4875	Buritizal	26	350820
4877	Cabreúva	26	350840
4878	Caçapava	26	350850
4880	Caconde	26	350870
4882	Caiabu	26	350890
4884	Caiuá	26	350910
4885	Cajamar	26	350920
4887	Cajobi	26	350930
4889	Campina Do Monte Alegre	26	350945
4891	Campo Limpo Paulista	26	350960
4893	Campos Novos Paulista	26	350980
4894	Cananéia	26	350990
4896	Cândido Mota	26	351000
4898	Canitar	26	351015
4900	Capela Do Alto	26	351030
4901	Capivari	26	351040
4903	Carapicuíba	26	351060
4905	Casa Branca	26	351080
4907	Castilho	26	351100
4908	Catanduva	26	351110
4910	Cedral	26	351130
4913	Cesário Lange	26	351160
4915	Chavantes	26	355720
4916	Clementina	26	351190
4918	Colômbia	26	351210
4920	Conchas	26	351230
4922	Coroados	26	351250
4924	Corumbataí	26	351270
4925	Cosmópolis	26	351280
4927	Cotia	26	351300
4928	Cravinhos	26	351310
4930	Cruzália	26	351330
4932	Cubatão	26	351350
4934	Descalvado	26	351370
4935	Diadema	26	351380
4937	Divinolândia	26	351390
4939	Dois Córregos	26	351410
4941	Dourado	26	351430
4943	Duartina	26	351450
4944	Dumont	26	351460
4946	Eldorado	26	351480
4948	Elisiário	26	351492
4950	Embu Das Artes	26	351500
4951	Embu Guaçu	26	351510
4953	Engenheiro Coelho	26	351515
4955	Espírito Santo Do Turvo	26	351519
4957	Estrela D`Oeste	26	351520
4958	Estrela Do Norte	26	351530
4960	Fartura	26	351540
4962	Fernandópolis	26	351550
4963	Fernão	26	351565
4965	Flora Rica	26	351580
4966	Floreal	26	351590
4968	Flórida Paulista	26	351600
4970	Francisco Morato	26	351630
4972	Gabriel Monteiro	26	351650
4973	Gália	26	351660
4975	Gastão Vidigal	26	351680
4977	General Salgado	26	351690
4979	Glicério	26	351710
4980	Guaiçara	26	351720
4982	Guaíra	26	351740
4984	Guapiara	26	351760
4986	Guaraçaí	26	351780
4988	Guarani D`Oeste	26	351800
4989	Guarantã	26	351810
4991	Guararema	26	351830
4993	Guareí	26	351850
4995	Guarujá	26	351870
4996	Guarulhos	26	351880
4998	Guzolândia	26	351890
5000	Holambra	26	351905
5002	Iacanga	26	351910
5003	Iacri	26	351920
5005	Ibaté	26	351930
5007	Ibirarema	26	351950
5009	Ibiúna	26	351970
5010	Icém	26	351980
5012	Igaraçu Do Tietê	26	352000
5014	Igaratá	26	352020
5016	Ilha Comprida	26	352042
5018	Ilhabela	26	352040
5019	Indaiatuba	26	352050
5021	Indiaporã	26	352070
5023	Ipaussu	26	352090
5024	Iperó	26	352100
5026	Ipiguá	26	352115
5028	Ipuã	26	352130
5030	Irapuã	26	352150
5031	Irapuru	26	352160
5033	Itaí	26	352180
5035	Itaju	26	352200
5037	Itaóca	26	352215
5039	Itapetininga	26	352230
5040	Itapeva	26	352240
5043	Itapirapuã Paulista	26	352265
5044	Itápolis	26	352270
5046	Itapuí	26	352290
5048	Itaquaquecetuba	26	352310
5050	Itariri	26	352330
5052	Itatinga	26	352350
5054	Itirapuã	26	352370
5055	Itobi	26	352380
5057	Itupeva	26	352400
5059	Jaborandi	26	352420
5061	Jacareí	26	352440
5063	Jacupiranga	26	352460
5064	Jaguariúna	26	352470
5066	Jambeiro	26	352490
5068	Jardinópolis	26	352510
5070	Jaú	26	352530
5071	Jeriquara	26	352540
5073	João Ramalho	26	352560
5075	Júlio Mesquita	26	352580
5077	Jundiaí	26	352590
5079	Juquiá	26	352610
5080	Juquitiba	26	352620
5082	Laranjal Paulista	26	352640
5084	Lavrinhas	26	352660
5085	Leme	26	352670
5087	Limeira	26	352690
5089	Lins	26	352710
5090	Lorena	26	352720
5092	Louveira	26	352730
5094	Lucianópolis	26	352750
5096	Luiziânia	26	352770
5097	Lupércio	26	352780
5099	Macatuba	26	352800
5101	Macedônia	26	352820
5103	Mairinque	26	352840
5105	Manduri	26	352860
5107	Maracaí	26	352880
5108	Marapoama	26	352885
5110	Marília	26	352900
5112	Martinópolis	26	352920
5113	Matão	26	352930
5115	Mendonça	26	352950
5117	Mesópolis	26	352965
5119	Mineiros Do Tietê	26	352980
5120	Mira Estrela	26	353000
5122	Mirandópolis	26	353010
5124	Mirassol	26	353030
5125	Mirassolândia	26	353040
5127	Mogi Das Cruzes	26	353060
5129	Moji Mirim	26	353080
5131	Monções	26	353100
5132	Mongaguá	26	353110
5134	Monte Alto	26	353130
5136	Monte Azul Paulista	26	353150
5138	Monte Mor	26	353180
5139	Monteiro Lobato	26	353170
5141	Morungaba	26	353200
5143	Murutinga Do Sul	26	353210
5144	Nantes	26	353215
5146	Natividade Da Serra	26	353230
5148	Neves Paulista	26	353250
5149	Nhandeara	26	353260
5151	Nova Aliança	26	353280
5153	Nova Canaã Paulista	26	353284
5155	Nova Europa	26	353290
5156	Nova Granada	26	353300
5158	Nova Independência	26	353320
5160	Nova Odessa	26	353340
5161	Novais	26	353325
5163	Nuporanga	26	353360
5165	Óleo	26	353380
5166	Olímpia	26	353390
5168	Oriente	26	353410
5170	Orlândia	26	353430
5428	Aguiarnópolis	27	170030
5173	Osvaldo Cruz	26	353460
5174	Ourinhos	26	353470
5176	Ouroeste	26	353475
5178	Palestina	26	353500
5180	Palmeira D`Oeste	26	353520
5181	Palmital	26	353530
5183	Paraguaçu Paulista	26	353550
5185	Paraíso	26	353570
5187	Paranapuã	26	353590
5188	Parapuã	26	353600
5190	Pariquera Açu	26	353620
5192	Patrocínio Paulista	26	353630
5194	Paulínia	26	353650
5195	Paulistânia	26	353657
5197	Pederneiras	26	353670
5199	Pedranópolis	26	353690
5201	Pedreira	26	353710
5202	Pedrinhas Paulista	26	353715
5204	Penápolis	26	353730
5206	Pereiras	26	353750
5207	Peruíbe	26	353760
5209	Piedade	26	353780
5211	Pindamonhangaba	26	353800
5213	Pinhalzinho	26	353820
5215	Piquete	26	353850
5216	Piracaia	26	353860
5218	Piraju	26	353880
5220	Pirangi	26	353900
5222	Pirapozinho	26	353920
5223	Pirassununga	26	353930
5225	Pitangueiras	26	353950
5227	Platina	26	353970
5229	Poloni	26	353990
5230	Pompéia	26	354000
5232	Pontal	26	354020
5234	Pontes Gestal	26	354030
5236	Porangaba	26	354050
5238	Porto Ferreira	26	354070
5240	Potirendaba	26	354080
5241	Pracinha	26	354085
5243	Praia Grande	26	354100
5245	Presidente Alves	26	354110
5247	Presidente Epitácio	26	354130
5248	Presidente Prudente	26	354140
5250	Promissão	26	354160
5251	Quadra	26	354165
5253	Queiroz	26	354180
5255	Quintana	26	354200
5257	Rancharia	26	354220
5259	Regente Feijó	26	354240
5260	Reginópolis	26	354250
5262	Restinga	26	354270
5264	Ribeirão Bonito	26	354290
5266	Ribeirão Corrente	26	354310
5268	Ribeirão Dos Índios	26	354323
5269	Ribeirão Grande	26	354325
5271	Ribeirão Preto	26	354340
5273	Rincão	26	354370
5275	Rio Claro	26	354390
5276	Rio Das Pedras	26	354400
5278	Riolândia	26	354420
5280	Rosana	26	354425
5282	Rubiácea	26	354440
5283	Rubinéia	26	354450
5285	Sagres	26	354470
5287	Sales Oliveira	26	354490
5289	Salmourão	26	354510
5290	Saltinho	26	354515
5292	Salto De Pirapora	26	354530
5294	Sandovalina	26	354550
5296	Santa Albertina	26	354570
5297	Santa Bárbara D`Oeste	26	354580
5413	Valinhos	26	355620
5415	Vargem	26	355635
5417	Vargem Grande Paulista	26	355645
5418	Várzea Paulista	26	355650
5420	Vinhedo	26	355670
5421	Viradouro	26	355680
5301	Santa Cruz Da Esperança	26	354625
5303	Santa Cruz Do Rio Pardo	26	354640
5304	Santa Ernestina	26	354650
5306	Santa Gertrudes	26	354670
5308	Santa Lúcia	26	354690
5309	Santa Maria Da Serra	26	354700
5311	Santa Rita D`Oeste	26	354740
5313	Santa Rosa De Viterbo	26	354760
5314	Santa Salete	26	354765
5316	Santana De Parnaíba	26	354730
5317	Santo Anastácio	26	354770
5319	Santo Antônio Da Alegria	26	354790
5321	Santo Antônio Do Aracanguá	26	354805
5323	Santo Antônio Do Pinhal	26	354820
5324	Santo Expedito	26	354830
5326	Santos	26	354850
5327	São Bento Do Sapucaí	26	354860
5329	São Caetano Do Sul	26	354880
5330	São Carlos	26	354890
5332	São João Da Boa Vista	26	354910
5334	São João De Iracema	26	354925
5336	São Joaquim Da Barra	26	354940
5337	São José Da Bela Vista	26	354950
5339	São José Do Rio Pardo	26	354970
5340	São José Do Rio Preto	26	354980
5342	São Lourenço Da Serra	26	354995
5344	São Manuel	26	355010
5345	São Miguel Arcanjo	26	355020
5347	São Pedro	26	355040
5349	São Roque	26	355060
5350	São Sebastião	26	355070
5352	São Simão	26	355090
5353	São Vicente	26	355100
5355	Sarutaiá	26	355120
5357	Serra Azul	26	355140
5358	Serra Negra	26	355160
5360	Sertãozinho	26	355170
5362	Severínia	26	355190
5364	Socorro	26	355210
5365	Sorocaba	26	355220
5367	Sumaré	26	355240
5369	Suzano	26	355250
5370	Tabapuã	26	355260
5372	Taboão Da Serra	26	355280
5374	Taguaí	26	355300
5376	Taiúva	26	355320
5378	Tanabi	26	355340
5379	Tapiraí	26	355350
5381	Taquaral	26	355365
5383	Taquarituba	26	355380
5385	Tarabai	26	355390
5386	Tarumã	26	355395
5388	Taubaté	26	355410
5390	Teodoro Sampaio	26	355430
5392	Tietê	26	355450
5394	Torre De Pedra	26	355465
5395	Torrinha	26	355470
5397	Tremembé	26	355480
5399	Tuiuti	26	355495
5401	Tupi Paulista	26	355510
5402	Turiúba	26	355520
5404	Ubarana	26	355535
5406	Ubirajara	26	355550
5408	União Paulista	26	355570
5410	Uru	26	355590
5411	Urupês	26	355600
5423	Vitória Brasil	26	355695
5425	Votuporanga	26	355710
5427	Abreulândia	27	170025
5429	Aliança Do Tocantins	27	170035
5430	Almas	27	170040
5432	Ananás	27	170100
5434	Aparecida Do Rio Negro	27	170110
5436	Araguacema	27	170190
5437	Araguaçu	27	170200
5439	Araguanã	27	170215
5441	Arapoema	27	170230
5443	Augustinópolis	27	170255
5445	Axixá Do Tocantins	27	170290
5446	Babaçulândia	27	170300
5448	Barra Do Ouro	27	170307
5450	Bernardo Sayão	27	170320
5451	Bom Jesus Do Tocantins	27	170330
5453	Brejinho De Nazaré	27	170370
5454	Buriti Do Tocantins	27	170380
5456	Campos Lindos	27	170384
5458	Carmolândia	27	170388
5460	Caseara	27	170390
5461	Centenário	27	170410
5463	Chapada De Areia	27	170460
5465	Colméia	27	171670
5466	Combinado	27	170555
5468	Couto Magalhães	27	170600
5469	Cristalândia	27	170610
5471	Darcinópolis	27	170650
5472	Dianópolis	27	170700
5474	Dois Irmãos Do Tocantins	27	170720
5476	Esperantina	27	170740
5477	Fátima	27	170755
5479	Filadélfia	27	170770
5481	Fortaleza Do Tabocão	27	170825
5483	Goiatins	27	170900
5484	Guaraí	27	170930
5486	Ipueiras	27	170980
5488	Itaguatins	27	171070
5490	Itaporã Do Tocantins	27	171110
5491	Jaú Do Tocantins	27	171150
5493	Lagoa Da Confusão	27	171190
5495	Lajeado	27	171200
5497	Lizarda	27	171240
5498	Luzinópolis	27	171245
5500	Mateiros	27	171270
5502	Miracema Do Tocantins	27	171320
5503	Miranorte	27	171330
5505	Monte Santo Do Tocantins	27	171370
5507	Natividade	27	171420
5508	Nazaré	27	171430
5510	Nova Rosalândia	27	171500
5512	Novo Alegre	27	171515
5514	Oliveira De Fátima	27	171550
5515	Palmas	27	172100
5517	Palmeiras Do Tocantins	27	171380
5519	Paraíso Do Tocantins	27	171610
5520	Paranã	27	171620
5522	Pedro Afonso	27	171650
5524	Pequizeiro	27	171665
5526	Piraquê	27	171720
5527	Pium	27	171750
5529	Ponte Alta Do Tocantins	27	171790
5531	Porto Nacional	27	171820
5532	Praia Norte	27	171830
5534	Pugmil	27	171845
5536	Riachinho	27	171855
5537	Rio Da Conceição	27	171865
5539	Rio Sono	27	171875
5541	Sandolândia	27	171884
5543	Santa Maria Do Tocantins	27	171888
5544	Santa Rita Do Tocantins	27	171889
1	Acrelândia	1	120001
2	Assis Brasil	1	120005
4	Bujari	1	120013
6	Cruzeiro Do Sul	1	120020
9	Jordão	1	120032
11	Manoel Urbano	1	120034
13	Plácido De Castro	1	120038
16	Rio Branco	1	120040
18	Santa Rosa Do Purus	1	120043
20	Senador Guiomard	1	120045
23	Água Branca	2	270010
25	Arapiraca	2	270030
27	Barra De Santo Antônio	2	270050
30	Belém	2	270080
32	Boca Da Mata	2	270100
35	Cajueiro	2	270130
37	Campo Alegre	2	270140
39	Canapi	2	270160
42	Chã Preta	2	270190
44	Colônia Leopoldina	2	270210
46	Coruripe	2	270230
48	Delmiro Gouveia	2	270240
51	Feira Grande	2	270260
53	Flexeiras	2	270280
56	Igaci	2	270310
58	Inhapi	2	270330
60	Jacuípe	2	270350
62	Jaramataia	2	270370
65	Jundiá	2	270390
67	Lagoa Da Canoa	2	270410
69	Maceió	2	270430
72	Maragogi	2	270450
74	Marechal Deodoro	2	270470
76	Mata Grande	2	270500
79	Minador Do Negrão	2	270530
81	Murici	2	270550
83	Olho D`Água Das Flores	2	270570
86	Olivença	2	270600
88	Palestina	2	270620
90	Pão De Açúcar	2	270640
93	Passo De Camaragibe	2	270650
95	Penedo	2	270670
98	Pindoba	2	270700
100	Poço Das Trincheiras	2	270720
102	Porto De Pedras	2	270740
105	Rio Largo	2	270770
107	Santa Luzia Do Norte	2	270790
110	São Brás	2	270820
112	São José Da Tapera	2	270840
115	São Miguel Dos Milagres	2	270870
118	Senador Rui Palmeira	2	270895
120	Taquarana	2	270910
123	União Dos Palmares	2	270930
125	Alvarães	3	130002
127	Anamã	3	130008
128	Anori	3	130010
130	Atalaia Do Norte	3	130020
133	Barreirinha	3	130050
135	Beruri	3	130063
137	Boca Do Acre	3	130070
139	Caapiranga	3	130083
142	Careiro	3	130110
144	Coari	3	130120
146	Eirunepé	3	130140
5547	Santa Terezinha Do Tocantins	27	172000
5549	São Félix Do Tocantins	27	172015
5550	São Miguel Do Tocantins	27	172020
5552	São Sebastião Do Tocantins	27	172030
5554	Silvanópolis	27	172065
5555	Sítio Novo Do Tocantins	27	172080
5557	Taguatinga	27	172090
5559	Talismã	27	172097
5560	Tocantínia	27	172110
5562	Tupirama	27	172125
5564	Wanderlândia	27	172208
148	Fonte Boa	3	130160
151	Ipixuna	3	130180
153	Itacoatiara	3	130190
155	Itapiranga	3	130200
157	Juruá	3	130220
160	Manacapuru	3	130250
162	Manaus	3	130260
164	Maraã	3	130280
167	Nova Olinda Do Norte	3	130310
169	Novo Aripuanã	3	130330
172	Presidente Figueiredo	3	130353
174	Santa Isabel Do Rio Negro	3	130360
176	São Gabriel Da Cachoeira	3	130380
180	Tabatinga	3	130406
182	Tefé	3	130420
184	Uarini	3	130426
186	Urucurituba	3	130440
189	Cutias	4	160021
191	Itaubal	4	160025
193	Macapá	4	160030
195	Oiapoque	4	160050
197	Porto Grande	4	160053
200	Serra Do Navio	4	160005
202	Vitória Do Jari	4	160080
205	Acajutiba	5	290030
207	Água Fria	5	290040
209	Alagoinhas	5	290070
212	Amargosa	5	290100
214	América Dourada	5	290115
216	Andaraí	5	290130
219	Anguera	5	290150
221	Antônio Cardoso	5	290170
223	Aporá	5	290190
226	Aracatu	5	290200
228	Aramari	5	290220
230	Aratuípe	5	290230
232	Baianópolis	5	290250
235	Barra	5	290270
237	Barra Do Choça	5	290290
240	Barreiras	5	290320
242	Barrocas	5	290327
244	Belmonte	5	290340
247	Boa Nova	5	290370
249	Bom Jesus Da Lapa	5	290390
251	Boninal	5	290400
254	Botuporã	5	290420
256	Brejolândia	5	290440
257	Brotas De Macaúbas	5	290450
259	Buerarema	5	290470
261	Caatiba	5	290480
263	Cachoeira	5	290490
266	Caetanos	5	290515
268	Cafarnaum	5	290530
270	Caldeirão Grande	5	290550
273	Camamu	5	290580
274	Campo Alegre De Lourdes	5	290590
277	Canarana	5	290620
280	Candeias	5	290650
282	Cândido Sales	5	290670
284	Canudos	5	290682
286	Capim Grosso	5	290687
289	Cardeal Da Silva	5	290700
291	Casa Nova	5	290720
294	Catu	5	290750
296	Central	5	290760
298	Cícero Dantas	5	290780
301	Cocos	5	290810
303	Conceição Do Almeida	5	290830
305	Conceição Do Jacuípe	5	290850
308	Contendas Do Sincorá	5	290880
311	Coribe	5	290910
313	Correntina	5	290930
315	Cravolândia	5	290950
318	Cruz Das Almas	5	290980
320	Dário Meira	5	291000
323	Dom Macedo Costa	5	291020
325	Encruzilhada	5	291040
327	Érico Cardoso	5	290050
330	Eunápolis	5	291072
332	Feira Da Mata	5	291077
335	Firmino Alves	5	291090
337	Formosa Do Rio Preto	5	291110
340	Gentio Do Ouro	5	291130
342	Gongogi	5	291150
345	Guanambi	5	291170
347	Heliópolis	5	291185
349	Ibiassucê	5	291200
351	Ibicoara	5	291220
354	Ibipitanga	5	291250
356	Ibirapitanga	5	291270
358	Ibirataia	5	291290
361	Ibotirama	5	291320
363	Igaporã	5	291340
365	Iguaí	5	291350
367	Inhambupe	5	291370
370	Ipirá	5	291400
372	Irajuba	5	291420
374	Iraquara	5	291440
377	Itabela	5	291465
379	Itabuna	5	291480
381	Itaeté	5	291500
383	Itagibá	5	291520
385	Itaguaçu Da Bahia	5	291535
386	Itaju Do Colônia	5	291540
388	Itamaraju	5	291560
390	Itambé	5	291580
393	Itaparica	5	291610
395	Itapebi	5	291630
397	Itapicuru	5	291650
399	Itaquara	5	291670
402	Itiruçu	5	291690
404	Itororó	5	291710
406	Ituberá	5	291730
408	Jaborandi	5	291735
411	Jaguaquara	5	291760
413	Jaguaripe	5	291780
415	Jequié	5	291800
418	Jitaúna	5	291830
419	João Dourado	5	291835
422	Jussara	5	291850
425	Lafaiete Coutinho	5	291870
427	Laje	5	291880
429	Lajedinho	5	291900
432	Lapão	5	291915
434	Lençóis	5	291930
436	Livramento De Nossa Senhora	5	291950
439	Macarani	5	291970
441	Macururé	5	291990
443	Maetinga	5	291995
445	Mairi	5	292010
447	Malhada De Pedras	5	292030
450	Maracás	5	292050
452	Maraú	5	292070
454	Mascote	5	292090
457	Medeiros Neto	5	292110
459	Milagres	5	292130
462	Monte Santo	5	292150
464	Morro Do Chapéu	5	292170
466	Mucugê	5	292190
468	Mulungu Do Morro	5	292205
471	Muquém De São Francisco	5	292225
474	Nazaré	5	292250
476	Nordestina	5	292265
478	Nova Fátima	5	292273
480	Nova Itarana	5	292280
483	Nova Viçosa	5	292300
485	Novo Triunfo	5	292305
487	Oliveira Dos Brejinhos	5	292320
490	Palmas De Monte Alto	5	292340
493	Paratinga	5	292370
495	Pau Brasil	5	292390
497	Pé De Serra	5	292405
499	Pedro Alexandre	5	292420
502	Pindaí	5	292450
504	Pintadas	5	292465
507	Piritiba	5	292480
509	Planalto	5	292500
511	Pojuca	5	292520
513	Porto Seguro	5	292530
516	Presidente Dutra	5	292560
517	Presidente Jânio Quadros	5	292570
520	Quijingue	5	292590
522	Rafael Jambeiro	5	292595
524	Retirolândia	5	292610
526	Riachão Do Jacuípe	5	292630
529	Ribeira Do Pombal	5	292660
532	Rio Do Antônio	5	292680
534	Rio Real	5	292700
537	Salinas Da Margarida	5	292730
539	Santa Bárbara	5	292750
541	Santa Cruz Cabrália	5	292770
544	Santa Luzia	5	292805
546	Santa Rita De Cássia	5	292840
549	Santana	5	292820
551	Santo Amaro	5	292860
553	Santo Estêvão	5	292880
555	São Domingos	5	292895
558	São Félix Do Coribe	5	292905
560	São Gabriel	5	292925
562	São José Da Vitória	5	292935
565	São Sebastião Do Passé	5	292950
568	Saubara	5	292975
570	Seabra	5	292990
572	Senhor Do Bonfim	5	293010
575	Serra Dourada	5	293030
578	Serrolândia	5	293060
580	Sítio Do Mato	5	293075
582	Sobradinho	5	293077
584	Tabocas Do Brejo Velho	5	293090
587	Tanquinho	5	293110
589	Tapiramutá	5	293130
591	Teodoro Sampaio	5	293140
594	Terra Nova	5	293170
596	Tucano	5	293190
599	Ubaitaba	5	293220
601	Uibaí	5	293240
603	Una	5	293250
605	Uruçuca	5	293270
607	Valença	5	293290
610	Várzea Do Poço	5	293310
612	Varzedo	5	293317
614	Vereda	5	293325
617	Wanderley	5	293345
618	Wenceslau Guimarães	5	293350
622	Acaraú	6	230020
624	Aiuaba	6	230040
626	Altaneira	6	230060
628	Amontada	6	230075
631	Aquiraz	6	230100
633	Aracoiaba	6	230120
635	Araripe	6	230130
638	Assaré	6	230160
640	Baixio	6	230180
642	Barbalha	6	230190
644	Barro	6	230200
646	Baturité	6	230210
648	Bela Cruz	6	230230
651	Camocim	6	230260
653	Canindé	6	230280
655	Caridade	6	230300
657	Caririaçu	6	230320
660	Cascavel	6	230350
662	Catunda	6	230365
664	Cedro	6	230380
667	Chorozinho	6	230395
669	Crateús	6	230410
671	Croatá	6	230423
673	Deputado Irapuan Pinheiro	6	230426
676	Farias Brito	6	230430
678	Fortaleza	6	230440
681	General Sampaio	6	230460
683	Granja	6	230470
685	Groaíras	6	230490
688	Guaramiranga	6	230510
690	Horizonte	6	230523
692	Ibiapina	6	230530
695	Icó	6	230540
697	Independência	6	230560
699	Ipaumirim	6	230570
702	Iracema	6	230600
704	Itaiçaba	6	230620
706	Itapagé	6	230630
709	Itarema	6	230655
711	Jaguaretama	6	230670
713	Jaguaribe	6	230690
716	Jati	6	230720
717	Jijoca De Jericoacoara	6	230725
720	Lavras Da Mangabeira	6	230750
723	Maracanaú	6	230765
725	Marco	6	230780
727	Massapê	6	230800
730	Milagres	6	230830
732	Miraíma	6	230837
734	Mombaça	6	230850
737	Moraújo	6	230880
739	Mucambo	6	230900
741	Nova Olinda	6	230920
743	Novo Oriente	6	230940
746	Pacajus	6	230960
748	Pacoti	6	230980
751	Palmácia	6	231010
753	Paraipaba	6	231025
755	Paramoti	6	231040
757	Penaforte	6	231060
759	Pereiro	6	231080
761	Piquet Carneiro	6	231090
764	Porteiras	6	231110
766	Potiretama	6	231123
768	Quixadá	6	231130
770	Quixeramobim	6	231140
773	Reriutaba	6	231170
774	Russas	6	231180
775	Saboeiro	6	231190
777	Santa Quitéria	6	231220
779	Santana Do Cariri	6	231210
782	São João Do Jaguaribe	6	231250
785	Senador Sá	6	231280
787	Solonópole	6	231300
789	Tamboril	6	231320
792	Tejuçuoca	6	231335
794	Trairi	6	231350
796	Ubajara	6	231360
799	Uruburetama	6	231380
801	Varjota	6	231395
803	Viçosa Do Ceará	6	231410
805	Afonso Cláudio	8	320010
808	Alegre	8	320020
810	Alto Rio Novo	8	320035
813	Aracruz	8	320060
814	Atilio Vivacqua	8	320070
817	Boa Esperança	8	320100
820	Cachoeiro De Itapemirim	8	320120
822	Castelo	8	320140
824	Conceição Da Barra	8	320160
827	Domingos Martins	8	320190
830	Fundão	8	320220
831	Governador Lindenberg	8	320225
834	Ibatiba	8	320245
837	Iconha	8	320260
839	Itaguaçu	8	320270
841	Itarana	8	320290
844	Jerônimo Monteiro	8	320310
846	Laranja Da Terra	8	320316
848	Mantenópolis	8	320330
851	Marilândia	8	320335
853	Montanha	8	320350
855	Muniz Freire	8	320370
858	Pancas	8	320400
860	Pinheiros	8	320410
863	Presidente Kennedy	8	320430
865	Rio Novo Do Sul	8	320440
867	Santa Maria De Jetibá	8	320455
870	São Gabriel Da Palha	8	320470
873	São Roque Do Canaã	8	320495
876	Vargem Alta	8	320503
878	Viana	8	320510
880	Vila Valério	8	320517
883	Abadia De Goiás	9	520005
885	Acreúna	9	520013
887	Água Fria De Goiás	9	520017
890	Alexânia	9	520030
892	Alto Horizonte	9	520055
895	Amaralina	9	520082
897	Amorinópolis	9	520090
899	Anhanguera	9	520120
900	Anicuns	9	520130
901	Aparecida De Goiânia	9	520140
904	Araçu	9	520160
906	Aragoiânia	9	520180
908	Arenópolis	9	520235
911	Avelinópolis	9	520280
913	Barro Alto	9	520320
915	Bom Jardim De Goiás	9	520340
918	Bonópolis	9	520357
920	Britânia	9	520380
922	Buriti De Goiás	9	520393
925	Cachoeira Alta	9	520410
927	Cachoeira Dourada	9	520425
930	Caldas Novas	9	520450
932	Campestre De Goiás	9	520460
935	Campo Alegre De Goiás	9	520480
937	Campos Belos	9	520490
939	Carmo Do Rio Verde	9	520500
942	Caturaí	9	520520
945	Cezarina	9	520545
947	Cidade Ocidental	9	520549
949	Colinas Do Sul	9	520552
952	Corumbaíba	9	520590
954	Cristianópolis	9	520630
956	Cromínia	9	520650
959	Damolândia	9	520680
961	Diorama	9	520710
963	Doverlândia	9	520725
966	Estrela Do Norte	9	520750
968	Fazenda Nova	9	520760
970	Flores De Goiás	9	520790
973	Gameleira De Goiás	9	520815
976	Goianésia	9	520860
978	Goianira	9	520880
980	Goiatuba	9	520910
983	Guaraíta	9	520929
985	Guarinos	9	520945
987	Hidrolândia	9	520970
990	Inaciolândia	9	520993
992	Inhumas	9	521000
994	Ipiranga De Goiás	9	521015
997	Itaberaí	9	521040
999	Itaguaru	9	521060
1001	Itapaci	9	521090
1004	Itarumã	9	521130
1006	Itumbiara	9	521150
1008	Jandaia	9	521170
1010	Jataí	9	521190
1013	Joviânia	9	521210
1015	Lagoa Santa	9	521225
1017	Luziânia	9	521250
1019	Mambaí	9	521270
1022	Matrinchã	9	521295
1024	Mimoso De Goiás	9	521305
1026	Mineiros	9	521310
1028	Monte Alegre De Goiás	9	521350
1030	Montividiu	9	521375
1032	Morrinhos	9	521380
1035	Mozarlândia	9	521400
1037	Mutunópolis	9	521410
1040	Niquelândia	9	521460
1042	Nova Aurora	9	521480
1044	Nova Glória	9	521486
1047	Nova Veneza	9	521500
1049	Novo Gama	9	521523
1051	Orizona	9	521530
1053	Ouvidor	9	521550
1055	Palestina De Goiás	9	521565
1058	Palminópolis	9	521590
1060	Paranaiguara	9	521630
1063	Petrolina De Goiás	9	521680
1065	Piracanjuba	9	521710
1067	Pirenópolis	9	521730
1070	Pontalina	9	521770
1072	Porteirão	9	521805
1075	Professor Jamil	9	521839
1077	Rialma	9	521860
1079	Rio Quente	9	521878
1082	Sanclerlândia	9	521900
1084	Santa Cruz De Goiás	9	521920
1086	Santa Helena De Goiás	9	521930
1089	Santa Rita Do Novo Destino	9	521945
1091	Santa Tereza De Goiás	9	521960
1094	Santo Antônio De Goiás	9	521973
1097	São Francisco De Goiás	9	521990
1099	São João Da Paraúna	9	522005
1102	São Miguel Do Araguaia	9	522020
1105	São Simão	9	522040
1107	Serranópolis	9	522050
1110	Sítio D`Abadia	9	522070
1112	Teresina De Goiás	9	522108
1114	Três Ranchos	9	522130
1117	Turvânia	9	522150
1119	Uirapuru	9	522157
1121	Uruana	9	522170
1123	Valparaíso De Goiás	9	522185
1126	Vicentinópolis	9	522205
1129	Açailândia	10	210005
1131	Água Doce Do Maranhão	10	210015
1134	Altamira Do Maranhão	10	210040
1136	Alto Alegre Do Pindaré	10	210047
1139	Amarante Do Maranhão	10	210060
1142	Apicum Açu	10	210083
1144	Araioses	10	210090
1146	Arari	10	210100
1149	Bacabeira	10	210125
1150	Bacuri	10	210130
1151	Bacurituba	10	210135
1153	Barão De Grajaú	10	210150
1155	Barreirinhas	10	210170
1158	Benedito Leite	10	210180
1160	Bernardo Do Mearim	10	210193
1163	Bom Jesus Das Selvas	10	210203
1165	Brejo	10	210210
1168	Buriti Bravo	10	210230
1170	Buritirana	10	210235
1172	Cajapió	10	210240
1174	Campestre Do Maranhão	10	210255
1177	Capinzal Do Norte	10	210275
1180	Caxias	10	210300
1182	Central Do Maranhão	10	210312
1184	Centro Novo Do Maranhão	10	210317
1187	Codó	10	210330
1189	Colinas	10	210350
1192	Cururupu	10	210370
1194	Dom Pedro	10	210380
1196	Esperantinópolis	10	210400
1198	Feira Nova Do Maranhão	10	210407
1201	Fortaleza Dos Nogueiras	10	210410
1204	Gonçalves Dias	10	210440
1206	Governador Edison Lobão	10	210455
1208	Governador Luiz Rocha	10	210462
1211	Graça Aranha	10	210470
1214	Humberto De Campos	10	210500
1216	Igarapé Do Meio	10	210515
1219	Itaipava Do Grajaú	10	210535
1221	Itinga Do Maranhão	10	210542
1223	Jenipapo Dos Vieiras	10	210547
1226	Junco Do Maranhão	10	210565
1228	Lago Do Junco	10	210580
1231	Lagoa Do Mato	10	210592
1233	Lajeado Novo	10	210598
1236	Luís Domingues	10	210620
1238	Maracaçumé	10	210632
1240	Maranhãozinho	10	210637
1243	Matões	10	210660
1245	Milagres Do Maranhão	10	210667
1248	Mirinzal	10	210680
1250	Montes Altos	10	210700
1253	Nova Colinas	10	210725
1255	Nova Olinda Do Maranhão	10	210735
1257	Olinda Nova Do Maranhão	10	210745
1260	Paraibano	10	210770
1263	Pastos Bons	10	210800
1265	Paulo Ramos	10	210810
1267	Pedro Do Rosário	10	210825
1270	Peritoró	10	210845
1272	Pinheiro	10	210860
1274	Pirapemas	10	210880
1275	Poção De Pedras	10	210890
1278	Presidente Dutra	10	210910
1280	Presidente Médici	10	210923
1283	Primeira Cruz	10	210940
1285	Riachão	10	210950
1288	Sambaíba	10	210970
1289	Santa Filomena Do Maranhão	10	210975
1292	Santa Luzia	10	211000
1294	Santa Quitéria Do Maranhão	10	211010
1297	Santo Amaro Do Maranhão	10	211027
1299	São Benedito Do Rio Preto	10	211040
1302	São Domingos Do Azeitão	10	211065
1305	São Francisco Do Brejão	10	211085
1307	São João Batista	10	211100
1309	São João Do Paraíso	10	211105
1312	São José De Ribamar	10	211120
1315	São Luís Gonzaga Do Maranhão	10	211140
1318	São Pedro Dos Crentes	10	211157
1320	São Raimundo Do Doca Bezerra	10	211163
1323	Satubinha	10	211172
1325	Senador La Rocque	10	211176
1328	Sucupira Do Norte	10	211190
1330	Tasso Fragoso	10	211200
1333	Trizidela Do Vale	10	211223
1335	Tuntum	10	211230
1338	Tutóia	10	211250
1340	Vargem Grande	10	211270
1342	Vila Nova Dos Martírios	10	211285
1345	Zé Doca	10	211400
1347	Abaeté	11	310020
1349	Acaiaca	11	310040
1352	Água Comprida	11	310070
1354	Águas Formosas	11	310090
1356	Aimorés	11	310110
1359	Albertina	11	310140
1361	Alfenas	11	310160
1363	Almenara	11	310170
1365	Alpinópolis	11	310190
1368	Alto JequitibÁ	11	315350
1370	Alvarenga	11	310220
1372	Alvorada De Minas	11	310240
1375	Andrelândia	11	310280
1377	Antônio Carlos	11	310290
1379	Antônio Prado De Minas	11	310310
1382	Araçuaí	11	310340
1385	Araponga	11	310370
1387	ArapuÁ	11	310380
1389	AraxÁ	11	310400
1391	Arcos	11	310420
1394	Aricanduva	11	310445
1396	Astolfo Dutra	11	310460
1397	Ataléia	11	310470
1400	Baldim	11	310500
1402	Bandeira	11	310520
1404	Barão De Cocais	11	310540
1407	Barra Longa	11	310570
1409	Bela Vista De Minas	11	310600
1411	Belo Horizonte	11	310620
1414	Berilo	11	310650
1416	Bertópolis	11	310660
1418	Bias Fortes	11	310680
1421	Boa Esperança	11	310710
1423	Bocaiúva	11	310730
1425	Bom Jardim De Minas	11	310750
1428	Bom Jesus Do Galho	11	310780
1430	Bom Sucesso	11	310800
1432	Bonfinópolis De Minas	11	310820
1435	Botelhos	11	310840
1437	BrÁs Pires	11	310870
1439	Brasília De Minas	11	310860
1442	Brumadinho	11	310900
1444	Buenópolis	11	310920
1447	Buritizeiro	11	310940
1449	Cabo Verde	11	310950
1451	Cachoeira De Minas	11	310970
1453	Cachoeira Dourada	11	310980
1456	Caiana	11	311010
1458	Caldas	11	311030
1461	Cambuí	11	311060
1463	CampanÁrio	11	311080
1465	Campestre	11	311100
1467	Campo Azul	11	311115
1469	Campo Do Meio	11	311130
1472	Campos Gerais	11	311160
1474	Canaã	11	311170
1477	Cantagalo	11	311205
1479	Capela Nova	11	311220
1481	Capetinga	11	311240
1483	Capinópolis	11	311260
1485	Capitão Enéas	11	311270
1488	Caraí	11	311300
1490	Carandaí	11	311320
1492	Caratinga	11	311340
1495	Carlos Chagas	11	311370
1497	Carmo Da Cachoeira	11	311390
1499	Carmo De Minas	11	311410
1502	Carmo Do Rio Claro	11	311440
1505	Carrancas	11	311460
1507	Carvalhos	11	311480
1509	Cascalho Rico	11	311500
1512	Catas Altas	11	311535
1513	Catas Altas Da Noruega	11	311540
1516	Caxambu	11	311550
1519	Centralina	11	311580
1521	Chalé	11	311600
1523	Chapada Gaúcha	11	311615
1524	Chiador	11	311620
1526	Claraval	11	311640
1528	ClÁudio	11	311660
1530	Coluna	11	311680
1532	Comercinho	11	311700
1534	Conceição Da Barra De Minas	11	311520
1537	Conceição De Ipanema	11	311740
1540	Conceição Do Rio Verde	11	311770
1542	Cônego Marinho	11	311783
1545	Congonhas	11	311800
1547	Conquista	11	311820
1549	Conselheiro Pena	11	311840
1552	Coqueiral	11	311870
1554	Cordisburgo	11	311890
1557	Coroaci	11	311920
1559	Coronel Fabriciano	11	311940
1561	Coronel Pacheco	11	311960
1564	Córrego Do Bom Jesus	11	311990
1566	Córrego Novo	11	312000
1569	Cristais	11	312020
1571	Cristiano Otoni	11	312040
1573	Crucilândia	11	312060
1576	Cuparaque	11	312083
1578	Curvelo	11	312090
1580	Delfim Moreira	11	312110
1583	Descoberto	11	312130
1585	Desterro Do Melo	11	312150
1587	Diogo De Vasconcelos	11	312170
1590	Divino	11	312200
1592	Divinolândia De Minas	11	312220
1595	Divisa Nova	11	312240
1598	Dom Cavati	11	312250
1600	Dom Silvério	11	312270
1602	Dona Eusébia	11	312290
1605	Dores Do IndaiÁ	11	312320
1608	Douradoquara	11	312350
1610	Elói Mendes	11	312360
1612	Engenheiro Navarro	11	312380
1615	ErvÁlia	11	312400
1617	Espera Feliz	11	312420
1619	Espírito Santo Do Dourado	11	312440
1622	Estrela Do IndaiÁ	11	312470
1624	Eugenópolis	11	312490
1627	Fama	11	312520
1629	Felício Dos Santos	11	312540
1632	Fernandes Tourinho	11	312580
1634	Fervedouro	11	312595
1636	Formiga	11	312610
1639	Fortuna De Minas	11	312640
1641	Francisco Dumont	11	312660
1643	Franciscópolis	11	312675
1646	Frei Lagonegro	11	312695
1647	Fronteira	11	312700
1648	Fronteira Dos Vales	11	312705
1651	Funilândia	11	312720
1653	Gameleiras	11	312733
1656	GoianÁ	11	312738
1658	Gonzaga	11	312750
1660	Governador Valadares	11	312770
1663	Guanhães	11	312800
1665	Guaraciaba	11	312820
1667	Guaranésia	11	312830
1670	Guarda Mor	11	312860
1672	Guidoval	11	312880
1674	Guiricema	11	312900
1676	Heliodora	11	312920
1679	IbiÁ	11	312950
1681	Ibiracatu	11	312965
1683	Ibirité	11	312980
1685	Ibituruna	11	313000
1688	Igaratinga	11	313020
1690	Ijaci	11	313040
1692	Imbé De Minas	11	313055
1695	Indianópolis	11	313070
1697	Inhapim	11	313090
1699	Inimutaba	11	313110
1702	Ipatinga	11	313130
1704	Ipuiúna	11	313150
1706	Itabira	11	313170
1709	Itacambira	11	313200
1711	Itaguara	11	313220
1713	ItajubÁ	11	313240
1715	Itamarati De Minas	11	313260
1717	Itambé Do Mato Dentro	11	313280
1720	Itanhandu	11	313310
1723	Itapagipe	11	313340
1725	Itapeva	11	313360
1727	Itaú De Minas	11	313375
1730	Itinga	11	313400
1732	Ituiutaba	11	313420
1734	Iturama	11	313440
1736	Jaboticatubas	11	313460
1739	Jacutinga	11	313490
1741	Jaíba	11	313505
1744	JanuÁria	11	313520
1746	Japonvar	11	313535
1748	Jenipapo De Minas	11	313545
1751	JequitibÁ	11	313570
1752	Jequitinhonha	11	313580
1755	Joanésia	11	313610
1757	João Pinheiro	11	313630
1760	José Gonçalves De Minas	11	313652
1762	Josenópolis	11	313657
1765	Juramento	11	313680
1767	Juvenília	11	313695
1769	Lagamar	11	313710
1772	Lagoa Dourada	11	313740
1774	Lagoa Grande	11	313753
1777	Lambari	11	313780
1778	Lamim	11	313790
1780	Lassance	11	313810
1782	Leandro Ferreira	11	313830
1784	Leopoldina	11	313840
1787	Limeira Do Oeste	11	313862
1789	Luisburgo	11	313867
1791	LuminÁrias	11	313870
1794	Machado	11	313900
1796	Malacacheta	11	313920
1798	Manga	11	313930
1801	Mantena	11	313960
1803	Maravilhas	11	313970
1805	Mariana	11	314000
1807	MÁrio Campos	11	314015
1810	Marmelópolis	11	314040
1812	Martins Soares	11	314053
1814	Materlândia	11	314060
1817	Matias Barbosa	11	314080
1819	Matipó	11	314090
1822	Matutina	11	314120
1824	Medina	11	314140
1826	Mercês	11	314160
1829	Minduri	11	314190
1831	Miradouro	11	314210
1833	Miravânia	11	314225
1835	Moema	11	314240
1837	Monsenhor Paulo	11	314260
1840	Monte Azul	11	314290
1842	Monte Carmelo	11	314310
1844	Monte Santo De Minas	11	314320
1847	Montezuma	11	314345
1849	Morro Da Garça	11	314360
1852	Muriaé	11	314390
1854	Muzambinho	11	314410
1857	Naque	11	314435
1859	Natércia	11	314440
1861	Nepomuceno	11	314460
1863	Nova Belém	11	314467
1866	Nova Módica	11	314490
1868	Nova Porteirinha	11	314505
1870	Nova Serrana	11	314520
1873	Novo Oriente De Minas	11	314535
1875	Olaria	11	314540
1877	Olímpio Noronha	11	314550
1880	Onça De Pitangui	11	314580
1882	Orizânia	11	314587
1885	Ouro Preto	11	314610
1887	Padre Carvalho	11	314625
1889	Pai Pedro	11	314655
1892	Paiva	11	314660
1894	Palmópolis	11	314675
1896	ParÁ De Minas	11	314710
1899	Paraisópolis	11	314730
1901	Passa Quatro	11	314760
1903	Passa Vinte	11	314780
1906	Patis	11	314795
1907	Patos De Minas	11	314800
1909	Patrocínio Do Muriaé	11	314820
1911	Paulistas	11	314840
1914	Pedra Azul	11	314870
1916	Pedra Do Anta	11	314880
1918	Pedra Dourada	11	314900
1921	Pedrinópolis	11	314920
1923	Pedro Teixeira	11	314940
1926	Perdigão	11	314970
1928	Perdões	11	314990
1930	Pescador	11	315000
1932	Piedade De Caratinga	11	315015
1935	Piedade Dos Gerais	11	315040
1937	Pingo D`Água	11	315053
1940	Pirajuba	11	315070
1942	Piranguçu	11	315090
1945	Pirapora	11	315120
1947	Pitangui	11	315140
1949	Planura	11	315160
1951	Poços De Caldas	11	315180
1954	Ponte Nova	11	315210
1956	Ponto Dos Volantes	11	315217
1959	Poté	11	315240
1961	Pouso Alto	11	315260
1963	Prata	11	315280
1965	Pratinha	11	315300
1967	Presidente Juscelino	11	315320
1970	Prudente De Morais	11	315360
1973	Raposos	11	315390
1975	Recreio	11	315410
1977	Resende Costa	11	315420
1980	Riachinho	11	315445
1982	Ribeirão Das Neves	11	315460
1984	Rio Acima	11	315480
1987	Rio Doce	11	315500
1989	Rio Manso	11	315530
1991	Rio Paranaíba	11	315550
1994	Rio Pomba	11	315580
1996	Rio Vermelho	11	315600
1998	Rochedo De Minas	11	315620
2001	RosÁrio Da Limeira	11	315645
2003	Rubim	11	315660
2005	Sabinópolis	11	315680
2008	Salto Da Divisa	11	315710
2010	Santa BÁrbara Do Leste	11	315725
2013	Santa Cruz De Minas	11	315733
2015	Santa Cruz Do Escalvado	11	315740
2018	Santa Helena De Minas	11	315765
2021	Santa Margarida	11	315790
2023	Santa Maria Do Salto	11	315810
2025	Santa Rita De Caldas	11	315920
2028	Santa Rita De Minas	11	315935
2030	Santa Rita Do Sapucaí	11	315960
2031	Santa Rosa Da Serra	11	315970
2032	Santa Vitória	11	315980
2034	Santana De Cataguases	11	315840
2037	Santana Do Garambéu	11	315870
2039	Santana Do Manhuaçu	11	315890
2042	Santana Dos Montes	11	315910
2044	Santo Antônio Do Aventureiro	11	316000
2047	Santo Antônio Do Jacinto	11	316030
2049	Santo Antônio Do Retiro	11	316045
2052	Santos Dumont	11	316070
2054	São BrÁs Do Suaçuí	11	316090
2057	São Félix De Minas	11	316105
2059	São Francisco De Paula	11	316120
2061	São Francisco Do Glória	11	316140
2064	São Geraldo Do Baixio	11	316165
2067	São Gonçalo Do Rio Abaixo	11	316190
2069	São Gonçalo Do Sapucaí	11	316200
2072	São João Da Lagoa	11	316225
2074	São João Da Ponte	11	316240
2077	São João Do Manhuaçu	11	316255
2079	São João Do Oriente	11	316260
2082	São João Evangelista	11	316280
2084	São Joaquim De Bicas	11	316292
2087	São José Da Safira	11	316300
2089	São José Do Alegre	11	316320
2092	São José Do Jacuri	11	316350
2094	São Lourenço	11	316370
2097	São Pedro Do Suaçuí	11	316410
2100	São Roque De Minas	11	316430
2102	São Sebastião Da Vargem Alegre	11	316443
2105	São Sebastião Do Oeste	11	316460
2107	São Sebastião Do Rio Preto	11	316480
2111	São TomÁs De Aquino	11	316510
2113	Sapucaí Mirim	11	316540
2116	Sem Peixe	11	316556
2118	Senador Cortes	11	316560
2120	Senador José Bento	11	316580
2123	Senhora Do Porto	11	316610
2125	Sericita	11	316630
2128	Serra Da Saudade	11	316660
2130	Serra Dos Aimorés	11	316670
2132	Serranópolis De Minas	11	316695
2135	Sete Lagoas	11	316720
2138	Silvianópolis	11	316740
2140	Simonésia	11	316760
2142	Soledade De Minas	11	316780
2145	Taparuba	11	316805
2146	Tapira	11	316810
2148	Taquaraçu De Minas	11	316830
2151	Teófilo Otoni	11	316860
2153	Tiradentes	11	316880
2156	Tocos Do Moji	11	316905
2158	Tombos	11	316920
2160	Três Marias	11	316935
2163	Tupaciguara	11	316960
2165	Turvolândia	11	316980
2168	Ubaporanga	11	317005
2170	Uberlândia	11	317020
2172	Unaí	11	317040
2175	Urucânia	11	317050
2177	Vargem Alegre	11	317057
2179	Vargem Grande Do Rio Pardo	11	317065
2182	VÁrzea Da Palma	11	317080
2185	Verdelândia	11	317103
2187	Veríssimo	11	317110
2189	Vespasiano	11	317120
2192	Virgem Da Lapa	11	317160
2194	Virginópolis	11	317180
2196	Visconde Do Rio Branco	11	317200
2199	Água Clara	12	500020
2201	Amambai	12	500060
2204	Angélica	12	500085
2206	Aparecida Do Taboado	12	500100
2208	Aral Moreira	12	500124
2211	Batayporã	12	500200
2213	Bodoquena	12	500215
2215	Brasilândia	12	500230
2218	Campo Grande	12	500270
2220	Cassilândia	12	500290
2223	Coronel Sapucaia	12	500315
2225	Costa Rica	12	500325
2228	Dois Irmãos Do Buriti	12	500348
2230	Dourados	12	500370
2232	Fátima Do Sul	12	500380
2235	Guia Lopes Da Laguna	12	500410
2238	Itaporã	12	500450
2240	Ivinhema	12	500470
2242	Jaraguari	12	500490
2244	Jateí	12	500510
2247	Laguna Carapã	12	500525
2249	Miranda	12	500560
2251	Naviraí	12	500570
2253	Nova Alvorada Do Sul	12	500600
2256	Paranaíba	12	500630
2258	Pedro Gomes	12	500640
2260	Porto Murtinho	12	500690
2263	Rio Negro	12	500730
2265	Rochedo	12	500750
2267	São Gabriel Do Oeste	12	500769
2270	Sidrolândia	12	500790
2272	Tacuru	12	500795
2273	Taquarussu	12	500797
2275	Três Lagoas	12	500830
2277	Acorizal	13	510010
2279	Alta Floresta	13	510025
2282	Alto Garças	13	510040
2284	Alto Taquari	13	510060
2287	Araguainha	13	510120
2289	Arenápolis	13	510130
2291	Barão De Melgaço	13	510160
2293	Barra Do Garças	13	510180
2296	Cáceres	13	510250
2298	Campo Novo Do Parecis	13	510263
2301	Canabrava Do Norte	13	510269
2303	Carlinda	13	510279
2305	Chapada Dos Guimarães	13	510300
2308	Colíder	13	510320
2311	Confresa	13	510335
2312	Conquista D`Oeste	13	510336
2315	Curvelândia	13	510343
2317	Diamantino	13	510350
2319	Feliz Natal	13	510370
2322	General Carneiro	13	510390
2324	Guarantã Do Norte	13	510410
2326	Indiavaí	13	510450
2329	Itaúba	13	510455
2331	Jaciara	13	510480
2333	Jauru	13	510500
2336	Juruena	13	510517
2338	Lambari D`Oeste	13	510523
2340	Luciára	13	510530
2343	Mirassol D`Oeste	13	510562
2345	Nortelândia	13	510600
2347	Nova Bandeirantes	13	510615
2350	Nova Guarita	13	510880
2352	Nova Marilândia	13	510885
2354	Nova Monte Verde	13	510895
2357	Nova Olímpia	13	510623
2359	Nova Ubiratã	13	510624
2361	Novo Horizonte Do Norte	13	510627
2364	Novo São Joaquim	13	510628
2366	Paranatinga	13	510630
2368	Peixoto De Azevedo	13	510642
2371	Pontal Do Araguaia	13	510665
2373	Pontes E Lacerda	13	510675
2375	Porto Dos Gaúchos	13	510680
2378	Poxoréo	13	510700
2380	Querência	13	510706
2382	Ribeirão Cascalheira	13	510718
2385	Rondolândia	13	510757
2387	Rosário Oeste	13	510770
2390	Santa Cruz Do Xingu	13	510774
2392	Santa Terezinha	13	510777
2394	Santo Antônio Do Leste	13	510779
2396	São Félix Do Araguaia	13	510785
2397	São José Do Povo	13	510729
2398	São José Do Rio Claro	13	510730
2400	São José Dos Quatro Marcos	13	510710
2404	Sinop	13	510790
2406	Tabaporã	13	510794
2408	Tapurah	13	510800
2411	Torixoréu	13	510820
2413	Vale De São Domingos	13	510835
2416	Vila Bela Da Santíssima Trindade	13	510550
2418	Abaetetuba	14	150010
2420	Acará	14	150020
2423	Alenquer	14	150040
2425	Altamira	14	150060
2427	Ananindeua	14	150080
2429	Augusto Corrêa	14	150090
2432	Bagre	14	150110
2434	Bannach	14	150125
2437	Belterra	14	150145
2439	Bom Jesus Do Tocantins	14	150157
2441	Bragança	14	150170
2443	Brejo Grande Do Araguaia	14	150175
2446	Bujaru	14	150190
2448	Cachoeira Do Piriá	14	150195
2451	Capanema	14	150220
2453	Castanhal	14	150240
2455	Colares	14	150260
2458	Cumaru Do Norte	14	150276
2460	Curralinho	14	150280
2462	Curuçá	14	150290
2464	Eldorado Dos Carajás	14	150295
2467	Garrafão Do Norte	14	150307
2470	Igarapé Açu	14	150320
2472	Inhangapi	14	150340
2474	Irituia	14	150350
2477	Jacareacanga	14	150375
2479	Juruti	14	150390
2481	Mãe Do Rio	14	150405
2483	Marabá	14	150420
2486	Marituba	14	150442
2488	Melgaço	14	150450
2490	Moju	14	150470
2493	Nova Esperança Do Piriá	14	150495
2495	Nova Timboteua	14	150500
2497	Novo Repartimento	14	150506
2500	Oriximiná	14	150530
2502	Ourilândia Do Norte	14	150543
2505	Paragominas	14	150550
2507	Pau D`Arco	14	150555
2510	Placas	14	150565
2512	Portel	14	150580
2514	Prainha	14	150600
2516	Quatipuru	14	150611
2519	Rondon Do Pará	14	150618
2521	Salinópolis	14	150620
2523	Santa Bárbara Do Pará	14	150635
2524	Santa Cruz Do Arari	14	150640
2525	Santa Isabel Do Pará	14	150650
2528	Santa Maria Do Pará	14	150660
2531	Santarém Novo	14	150690
2533	São Caetano De Odivelas	14	150710
2535	São Domingos Do Capim	14	150720
2538	São Geraldo Do Araguaia	14	150745
2540	São João De Pirabas	14	150747
2543	São Sebastião Da Boa Vista	14	150770
2546	Soure	14	150790
2548	Terra Alta	14	150796
2551	Tracuateua	14	150803
2553	Tucumã	14	150808
2555	Ulianópolis	14	150812
2558	Viseu	14	150830
2560	Xinguara	14	150840
2562	Aguiar	15	250020
2564	Alagoa Nova	15	250040
2566	Alcantil	15	250053
2569	Amparo	15	250073
2571	Araçagi	15	250080
2573	Araruna	15	250100
2575	Areia De Baraúnas	15	250115
2578	Assunção	15	250135
2580	Bananeiras	15	250150
2582	Barra De Santa Rosa	15	250160
2585	Bayeux	15	250180
2587	Belém Do Brejo Do Cruz	15	250200
2590	Boa Vista	15	250215
2592	Bom Sucesso	15	250230
2594	Boqueirão	15	250250
2597	Brejo Dos Santos	15	250290
2599	Cabaceiras	15	250310
2601	Cachoeira Dos Índios	15	250330
2604	Cacimbas	15	250355
2606	Cajazeiras	15	250370
2608	Caldas Brandão	15	250380
2611	Capim	15	250403
2613	Carrapateira	15	250410
2615	Catingueira	15	250420
2618	Conceição	15	250440
2620	Conde	15	250460
2622	Coremas	15	250480
2624	Cruz Do Espírito Santo	15	250490
2627	Cuité De Mamanguape	15	250523
2629	Curral De Cima	15	250527
2632	Desterro	15	250540
2635	Duas Estradas	15	250580
2637	Esperança	15	250600
2639	Frei Martinho	15	250620
2642	Gurinhém	15	250640
2644	Ibiara	15	250660
2646	Imaculada	15	250670
2649	Itaporanga	15	250700
2650	Itapororoca	15	250710
2651	Itatuba	15	250720
2653	Jericó	15	250740
2655	Joca Claudino	15	251365
2658	Junco Do Seridó	15	250780
2661	Lagoa	15	250810
2663	Lagoa Seca	15	250830
2665	Livramento	15	250850
2667	Lucena	15	250860
2670	Mamanguape	15	250890
2672	Marcação	15	250905
2674	Marizópolis	15	250915
2677	Matinhas	15	250933
2679	Maturéia	15	250939
2681	Montadas	15	250950
2684	Mulungu	15	250980
2686	Nazarezinho	15	251000
2688	Nova Olinda	15	251020
2690	Olho D`Água	15	251040
2692	Ouro Velho	15	251060
2695	Patos	15	251080
2697	Pedra Branca	15	251100
2699	Pedras De Fogo	15	251120
2702	Picuí	15	251140
2704	Pilões	15	251160
2706	Pirpirituba	15	251180
2709	Poço Dantas	15	251203
2711	Pombal	15	251210
2738	São Domingos	15	251396
2741	São João Do Cariri	15	251400
2744	São José Da Lagoa Tapada	15	251420
2746	São José De Espinharas	15	251440
2749	São José Do Bonfim	15	251460
2752	São José Dos Cordeiros	15	251480
2755	São Miguel De Taipu	15	251500
2757	São Sebastião Do Umbuzeiro	15	251520
2760	Serra Branca	15	251550
2763	Serra Redonda	15	251580
2765	Sertãozinho	15	251593
2768	Soledade	15	251610
2770	Sousa	15	251620
2772	Tacima	15	251640
2775	Teixeira	15	251670
2776	Tenório	15	251675
2778	Uiraúna	15	251690
2781	Vieirópolis	15	251720
2783	Zabelê	15	251740
2785	Afogados Da Ingazeira	16	260010
2788	Água Preta	16	260040
2790	Alagoinha	16	260060
2792	Altinho	16	260080
2795	Araçoiaba	16	260105
2797	Arcoverde	16	260120
2799	Barreiros	16	260140
2801	Belém De São Francisco	16	260160
2804	Bezerros	16	260190
2806	Bom Conselho	16	260210
2809	Brejão	16	260240
2811	Brejo Da Madre De Deus	16	260260
2814	Cabo De Santo Agostinho	16	260290
2816	Cachoeirinha	16	260310
2819	Calumbi	16	260340
2821	Camocim De São Félix	16	260350
2824	Capoeiras	16	260380
2826	Carnaubeira Da Penha	16	260392
2829	Casinhas	16	260415
2831	Cedro	16	260430
2833	Chã Grande	16	260450
2835	Correntes	16	260470
2838	Cupira	16	260500
2840	Dormentes	16	260515
2842	Exu	16	260530
2844	Fernando De Noronha	16	260545
2847	Floresta	16	260570
2849	Gameleira	16	260590
2851	Glória Do Goitá	16	260610
2854	Gravatá	16	260640
2856	Ibimirim	16	260660
2858	Igarassu	16	260680
2860	Ilha De Itamaracá	16	260760
2874	Jatobá	16	260805
2876	Joaquim Nabuco	16	260820
2879	Jurema	16	260840
2881	Lagoa Do Itaenga	16	260850
2883	Lagoa Dos Gatos	16	260870
2886	Limoeiro	16	260890
2888	Machados	16	260910
2890	Maraial	16	260920
2892	Moreilândia	16	261430
2895	Olinda	16	260960
2897	Orocó	16	260980
2900	Palmeirina	16	261010
2902	Paranatama	16	261030
2904	Passira	16	261050
2906	Paulista	16	261070
2907	Pedra	16	261080
2910	Petrolina	16	261110
2912	Pombos	16	261130
2914	Quipapá	16	261150
2917	Riacho Das Almas	16	261170
2919	Rio Formoso	16	261190
2921	Salgadinho	16	261210
2924	Sanharó	16	261240
2926	Santa Cruz Da Baixa Verde	16	261247
2928	Santa Filomena	16	261255
2931	Santa Terezinha	16	261280
2933	São Bento Do Una	16	261300
2936	São Joaquim Do Monte	16	261330
2938	São José Do Belmonte	16	261350
2940	São Lourenço Da Mata	16	261370
2943	Serrita	16	261400
2945	Sirinhaém	16	261420
2948	Tabira	16	261460
2950	Tacaratu	16	261480
2952	Taquaritinga Do Norte	16	261500
2955	Timbaúba	16	261530
2957	Tracunhaém	16	261550
2959	Triunfo	16	261570
2962	Venturosa	16	261600
2964	Vertente Do Lério	16	261618
2966	Vicência	16	261630
2969	Acauã	17	220005
2971	Água Branca	17	220020
2973	Alegrete Do Piauí	17	220027
2976	Alvorada Do Gurguéia	17	220045
2978	Angical Do Piauí	17	220060
2980	Antônio Almeida	17	220080
2983	Arraial	17	220100
2986	Baixa Grande Do Ribeiro	17	220115
2988	Barras	17	220120
2990	Barro Duro	17	220140
2992	Bela Vista Do Piauí	17	220155
2995	Bertolínia	17	220170
2998	Bocaina	17	220180
3000	Bom Princípio Do Piauí	17	220191
3002	Boqueirão Do Piauí	17	220194
3005	Buriti Dos Lopes	17	220200
3007	Cabeceiras Do Piauí	17	220205
3010	Caldeirão Grande Do Piauí	17	220209
3013	Campo Grande Do Piauí	17	220213
3016	Canavieira	17	220225
3017	Canto Do Buriti	17	220230
3020	Caracol	17	220250
3022	Caridade Do Piauí	17	220255
3025	Cocal	17	220270
3027	Cocal Dos Alves	17	220272
3029	Colônia Do Gurguéia	17	220275
3030	Colônia Do Piauí	17	220277
3031	Conceição Do Canindé	17	220280
3034	Cristalândia Do Piauí	17	220300
3036	Curimatá	17	220320
3038	Curral Novo Do Piauí	17	220327
3041	Dirceu Arcoverde	17	220335
3043	Dom Inocêncio	17	220345
3045	Elesbão Veloso	17	220350
3048	Fartura Do Piauí	17	220375
3050	Floresta Do Piauí	17	220385
3052	Francinópolis	17	220400
3055	Francisco Santos	17	220420
3057	Geminiano	17	220435
3060	Guaribas	17	220455
3062	Ilha Grande	17	220465
3064	Ipiranga Do Piauí	17	220480
3066	Itainópolis	17	220500
3069	Jaicós	17	220520
3071	Jatobá Do Piauí	17	220527
3074	Joaquim Pires	17	220540
3076	José De Freitas	17	220550
3078	Júlio Borges	17	220552
3081	Lagoa De São Francisco	17	220557
3083	Lagoa Do Piauí	17	220558
3086	Landri Sales	17	220560
3088	Luzilândia	17	220580
3090	Manoel Emídio	17	220590
3093	Massapê Do Piauí	17	220605
3095	Miguel Alves	17	220620
3097	Milton Brandão	17	220635
3100	Monte Alegre Do Piauí	17	220660
3102	Morro Do Chapéu Do Piauí	17	220667
3105	Nazária	17	220672
3107	Nossa Senhora Dos Remédios	17	220680
3110	Novo Santo Antônio	17	220695
3112	Olho D`Água Do Piauí	17	220710
3115	Pajeú Do Piauí	17	220735
3118	Paquetá	17	220755
3120	Parnaíba	17	220770
3122	Patos Do Piauí	17	220777
3124	Paulistana	17	220780
3127	Pedro Laurentino	17	220793
3129	Pimenteiras	17	220810
3131	Piracuruca	17	220830
3134	Porto Alegre Do Piauí	17	220855
3136	Queimada Nova	17	220865
3138	Regeneração	17	220880
3141	Ribeiro Gonçalves	17	220890
3143	Santa Cruz Do Piauí	17	220910
3146	Santa Luz	17	220930
3148	Santana Do Piauí	17	220935
3150	Santo Antônio Dos Milagres	17	220945
3151	Santo Inácio Do Piauí	17	220950
3153	São Félix Do Piauí	17	220960
3155	São Francisco Do Piauí	17	220970
3157	São Gonçalo Do Piauí	17	220980
3160	São João Da Serra	17	220990
3162	São João Do Arraial	17	220997
3165	São José Do Peixe	17	221010
3168	São Lourenço Do Piauí	17	221035
3170	São Miguel Da Baixa Grande	17	221038
3173	São Pedro Do Piauí	17	221050
3175	Sebastião Barros	17	221062
3178	Simões	17	221070
3180	Socorro Do Piauí	17	221090
3182	Tamboril Do Piauí	17	221095
3185	União	17	221110
3187	Valença Do Piauí	17	221130
3189	Várzea Grande	17	221140
3192	Wall Ferraz	17	221170
3194	Adrianópolis	18	410020
3196	Almirante Tamandaré	18	410040
3199	Alto Paraná	18	410060
3202	Alvorada Do Sul	18	410080
3204	Ampére	18	410100
3206	Andirá	18	410110
3209	Antônio Olinto	18	410130
3211	Arapongas	18	410150
3213	Arapuã	18	410165
3216	Ariranha Do Ivaí	18	410185
3218	Assis Chateaubriand	18	410200
3220	Atalaia	18	410220
3223	Barbosa Ferraz	18	410250
3225	Barracão	18	410260
3227	Bela Vista Do Paraíso	18	410280
3230	Boa Esperança Do Iguaçu	18	410302
3232	Boa Vista Da Aparecida	18	410305
3235	Bom Sucesso	18	410320
3237	Borrazópolis	18	410330
3240	Cafeara	18	410340
3242	Cafezal Do Sul	18	410347
3244	Cambará	18	410360
3247	Campina Da Lagoa	18	410390
3249	Campina Grande Do Sul	18	410400
3251	Campo Do Tenente	18	410410
3254	Campo Mourão	18	410430
3256	Candói	18	410442
3259	Capitão Leônidas Marques	18	410460
3261	Carlópolis	18	410470
3264	Catanduvas	18	410500
3266	Cerro Azul	18	410520
3268	Chopinzinho	18	410540
3270	Cidade Gaúcha	18	410560
3274	Congonhinhas	18	410600
3275	Conselheiro Mairinck	18	410610
3277	Corbélia	18	410630
3279	Coronel Domingos Soares	18	410645
3282	Cruz Machado	18	410680
3284	Cruzeiro Do Oeste	18	410660
3287	Curitiba	18	410690
3289	Diamante D`Oeste	18	410715
3291	Diamante Do Sul	18	410712
3294	Doutor Camargo	18	410730
3296	Enéas Marques	18	410740
3299	Esperança Nova	18	410752
3301	Farol	18	410755
3303	Fazenda Rio Grande	18	410765
3306	Figueira	18	410775
3308	Floraí	18	410780
3310	Florestópolis	18	410800
3312	Formosa Do Oeste	18	410820
3315	Francisco Alves	18	410832
3317	General Carneiro	18	410850
3320	Goioxim	18	410865
3322	Guaíra	18	410880
3324	Guamiranga	18	410895
3327	Guaraci	18	410920
3329	Guarapuava	18	410940
3331	Guaratuba	18	410960
3333	Ibaiti	18	410970
3336	Icaraíma	18	410990
3338	Iguatu	18	411005
3340	Imbituva	18	411010
3342	Inajá	18	411030
3345	Iporã	18	411060
3347	Irati	18	411070
3349	Itaguajé	18	411090
3351	Itambaracá	18	411100
3353	Itapejara D`Oeste	18	411120
3356	Ivaí	18	411140
3358	Ivaté	18	411155
3361	Jacarezinho	18	411180
3363	Jaguariaíva	18	411200
3365	Janiópolis	18	411220
3367	Japurá	18	411240
3369	Jardim Olinda	18	411260
3372	Joaquim Távora	18	411280
3374	Juranda	18	411295
3377	Lapa	18	411320
3379	Laranjeiras Do Sul	18	411330
3381	Lidianópolis	18	411342
3384	Lobato	18	411360
3386	Luiziana	18	411373
3388	Lupionópolis	18	411380
3391	Mandaguaçu	18	411410
3393	Mandirituba	18	411430
3395	Mangueirinha	18	411440
3397	Marechal Cândido Rondon	18	411460
3400	Marilândia Do Sul	18	411490
3402	Mariluz	18	411510
3403	Maringá	18	411520
3405	Maripá	18	411535
3407	Marquinho	18	411545
3410	Matinhos	18	411570
3412	Mauá Da Serra	18	411575
3414	Mercedes	18	411585
3417	Missal	18	411605
3419	Morretes	18	411620
3421	Nossa Senhora Das Graças	18	411640
3423	Nova América Da Colina	18	411660
3426	Nova Esperança	18	411690
3428	Nova Fátima	18	411700
3430	Nova Londrina	18	411710
3433	Nova Santa Bárbara	18	411721
3436	Novo Itacolomi	18	411729
3438	Ourizona	18	411740
3440	Paiçandu	18	411750
3443	Palmital	18	411780
3445	Paraíso Do Norte	18	411800
3447	Paranaguá	18	411820
3450	Pato Bragado	18	411845
3452	Paula Freitas	18	411860
3454	Peabiru	18	411880
3457	Pérola D`Oeste	18	411900
3459	Pinhais	18	411915
3461	Pinhalão	18	411920
3464	Piraquara	18	411950
3466	Pitangueiras	18	411965
3468	Planalto	18	411980
3471	Porecatu	18	412000
3473	Porto Barreiro	18	412015
3475	Porto Vitória	18	412030
3478	Presidente Castelo Branco	18	412040
3480	Prudentópolis	18	412060
3482	Quatiguá	18	412070
3484	Quatro Pontes	18	412085
3487	Quinta Do Sol	18	412110
3489	Ramilândia	18	412125
3491	Rancho Alegre D`Oeste	18	412135
3494	Renascença	18	412160
3496	Reserva Do Iguaçu	18	412175
3498	Ribeirão Do Pinhal	18	412190
3501	Rio Bonito Do Iguaçu	18	412215
3504	Rio Negro	18	412230
3506	Roncador	18	412250
3508	Rosário Do Ivaí	18	412265
3511	Salto Do Itararé	18	412290
3513	Santa Amélia	18	412310
3515	Santa Cruz De Monte Castelo	18	412330
3518	Santa Inês	18	412360
3520	Santa Izabel Do Oeste	18	412380
3523	Santa Mariana	18	412390
3525	Santa Tereza Do Oeste	18	412402
3527	Santana Do Itararé	18	412400
3528	Santo Antônio Da Platina	18	412410
3531	Santo Antônio Do Sudoeste	18	412440
3533	São Carlos Do Ivaí	18	412460
3536	São João Do Caiuá	18	412490
3539	São Jorge D`Oeste	18	412520
3541	São Jorge Do Patrocínio	18	412535
3544	São José Dos Pinhais	18	412550
3547	São Miguel Do Iguaçu	18	412570
3549	São Pedro Do Ivaí	18	412580
3551	São Sebastião Da Amoreira	18	412600
3554	Sarandi	18	412625
3557	Serranópolis Do Iguaçu	18	412635
3559	Sertanópolis	18	412650
3562	Tamarana	18	412667
3564	Tapejara	18	412680
3566	Teixeira Soares	18	412700
3569	Terra Rica	18	412730
3571	Tibagi	18	412750
3573	Toledo	18	412770
3575	Três Barras Do Paraná	18	412785
3578	Tupãssi	18	412795
3580	Ubiratã	18	412800
3582	União Da Vitória	18	412820
3585	Ventania	18	412853
3587	Verê	18	412860
3590	Wenceslau Braz	18	412850
3592	Angra Dos Reis	19	330010
3595	Areal	19	330022
3597	Arraial Do Cabo	19	330025
3599	Barra Mansa	19	330040
3602	Bom Jesus Do Itabapoana	19	330060
3604	Cachoeiras De Macacu	19	330080
3607	Cantagalo	19	330110
3609	Cardoso Moreira	19	330115
3612	Comendador Levy Gasparian	19	330095
3614	Cordeiro	19	330150
3616	Duque De Caxias	19	330170
3619	Iguaba Grande	19	330187
3622	Italva	19	330205
3624	Itaperuna	19	330220
3626	Japeri	19	330227
3629	Macuco	19	330245
3631	Mangaratiba	19	330260
3633	Mendes	19	330280
3635	Miguel Pereira	19	330290
3638	Nilópolis	19	330320
3640	Nova Friburgo	19	330340
3643	Paraíba Do Sul	19	330370
3645	Paty Do Alferes	19	330385
3647	Pinheiral	19	330395
3650	Porto Real	19	330411
3651	Quatis	19	330412
3652	Queimados	19	330414
3654	Resende	19	330420
3656	Rio Claro	19	330440
3658	Rio Das Ostras	19	330452
3661	Santo Antônio De Pádua	19	330470
3663	São Francisco De Itabapoana	19	330475
3666	São João De Meriti	19	330510
3669	São Pedro Da Aldeia	19	330520
3672	Saquarema	19	330550
3674	Silva Jardim	19	330560
3677	Teresópolis	19	330580
3679	Três Rios	19	330600
3682	Vassouras	19	330620
3683	Volta Redonda	19	330630
3686	Afonso Bezerra	20	240030
3689	Almino Afonso	20	240060
3691	Angicos	20	240080
3693	Apodi	20	240100
3695	Arês	20	240120
3697	Baía Formosa	20	240140
3700	Bento Fernandes	20	240160
3702	Bom Jesus	20	240170
3704	Caiçara Do Norte	20	240185
3707	Campo Redondo	20	240210
3709	Caraúbas	20	240230
3711	Carnaubais	20	240250
3714	Coronel Ezequiel	20	240280
3716	Cruzeta	20	240300
3718	Doutor Severiano	20	240320
3721	Espírito Santo	20	240350
3723	Felipe Guerra	20	240370
3725	Florânia	20	240380
3727	Frutuoso Gomes	20	240400
3730	Governador Dix Sept Rosado	20	240430
3733	Ielmo Marinho	20	240460
3735	Ipueira	20	240480
3737	Itaú	20	240490
3740	Janduís	20	240520
3742	Japi	20	240540
3744	Jardim De Piranhas	20	240560
3746	João Câmara	20	240580
3749	Jucurutu	20	240610
3751	Lagoa D`Anta	20	240620
3754	Lagoa Nova	20	240650
3756	Lajes	20	240670
3758	Lucrécia	20	240690
3760	Macaíba	20	240710
3763	Marcelino Vieira	20	240730
3765	Maxaranguape	20	240750
3767	Montanhas	20	240770
3769	Monte Das Gameleiras	20	240790
3772	Nísia Floresta	20	240820
3774	Olho D`Água Do Borges	20	240840
3777	Paraú	20	240870
3778	Parazinho	20	240880
3780	Parnamirim	20	240325
3782	Passagem	20	240920
3784	Pau Dos Ferros	20	240940
3787	Pedro Avelino	20	240970
3790	Pilões	20	241000
3791	Poço Branco	20	241010
3794	Presidente Juscelino	20	241030
3797	Rafael Godeiro	20	241060
3799	Riacho De Santana	20	241080
3802	Rodolfo Fernandes	20	241100
3804	Santa Cruz	20	241120
3806	Santana Do Matos	20	241140
3808	Santo Antônio	20	241150
3811	São Fernando	20	241180
3813	São Gonçalo Do Amarante	20	241200
3816	São José Do Campestre	20	241230
3818	São Miguel	20	241250
3820	São Paulo Do Potengi	20	241260
3823	São Tomé	20	241290
3825	Senador Elói De Souza	20	241310
3828	Serra Do Mel	20	241335
3830	Serrinha	20	241350
3832	Severiano Melo	20	241360
3835	Taipu	20	241390
3837	Tenente Ananias	20	241410
3839	Tibau	20	241105
3841	Timbaúba Dos Batistas	20	241430
3844	Umarizal	20	241450
3846	Várzea	20	241470
3849	Viçosa	20	241490
3851	Alta Floresta D`Oeste	21	110001
3853	Alto Paraíso	21	110040
3856	Buritis	21	110045
3858	Cacaulândia	21	110060
3860	Campo Novo De Rondônia	21	110070
3863	Cerejeiras	21	110005
3865	Colorado Do Oeste	21	110006
3868	Cujubim	21	110094
3870	Governador Jorge Teixeira	21	110100
3873	Jaru	21	110011
3875	Machadinho D`Oeste	21	110013
3877	Mirante Da Serra	21	110130
3880	Nova Mamoré	21	110033
3882	Novo Horizonte Do Oeste	21	110050
3885	Pimenta Bueno	21	110018
3887	Porto Velho	21	110020
3889	Primavera De Rondônia	21	110147
3892	Santa Luzia D`Oeste	21	110029
3894	São Francisco Do Guaporé	21	110149
3897	Teixeirópolis	21	110155
3900	Vale Do Anari	21	110175
3901	Vale Do Paraíso	21	110180
3904	Amajari	22	140002
3906	Bonfim	22	140015
3908	Caracaraí	22	140020
3910	Iracema	22	140028
3913	Pacaraima	22	140045
3915	São João Da Baliza	22	140050
3918	Aceguá	23	430003
3920	Agudo	23	430010
3922	Alecrim	23	430030
3924	Alegria	23	430045
3926	Alpestre	23	430050
3928	Alto Feliz	23	430057
4109	Herveiras	23	430957
3931	Ametista Do Sul	23	430064
3933	Anta Gorda	23	430070
3935	Arambaré	23	430085
3938	Arroio Do Meio	23	430100
3940	Arroio Do Sal	23	430105
3943	Arroio Grande	23	430130
3945	Augusto Pestana	23	430150
3948	Balneário Pinhal	23	430163
3950	Barão De Cotegipe	23	430170
3952	Barra Do Guarita	23	430185
3955	Barra Do Rio Azul	23	430192
3958	Barros Cassal	23	430200
3960	Bento Gonçalves	23	430210
3962	Boa Vista Do Buricá	23	430220
3965	Boa Vista Do Sul	23	430225
3967	Bom Princípio	23	430235
3970	Boqueirão Do Leão	23	430245
3972	Bozano	23	430258
3975	Butiá	23	430270
3977	Cacequi	23	430290
3979	Cachoeirinha	23	430310
3982	Caiçara	23	430340
3984	Camargo	23	430355
3986	Campestre Da Serra	23	430367
3988	Campinas Do Sul	23	430380
3991	Campos Borges	23	430410
3993	Cândido Godói	23	430430
3996	Canguçu	23	430450
3998	Canudos Do Vale	23	430461
4000	Capão Da Canoa	23	430463
4003	Capela De Santana	23	430468
4005	Capivari Do Sul	23	430467
4008	Carlos Barbosa	23	430480
4010	Casca	23	430490
4013	Caxias Do Sul	23	430510
4015	Cerrito	23	430512
4017	Cerro Grande	23	430515
4019	Cerro Largo	23	430520
4022	Charrua	23	430537
4024	Chuí	23	430543
4026	Cidreira	23	430545
4027	Ciríaco	23	430550
4029	Colorado	23	430560
4031	Constantina	23	430580
4033	Coqueiros Do Sul	23	430585
4035	Coronel Bicaco	23	430590
4038	Coxilha	23	430597
4040	Cristal	23	430605
4042	Cruz Alta	23	430610
4044	Cruzeiro Do Sul	23	430620
4047	Dezesseis De Novembro	23	430635
4049	Dois Irmãos	23	430640
4052	Dom Feliciano	23	430650
4054	Dom Pedro De Alcântara	23	430655
4057	Doutor Ricardo	23	430675
4060	Encruzilhada Do Sul	23	430690
4062	Entre Rios Do Sul	23	430695
4064	Erebango	23	430697
4067	Erval Grande	23	430720
4069	Esmeralda	23	430740
4071	Espumoso	23	430750
4073	Estância Velha	23	430760
4076	Estrela Velha	23	430781
4078	Fagundes Varela	23	430786
4081	Faxinalzinho	23	430805
4083	Feliz	23	430810
4085	Floriano Peixoto	23	430825
4088	Forquetinha	23	430843
4090	Frederico Westphalen	23	430850
4093	Gaurama	23	430870
4095	Gentil	23	430885
4097	Giruá	23	430900
4100	Gramado Dos Loureiros	23	430912
4102	Gravataí	23	430920
4104	Guaíba	23	430930
4106	Guarani Das Missões	23	430950
4112	Humaitá	23	430970
4114	Ibiaçá	23	430980
4116	Ibirapuitã	23	430995
4118	Igrejinha	23	431010
4121	Imbé	23	431033
4123	Independência	23	431040
4126	Ipiranga Do Sul	23	431046
4128	Itaara	23	431053
4130	Itapuca	23	431057
4133	Itatiba Do Sul	23	431070
4135	Ivoti	23	431080
4137	Jacuizinho	23	431087
4140	Jaguari	23	431110
4142	Jari	23	431113
4144	Júlio De Castilhos	23	431120
4146	Lagoa Dos Três Cantos	23	431127
4149	Lajeado	23	431140
4151	Lavras Do Sul	23	431150
4154	Linha Nova	23	431164
4155	Maçambara	23	431171
4157	Mampituba	23	431173
4159	Maquiné	23	431177
4162	Marcelino Ramos	23	431190
4164	Mariano Moro	23	431200
4167	Mato Castelhano	23	431213
4169	Mato Queimado	23	431217
4172	Miraguaí	23	431230
4174	Monte Alegre Dos Campos	23	431237
4177	Mormaço	23	431242
4179	Morro Redondo	23	431245
4181	Mostardas	23	431250
4183	Muitos Capões	23	431261
4186	Nicolau Vergueiro	23	431267
4188	Nova Alvorada	23	431275
4191	Nova Boa Vista	23	431295
4193	Nova Candelária	23	431301
4196	Nova Pádua	23	431308
4198	Nova Petrópolis	23	431320
4200	Nova Ramada	23	431333
4202	Nova Santa Rita	23	431337
4205	Novo Hamburgo	23	431340
4207	Novo Tiradentes	23	431344
4210	Paim Filho	23	431360
4212	Palmeira Das Missões	23	431370
4215	Pantano Grande	23	431395
4217	Paraíso Do Sul	23	431402
4220	Passa Sete	23	431406
4221	Passo Do Sobrado	23	431407
4224	Paverama	23	431415
4226	Pedro Osório	23	431420
4229	Picada Café	23	431442
4231	Pinhal Da Serra	23	431446
4234	Pinheiro Machado	23	431450
4236	Piratini	23	431460
4238	Poço Das Antas	23	431475
4241	Portão	23	431480
4243	Porto Lucena	23	431500
4245	Porto Vera Cruz	23	431507
4248	Presidente Lucena	23	431514
4250	Protásio Alves	23	431517
4253	Quatro Irmãos	23	431531
4255	Quinze De Novembro	23	431535
4258	Restinga Seca	23	431550
4260	Rio Grande	23	431560
4262	Riozinho	23	431575
4265	Rolador	23	431595
4267	Ronda Alta	23	431610
4269	Roque Gonzales	23	431630
4272	Saldanha Marinho	23	431643
4274	Salvador Das Missões	23	431647
4277	Santa Bárbara Do Sul	23	431670
4279	Santa Clara Do Sul	23	431675
4280	Santa Cruz Do Sul	23	431680
4281	Santa Margarida Do Sul	23	431697
4283	Santa Maria Do Herval	23	431695
4286	Santa Vitória Do Palmar	23	431730
4288	Santana Do Livramento	23	431710
4291	Santo Antônio Da Patrulha	23	431760
4293	Santo Antônio Do Palma	23	431755
4295	Santo Augusto	23	431780
4298	São Borja	23	431800
4300	São Francisco De Assis	23	431810
4303	São Jerônimo	23	431840
4305	São João Do Polêsine	23	431843
4308	São José Do Herval	23	431846
4310	São José Do Inhacorá	23	431849
4313	São José Do Sul	23	431861
4316	São Lourenço Do Sul	23	431880
4319	São Martinho	23	431910
4321	São Miguel Das Missões	23	431915
4528	Indaial	24	420750
4323	São Paulo Das Missões	23	431930
4326	São Pedro Do Butiá	23	431937
4329	São Sepé	23	431960
4331	São Valentim Do Sul	23	431971
4334	São Vicente Do Sul	23	431980
4336	Sapucaia Do Sul	23	432000
4339	Sede Nova	23	432023
4341	Selbach	23	432030
4343	Sentinela Do Sul	23	432035
4346	Sertão	23	432050
4348	Sete De Setembro	23	432057
4350	Silveira Martins	23	432065
4353	Soledade	23	432080
4355	Tapejara	23	432090
4358	Taquara	23	432120
4360	Taquaruçu Do Sul	23	432132
4362	Tenente Portela	23	432140
4365	Tio Hugo	23	432146
4367	Toropi	23	432149
4369	Tramandaí	23	432160
4371	Três Arroios	23	432163
4374	Três De Maio	23	432180
4376	Três Palmeiras	23	432185
4379	Triunfo	23	432200
4381	Tunas	23	432215
4383	Tupanciretã	23	432220
4386	Turuçu	23	432232
4388	União Da Serra	23	432235
4390	Uruguaiana	23	432240
4393	Vale Real	23	432254
4395	Vanini	23	432255
4397	Vera Cruz	23	432270
4399	Vespasiano Correa	23	432285
4402	Vicente Dutra	23	432310
4403	Victor Graeff	23	432320
4405	Vila Lângaro	23	432335
4407	Vila Nova Do Sul	23	432345
4410	Vista Gaúcha	23	432370
4413	Xangri Lá	23	432380
4415	Abelardo Luz	24	420010
4417	Agronômica	24	420030
4419	Águas De Chapecó	24	420050
4422	Alfredo Wagner	24	420070
4424	Anchieta	24	420080
4426	Anita Garibaldi	24	420100
4429	Apiúna	24	420125
4431	Araquari	24	420130
4434	Arroio Trinta	24	420160
4436	Ascurra	24	420170
4438	Aurora	24	420190
4440	Balneário Barra Do Sul	24	420205
4443	Balneáreo Piçarras	24	421280
4446	Barra Velha	24	420210
4448	Belmonte	24	420215
4450	Biguaçu	24	420230
4452	Bocaina Do Sul	24	420243
4455	Bom Jesus Do Oeste	24	420257
4458	Botuverá	24	420270
4460	Braço Do Trombudo	24	420285
4462	Brusque	24	420290
4465	Calmon	24	420315
4467	Campo Alegre	24	420330
4469	Campo Erê	24	420350
4472	Canoinhas	24	420380
4474	Capinzal	24	420390
4476	Catanduvas	24	420400
4478	Celso Ramos	24	420415
4480	Chapadão Do Lageado	24	420419
4483	Concórdia	24	420430
4485	Coronel Freitas	24	420440
4487	Correia Pinto	24	420455
4490	Cunha Porã	24	420470
4492	Curitibanos	24	420480
4494	Dionísio Cerqueira	24	420500
4497	Entre Rios	24	420517
4500	Faxinal Dos Guedes	24	420530
4502	Florianópolis	24	420540
4504	Forquilhinha	24	420545
4507	Galvão	24	420560
4509	Garuva	24	420580
4511	Governador Celso Ramos	24	420600
4514	Guabiruba	24	420630
4516	Guaramirim	24	420650
4518	Guatambú	24	420665
4521	Ibicaré	24	420680
4523	Içara	24	420700
4525	Imaruí	24	420720
4530	Ipira	24	420760
4531	Iporã Do Oeste	24	420765
4534	Iraceminha	24	420775
4536	Irati	24	420785
4539	Itaiópolis	24	420810
4541	Itapema	24	420830
4543	Itapoá	24	420845
4546	Jacinto Machado	24	420870
4548	Jaraguá Do Sul	24	420890
4550	Joaçaba	24	420900
4553	Jupiá	24	420917
4555	Lages	24	420930
4557	Lajeado Grande	24	420945
4560	Lebon Régis	24	420970
4562	Lindóia Do Sul	24	420985
4565	Luzerna	24	421003
4567	Mafra	24	421010
4569	Major Vieira	24	421030
4572	Marema	24	421055
4574	Matos Costa	24	421070
4576	Mirim Doce	24	421085
4578	Mondaí	24	421100
4580	Monte Castelo	24	421110
4583	Navegantes	24	421130
4585	Nova Itaberaba	24	421145
4588	Novo Horizonte	24	421165
4590	Otacílio Costa	24	421175
4593	Paial	24	421187
4595	Palhoça	24	421190
4597	Palmeira	24	421205
4600	Paraíso	24	421223
4602	Passos Maia	24	421227
4604	Pedras Grandes	24	421240
4607	Petrolândia	24	421270
4609	Pinheiro Preto	24	421300
4611	Planalto Alegre	24	421315
4614	Ponte Alta Do Norte	24	421335
4616	Porto Belo	24	421350
4618	Pouso Redondo	24	421370
4621	Presidente Getúlio	24	421400
4623	Princesa	24	421415
4625	Rancho Queimado	24	421430
4628	Rio Do Oeste	24	421460
4630	Rio Dos Cedros	24	421470
4633	Rio Rufino	24	421505
4635	Rodeio	24	421510
4638	Saltinho	24	421535
4640	Sangão	24	421545
4642	Santa Helena	24	421555
4644	Santa Rosa Do Sul	24	421565
4647	Santiago Do Sul	24	421569
4827	Aspásia	26	350395
4649	São Bento Do Sul	24	421580
4652	São Carlos	24	421600
4654	São Domingos	24	421610
4656	São João Batista	24	421630
4657	São João Do Itaperiú	24	421635
4659	São João Do Sul	24	421640
4662	São José Do Cedro	24	421670
4664	São Lourenço Do Oeste	24	421690
4666	São Martinho	24	421710
4669	São Pedro De Alcântara	24	421725
4672	Seara	24	421750
4674	Siderópolis	24	421760
4676	Sul Brasil	24	421775
4679	Tigrinhos	24	421795
4681	Timbé Do Sul	24	421810
4683	Timbó Grande	24	421825
4686	Treze De Maio	24	421840
4688	Trombudo Central	24	421860
4691	Turvo	24	421880
4693	Urubici	24	421890
4695	Urussanga	24	421900
4698	Vargem Bonita	24	421917
4700	Videira	24	421930
4702	Witmarsum	24	421940
4704	Xavantina	24	421960
4707	Amparo De São Francisco	25	280010
4709	Aracaju	25	280030
4711	Areia Branca	25	280050
4714	Brejo Grande	25	280070
4716	Canhoba	25	280110
4719	Carira	25	280140
4721	Cedro De São João	25	280160
4723	Cumbe	25	280190
4725	Estância	25	280210
4728	Gararu	25	280240
4730	Gracho Cardoso	25	280260
4732	Indiaroba	25	280280
4735	Itabi	25	280310
4737	Japaratuba	25	280330
4739	Lagarto	25	280350
4742	Malhada Dos Bois	25	280380
4744	Maruim	25	280400
4746	Monte Alegre De Sergipe	25	280420
4749	Nossa Senhora Aparecida	25	280445
4751	Nossa Senhora Das Dores	25	280460
4754	Pacatuba	25	280490
4756	Pedrinhas	25	280510
4759	Poço Redondo	25	280540
4761	Porto Da Folha	25	280560
4763	Riachão Do Dantas	25	280580
4766	Rosário Do Catete	25	280610
4768	Santa Luzia Do Itanhy	25	280630
4771	Santo Amaro Das Brotas	25	280660
4773	São Domingos	25	280680
4776	Simão Dias	25	280710
4778	Telha	25	280730
4780	Tomar Do Geru	25	280750
4782	Adamantina	26	350010
4783	Adolfo	26	350020
4785	Águas Da Prata	26	350040
4787	Águas De Santa Bárbara	26	350055
4790	Alambari	26	350075
4792	Altair	26	350090
4795	Alumínio	26	350115
4796	Álvares Florence	26	350120
4799	Alvinlândia	26	350150
4801	Américo Brasiliense	26	350170
4804	Analândia	26	350200
4806	Angatuba	26	350220
4809	Aparecida	26	350250
4811	Apiaí	26	350270
4813	Araçatuba	26	350280
4815	Aramina	26	350300
4818	Araraquara	26	350320
4820	Arco Íris	26	350335
4822	Areias	26	350350
4824	Ariranha	26	350370
4829	Atibaia	26	350410
4831	Avaí	26	350430
4834	Bady Bassitt	26	350460
4836	Bálsamo	26	350480
4838	Barão De Antonina	26	350500
4841	Barra Bonita	26	350530
4843	Barra Do Turvo	26	350540
4846	Barueri	26	350570
4848	Batatais	26	350590
4850	Bebedouro	26	350610
4852	Bernardino De Campos	26	350630
4855	Birigui	26	350650
4857	Boa Esperança Do Sul	26	350670
4860	Boituva	26	350700
4862	Bom Sucesso De Itararé	26	350715
4865	Borborema	26	350740
4867	Botucatu	26	350750
4869	Braúna	26	350770
4872	Brotas	26	350790
4874	Buritama	26	350810
4876	Cabrália Paulista	26	350830
4879	Cachoeira Paulista	26	350860
4881	Cafelândia	26	350880
4883	Caieiras	26	350900
4886	Cajati	26	350925
4888	Cajuru	26	350940
4890	Campinas	26	350950
4892	Campos Do Jordão	26	350970
4895	Canas	26	350995
4897	Cândido Rodrigues	26	351010
4899	Capão Bonito	26	351020
4902	Caraguatatuba	26	351050
4904	Cardoso	26	351070
4906	Cássia Dos Coqueiros	26	351090
4909	Catiguá	26	351120
4911	Cerqueira César	26	351140
4912	Cerquilho	26	351150
4914	Charqueada	26	351170
4917	Colina	26	351200
4919	Conchal	26	351220
4921	Cordeirópolis	26	351240
4923	Coronel Macedo	26	351260
4926	Cosmorama	26	351290
4929	Cristais Paulista	26	351320
4931	Cruzeiro	26	351340
4933	Cunha	26	351360
4936	Dirce Reis	26	351385
4938	Dobrada	26	351400
4940	Dolcinópolis	26	351420
4942	Dracena	26	351440
4945	Echaporã	26	351470
4947	Elias Fausto	26	351490
4949	Embaúba	26	351495
4952	Emilianópolis	26	351512
4954	Espírito Santo Do Pinhal	26	351518
4956	Estiva Gerbi	26	355730
4959	Euclides Da Cunha Paulista	26	351535
4961	Fernando Prestes	26	351560
4964	Ferraz De Vasconcelos	26	351570
4967	Florínia	26	351610
4969	Franca	26	351620
4971	Franco Da Rocha	26	351640
4974	Garça	26	351670
4976	Gavião Peixoto	26	351685
4978	Getulina	26	351700
4981	Guaimbê	26	351730
4983	Guapiaçu	26	351750
4985	Guará	26	351770
4987	Guaraci	26	351790
4990	Guararapes	26	351820
4992	Guaratinguetá	26	351840
4994	Guariba	26	351860
4997	Guatapará	26	351885
4999	Herculândia	26	351900
5001	Hortolândia	26	351907
5004	Iaras	26	351925
5006	Ibirá	26	351940
5008	Ibitinga	26	351960
5011	Iepê	26	351990
5013	Igarapava	26	352010
5015	Iguape	26	352030
5017	Ilha Solteira	26	352044
5020	Indiana	26	352060
5022	Inúbia Paulista	26	352080
5025	Ipeúna	26	352110
5027	Iporanga	26	352120
5029	Iracemápolis	26	352140
5032	Itaberá	26	352170
5034	Itajobi	26	352190
5036	Itanhaém	26	352210
5038	Itapecerica Da Serra	26	352220
5041	Itapevi	26	352250
5042	Itapira	26	352260
5045	Itaporanga	26	352280
5047	Itapura	26	352300
5049	Itararé	26	352320
5051	Itatiba	26	352340
5053	Itirapina	26	352360
5056	Itu	26	352390
5058	Ituverava	26	352410
5060	Jaboticabal	26	352430
5062	Jaci	26	352450
5065	Jales	26	352480
5067	Jandira	26	352500
5069	Jarinu	26	352520
5072	Joanópolis	26	352550
5074	José Bonifácio	26	352570
5076	Jumirim	26	352585
5078	Junqueirópolis	26	352600
5081	Lagoinha	26	352630
5083	Lavínia	26	352650
5086	Lençóis Paulista	26	352680
5088	Lindóia	26	352700
5091	Lourdes	26	352725
5093	Lucélia	26	352740
5095	Luís Antônio	26	352760
5098	Lutécia	26	352790
5100	Macaubal	26	352810
5102	Magda	26	352830
5104	Mairiporã	26	352850
5106	Marabá Paulista	26	352870
5109	Mariápolis	26	352890
5111	Marinópolis	26	352910
5114	Mauá	26	352940
5116	Meridiano	26	352960
5118	Miguelópolis	26	352970
5121	Miracatu	26	352990
5123	Mirante Do Paranapanema	26	353020
5126	Mococa	26	353050
5128	Mogi Guaçu	26	353070
5130	Mombuca	26	353090
5133	Monte Alegre Do Sul	26	353120
5135	Monte Aprazível	26	353140
5137	Monte Castelo	26	353160
5140	Morro Agudo	26	353190
5142	Motuca	26	353205
5145	Narandiba	26	353220
5147	Nazaré Paulista	26	353240
5150	Nipoã	26	353270
5152	Nova Campina	26	353282
5154	Nova Castilho	26	353286
5157	Nova Guataporanga	26	353310
5159	Nova Luzitânia	26	353330
5162	Novo Horizonte	26	353350
5164	Ocauçu	26	353370
5167	Onda Verde	26	353400
5169	Orindiúva	26	353420
5171	Osasco	26	353440
5172	Oscar Bressane	26	353450
5175	Ouro Verde	26	353480
5177	Pacaembu	26	353490
5179	Palmares Paulista	26	353510
5182	Panorama	26	353540
5184	Paraibuna	26	353560
5186	Paranapanema	26	353580
5189	Pardinho	26	353610
5191	Parisi	26	353625
5193	Paulicéia	26	353640
5196	Paulo De Faria	26	353660
5198	Pedra Bela	26	353680
5200	Pedregulho	26	353700
5203	Pedro De Toledo	26	353720
5205	Pereira Barreto	26	353740
5208	Piacatu	26	353770
5210	Pilar Do Sul	26	353790
5212	Pindorama	26	353810
5214	Piquerobi	26	353830
5217	Piracicaba	26	353870
5219	Pirajuí	26	353890
5221	Pirapora Do Bom Jesus	26	353910
5224	Piratininga	26	353940
5226	Planalto	26	353960
5228	Poá	26	353980
5231	Pongaí	26	354010
5233	Pontalinda	26	354025
5235	Populina	26	354040
5237	Porto Feliz	26	354060
5239	Potim	26	354075
5242	Pradópolis	26	354090
5244	Pratânia	26	354105
5246	Presidente Bernardes	26	354120
5249	Presidente Venceslau	26	354150
5252	Quatá	26	354170
5254	Queluz	26	354190
5256	Rafard	26	354210
5258	Redenção Da Serra	26	354230
5261	Registro	26	354260
5263	Ribeira	26	354280
5265	Ribeirão Branco	26	354300
5267	Ribeirão Do Sul	26	354320
5270	Ribeirão Pires	26	354330
5272	Rifaina	26	354360
5274	Rinópolis	26	354380
5277	Rio Grande Da Serra	26	354410
5279	Riversul	26	354350
5281	Roseira	26	354430
5284	Sabino	26	354460
5286	Sales	26	354480
5288	Salesópolis	26	354500
5291	Salto	26	354520
5293	Salto Grande	26	354540
5295	Santa Adélia	26	354560
5298	Santa Branca	26	354600
5299	Santa Clara D`Oeste	26	354610
5300	Santa Cruz Da Conceição	26	354620
5302	Santa Cruz Das Palmeiras	26	354630
5305	Santa Fé Do Sul	26	354660
5307	Santa Isabel	26	354680
5310	Santa Mercedes	26	354710
5312	Santa Rita Do Passa Quatro	26	354750
5315	Santana Da Ponte Pensa	26	354720
5318	Santo André	26	354780
5320	Santo Antônio De Posse	26	354800
5322	Santo Antônio Do Jardim	26	354810
5325	Santópolis Do Aguapeí	26	354840
5328	São Bernardo Do Campo	26	354870
5331	São Francisco	26	354900
5333	São João Das Duas Pontes	26	354920
5335	São João Do Pau D`Alho	26	354930
5338	São José Do Barreiro	26	354960
5341	São José Dos Campos	26	354990
5343	São Luís Do Paraitinga	26	355000
5346	São Paulo	26	355030
5348	São Pedro Do Turvo	26	355050
5351	São Sebastião Da Grama	26	355080
5354	Sarapuí	26	355110
5356	Sebastianópolis Do Sul	26	355130
5359	Serrana	26	355150
5361	Sete Barras	26	355180
5363	Silveiras	26	355200
5366	Sud Mennucci	26	355230
5368	Suzanápolis	26	355255
5371	Tabatinga	26	355270
5373	Taciba	26	355290
5375	Taiaçu	26	355310
5377	Tambaú	26	355330
5380	Tapiratiba	26	355360
5382	Taquaritinga	26	355370
5384	Taquarivaí	26	355385
5387	Tatuí	26	355400
5389	Tejupá	26	355420
5391	Terra Roxa	26	355440
5393	Timburi	26	355460
5396	Trabiju	26	355475
5398	Três Fronteiras	26	355490
5400	Tupã	26	355500
5403	Turmalina	26	355530
5405	Ubatuba	26	355540
5407	Uchoa	26	355560
5409	Urânia	26	355580
5412	Valentim Gentil	26	355610
5431	Alvorada	27	170070
5433	Angico	27	170105
5435	Aragominas	27	170130
5438	Araguaína	27	170210
5440	Araguatins	27	170220
5442	Arraias	27	170240
5444	Aurora Do Tocantins	27	170270
5447	Bandeirantes Do Tocantins	27	170305
5449	Barrolândia	27	170310
5452	Brasilândia Do Tocantins	27	170360
5455	Cachoeirinha	27	170382
5457	Cariri Do Tocantins	27	170386
5459	Carrasco Bonito	27	170389
5462	Chapada Da Natividade	27	170510
5464	Colinas Do Tocantins	27	170550
5467	Conceição Do Tocantins	27	170560
5470	Crixás Do Tocantins	27	170625
5473	Divinópolis Do Tocantins	27	170710
5475	Dueré	27	170730
5478	Figueirópolis	27	170765
5480	Formoso Do Araguaia	27	170820
5482	Goianorte	27	170830
5485	Gurupi	27	170950
5487	Itacajá	27	171050
5489	Itapiratins	27	171090
5492	Juarina	27	171180
5494	Lagoa Do Tocantins	27	171195
5496	Lavandeira	27	171215
5499	Marianópolis Do Tocantins	27	171250
5501	Maurilândia Do Tocantins	27	171280
5504	Monte Do Carmo	27	171360
5506	Muricilândia	27	171395
5509	Nova Olinda	27	171488
5511	Novo Acordo	27	171510
5513	Novo Jardim	27	171525
5516	Palmeirante	27	171570
5518	Palmeirópolis	27	171575
5521	Pau D`Arco	27	171630
5523	Peixe	27	171660
5525	Pindorama Do Tocantins	27	171700
5528	Ponte Alta Do Bom Jesus	27	171780
5530	Porto Alegre Do Tocantins	27	171800
5533	Presidente Kennedy	27	171840
5535	Recursolândia	27	171850
5538	Rio Dos Bois	27	171870
5540	Sampaio	27	171880
5542	Santa Fé Do Araguaia	27	171886
5545	Santa Rosa Do Tocantins	27	171890
5546	Santa Tereza Do Tocantins	27	171900
5548	São Bento Do Tocantins	27	172010
5551	São Salvador Do Tocantins	27	172025
5553	São Valério	27	172049
5558	Taipas Do Tocantins	27	172093
5561	Tocantinópolis	27	172120
5563	Tupiratins	27	172130
5565	Xambioá	27	172210
\.


--
-- TOC entry 3011 (class 0 OID 17387)
-- Dependencies: 200
-- Data for Name: empresa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.empresa (cnae, cnpj, nomefantasia, razaosocial, id) FROM stdin;
\.


--
-- TOC entry 3013 (class 0 OID 17394)
-- Dependencies: 202
-- Data for Name: endereco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.endereco (id, bairro, cep, complemento, logradouro, numero, cidade_id) FROM stdin;
\.


--
-- TOC entry 3015 (class 0 OID 17402)
-- Dependencies: 204
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estado (id, nome, sigla) FROM stdin;
1	Acre	AC
2	Alagoas	AL
3	Amazonas	AM
4	Amapá	AP
5	Bahia	BA
6	Ceará	CE
7	Distrito Federal	DF
8	Espirito Santo	ES
9	Goiás	GO
10	Maranhão	MA
11	Minas Gerais	MG
12	Mato Grosso do Sul	MS
13	Mato Grosso	MT
14	Pará	PA
15	Paraíba	PB
16	Pernambuco	PE
17	Piauí	PI
18	Paraná	PR
19	Rio de Janeiro	RJ
20	Rio Grande do Norte	RN
21	Rondônia	RO
22	Roraima	RR
23	Rio Grande do Sul	RS
24	Santa Catarina	SC
25	Sergipe	SE
26	São Paulo	SP
27	Tocantins	TO
\.


--
-- TOC entry 3017 (class 0 OID 17410)
-- Dependencies: 206
-- Data for Name: evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.evento (id, datahorainicio, datahoratermino, descricao, facilitador, imagemdivulgacao, maximodeparticipantes, minimodeparticipantes, observacoes, valor, categoriaevento_id, empresa_id, endereco_id, tipoevento_id) FROM stdin;
\.


--
-- TOC entry 3019 (class 0 OID 17421)
-- Dependencies: 208
-- Data for Name: meiocontato; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.meiocontato (id, contato, evento_id, tipomeiocontato_id, usuario_id) FROM stdin;
\.


--
-- TOC entry 3020 (class 0 OID 17427)
-- Dependencies: 209
-- Data for Name: participante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.participante (cpf, datanascimento, nome, id) FROM stdin;
\.


--
-- TOC entry 3022 (class 0 OID 17434)
-- Dependencies: 211
-- Data for Name: tipoevento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipoevento (id, descricao) FROM stdin;
\.


--
-- TOC entry 3024 (class 0 OID 17442)
-- Dependencies: 213
-- Data for Name: tipomeiocontato; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipomeiocontato (id, descricao) FROM stdin;
\.


--
-- TOC entry 3026 (class 0 OID 17450)
-- Dependencies: 215
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id, nomeusuario, senha, endereco_id) FROM stdin;
\.


--
-- TOC entry 3041 (class 0 OID 0)
-- Dependencies: 196
-- Name: categoriaevento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categoriaevento_id_seq', 1, false);


--
-- TOC entry 3042 (class 0 OID 0)
-- Dependencies: 198
-- Name: cidade_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cidade_id_seq', 1, false);


--
-- TOC entry 3043 (class 0 OID 0)
-- Dependencies: 201
-- Name: endereco_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.endereco_id_seq', 1, false);


--
-- TOC entry 3044 (class 0 OID 0)
-- Dependencies: 203
-- Name: estado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estado_id_seq', 1, false);


--
-- TOC entry 3045 (class 0 OID 0)
-- Dependencies: 205
-- Name: evento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.evento_id_seq', 1, false);


--
-- TOC entry 3046 (class 0 OID 0)
-- Dependencies: 207
-- Name: meiocontato_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.meiocontato_id_seq', 1, false);


--
-- TOC entry 3047 (class 0 OID 0)
-- Dependencies: 210
-- Name: tipoevento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipoevento_id_seq', 1, false);


--
-- TOC entry 3048 (class 0 OID 0)
-- Dependencies: 212
-- Name: tipomeiocontato_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipomeiocontato_id_seq', 1, false);


--
-- TOC entry 3049 (class 0 OID 0)
-- Dependencies: 214
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_seq', 1, false);


--
-- TOC entry 2853 (class 2606 OID 17378)
-- Name: categoriaevento categoriaevento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoriaevento
    ADD CONSTRAINT categoriaevento_pkey PRIMARY KEY (id);


--
-- TOC entry 2855 (class 2606 OID 17386)
-- Name: cidade cidade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cidade
    ADD CONSTRAINT cidade_pkey PRIMARY KEY (id);


--
-- TOC entry 2857 (class 2606 OID 17391)
-- Name: empresa empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_pkey PRIMARY KEY (id);


--
-- TOC entry 2859 (class 2606 OID 17399)
-- Name: endereco endereco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.endereco
    ADD CONSTRAINT endereco_pkey PRIMARY KEY (id);


--
-- TOC entry 2861 (class 2606 OID 17407)
-- Name: estado estado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id);


--
-- TOC entry 2863 (class 2606 OID 17418)
-- Name: evento evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento
    ADD CONSTRAINT evento_pkey PRIMARY KEY (id);


--
-- TOC entry 2865 (class 2606 OID 17426)
-- Name: meiocontato meiocontato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meiocontato
    ADD CONSTRAINT meiocontato_pkey PRIMARY KEY (id);


--
-- TOC entry 2867 (class 2606 OID 17431)
-- Name: participante participante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participante
    ADD CONSTRAINT participante_pkey PRIMARY KEY (id);


--
-- TOC entry 2869 (class 2606 OID 17439)
-- Name: tipoevento tipoevento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipoevento
    ADD CONSTRAINT tipoevento_pkey PRIMARY KEY (id);


--
-- TOC entry 2871 (class 2606 OID 17447)
-- Name: tipomeiocontato tipomeiocontato_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipomeiocontato
    ADD CONSTRAINT tipomeiocontato_pkey PRIMARY KEY (id);


--
-- TOC entry 2873 (class 2606 OID 17455)
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2875 (class 2606 OID 17461)
-- Name: empresa fk1qxb7ae8vdagy0mb5p5pjifed; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT fk1qxb7ae8vdagy0mb5p5pjifed FOREIGN KEY (id) REFERENCES public.usuario(id);


--
-- TOC entry 2882 (class 2606 OID 17496)
-- Name: meiocontato fk5vkdr60tx83okowhh42lax5gg; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meiocontato
    ADD CONSTRAINT fk5vkdr60tx83okowhh42lax5gg FOREIGN KEY (tipomeiocontato_id) REFERENCES public.tipomeiocontato(id);


--
-- TOC entry 2876 (class 2606 OID 17466)
-- Name: endereco fk8b1kcb3wucapb8dejshyn5fsx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.endereco
    ADD CONSTRAINT fk8b1kcb3wucapb8dejshyn5fsx FOREIGN KEY (cidade_id) REFERENCES public.cidade(id);


--
-- TOC entry 2885 (class 2606 OID 17511)
-- Name: usuario fk8fl5dxscva53gw12f19q6qxf8; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT fk8fl5dxscva53gw12f19q6qxf8 FOREIGN KEY (endereco_id) REFERENCES public.endereco(id);


--
-- TOC entry 2884 (class 2606 OID 17506)
-- Name: participante fkb06umka1q78t2smjwj53i5i8d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participante
    ADD CONSTRAINT fkb06umka1q78t2smjwj53i5i8d FOREIGN KEY (id) REFERENCES public.usuario(id);


--
-- TOC entry 2881 (class 2606 OID 17491)
-- Name: meiocontato fkcxvy1mrk3dh8tcdlvfeb9ubmc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meiocontato
    ADD CONSTRAINT fkcxvy1mrk3dh8tcdlvfeb9ubmc FOREIGN KEY (evento_id) REFERENCES public.evento(id);


--
-- TOC entry 2877 (class 2606 OID 17471)
-- Name: evento fkgnq7godxae8tw13blsq77o74u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento
    ADD CONSTRAINT fkgnq7godxae8tw13blsq77o74u FOREIGN KEY (categoriaevento_id) REFERENCES public.categoriaevento(id);


--
-- TOC entry 2874 (class 2606 OID 17456)
-- Name: cidade fkkworrwk40xj58kevvh3evi500; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cidade
    ADD CONSTRAINT fkkworrwk40xj58kevvh3evi500 FOREIGN KEY (estado_id) REFERENCES public.estado(id);


--
-- TOC entry 2883 (class 2606 OID 17501)
-- Name: meiocontato fkpr2omk67ew3j8ksh2wid96uoc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meiocontato
    ADD CONSTRAINT fkpr2omk67ew3j8ksh2wid96uoc FOREIGN KEY (usuario_id) REFERENCES public.usuario(id);


--
-- TOC entry 2879 (class 2606 OID 17481)
-- Name: evento fkqd0alhw4xw95c617h4kgil7qs; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento
    ADD CONSTRAINT fkqd0alhw4xw95c617h4kgil7qs FOREIGN KEY (endereco_id) REFERENCES public.endereco(id);


--
-- TOC entry 2878 (class 2606 OID 17476)
-- Name: evento fkqk3rs917r9nn0g5iargrbects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento
    ADD CONSTRAINT fkqk3rs917r9nn0g5iargrbects FOREIGN KEY (empresa_id) REFERENCES public.empresa(id);


--
-- TOC entry 2880 (class 2606 OID 17486)
-- Name: evento fkr5txxi0im1pui5fcidvomc88e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.evento
    ADD CONSTRAINT fkr5txxi0im1pui5fcidvomc88e FOREIGN KEY (tipoevento_id) REFERENCES public.tipoevento(id);


-- Completed on 2019-04-19 20:32:56 -03

--
-- PostgreSQL database dump complete
--

